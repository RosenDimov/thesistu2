﻿using System;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace ThesisTU.Common.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString CustomValidationSummary(this HtmlHelper helper)
        {
            try
            {
                if (!helper.ViewData.ModelState.IsValid)
                {
                    if (helper.ViewData.ModelState.Count() > 0)
                    {
                        TagBuilder tagBuilder = new TagBuilder("div");
                        tagBuilder.AddCssClass("alert alert-danger");

                        tagBuilder.InnerHtml += @"<a href=""#"" class=""close"" data-dismiss=""alert"">&times;</a>";
                        
                        //<div class="alert alert-danger">
                        //    <p>@Html.ValidationMessageFor(m => m.UserName)</p>
                        //    <a href="#" class="close" data-dismiss="alert">&times;</a>
                        //</div>

                        StringBuilder textBuilder = new StringBuilder();

                        foreach (var key in helper.ViewData.ModelState.Keys)
                        {
                            if (helper.ViewData.ModelState[key].Errors.Count() > 0)
                            {
                                var errorMessages = helper.ViewData.ModelState[key].Errors.Select(e => e.ErrorMessage);

                                foreach (string errorMsg in errorMessages)
                                {
                                    textBuilder.Append(helper.Encode(errorMsg));
                                    textBuilder.Append("<br/>");
                                }
                            }
                        }

                        textBuilder.Remove(textBuilder.Length - 5, 5);
                        tagBuilder.InnerHtml += textBuilder.ToString();

                        return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
    }
}
