﻿using Ninject.Modules;
using Ninject.Web.Common;
using System;
using ThesisTU.Models.Authentication;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Repositories.Concrete;

namespace ThesisTU.Common.Ninject
{
    public class DefaultNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserContextProvider>().To<UserContextProviderImpl>();
            Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
        }
    }
}