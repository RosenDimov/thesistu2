﻿using System;
using System.IO;

namespace ThesisTU.Common.Utils
{
    public class FileUtils
    {
        public static string GetNewFullFilename(string extension, string directoryName)
        {
            if (string.IsNullOrWhiteSpace(extension) || string.IsNullOrWhiteSpace(directoryName))
                throw new ArgumentException("Extension and directory name cannot be empty!");

            if (!Directory.Exists(directoryName))
                throw new DirectoryNotFoundException();

            string guid = Guid.NewGuid().ToString();
            string newFullFilename = string.Format(@"{0}\\{1}{2}", directoryName, guid, extension);

            return newFullFilename;
        }
    }
}
