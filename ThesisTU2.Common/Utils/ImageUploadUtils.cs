﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThesisTU.Common.Utils
{
    public static class ImageUploadUtils
    {
        private static readonly string[] AllowedExtensions = new string[] { ".jpg", ".jpeg", ".png", ".gif" };
        public const int ClubLogoMaxWidth = 200;
        public const int ClubLogoMaxHeight = 200;
        public const int PlayerImageMaxWidth = 500;
        public const int PlayerImageMaxHeight = 300;
        public const int UserAvatarWidth = 250;
        public const int UserAvatarHeight = 250;

        public static bool HasValidExtension(string filename)
        {
            string extension = Path.GetExtension(filename);
            return AllowedExtensions.Contains(extension);
        }

        public static bool HasClubLogoValidSize(Stream stream)
        {
            return HasValidSize(ClubLogoMaxWidth, ClubLogoMaxHeight, stream);
        }

        public static bool HasPlayerImageValidSize(Stream stream)
        {
            return HasValidSize(PlayerImageMaxWidth, PlayerImageMaxHeight, stream);
        }

        public static bool HasUserAvatarValidSize(Stream stream)
        {
            return HasValidSize(UserAvatarWidth, UserAvatarHeight, stream);
        }

        private static bool HasValidSize(int width, int height, Stream imageStream)
        {
            try
            {
                using (var img = Image.FromStream(imageStream))
                {
                    return (img.Width <= width && img.Height <= height);
                }
            }
            catch 
            {
                return false;
            }
        }
    }
}
