﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace ThesisTU.Common.Validation
{
    public static class UserRegistration
    {
        private static char[] FullnameValidNonLetterChars = new char[] { '-', ' ', '\'' };
        private const int MaxImageWidth = 500;
        private const int MaxImageHeight = 300;

        public static bool ValidateUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username) || !Char.IsLetter(username[0]))
                return false;

            if (username.All(c => Char.IsLetter(c) || Char.IsDigit(c) || c == '_')) 
            {
                return true;    
            }

            return false;
        }

        public static bool ValidateFullname(string fullname)
        {
            if (string.IsNullOrWhiteSpace(fullname) || !Char.IsLetter(fullname[0]))
                return false;

            if (fullname.All(c => Char.IsLetter(c) || FullnameValidNonLetterChars.Contains(c)))
               return true;

            return false;
        }

        public static bool ValidateImage(HttpPostedFileBase file)
        {
            if (file == null)
                return false;

            if (file.ContentLength > 1 * 1024 * 1024)
            {
                return false;
            }

            try
            {
                using (var img = Image.FromStream(file.InputStream))
                {
                    return (img.RawFormat.Equals(ImageFormat.Png) || img.RawFormat.Equals(ImageFormat.Jpeg)) && 
                           (img.Width <= MaxImageWidth && img.Height <= MaxImageHeight);
                }
            }
            catch 
            {
                return false;
            }
        }
    }
}
