﻿using System;
using System.Collections.Generic;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class ArticleCommentValidator
    {
        private const int CommentMaxLength = 300;

        public static bool Validate(ArticleCommentDO article)
        {
            if (article == null || string.IsNullOrWhiteSpace(article.Text) || article.Text.Length > CommentMaxLength)
                return false;

            return true;
        }
    }
}
