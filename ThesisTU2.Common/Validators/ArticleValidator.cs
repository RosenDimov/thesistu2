﻿using System;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class ArticleValidator
    {
        private const int TitleMaxLength = 100;
        private const int MaxConnectedTeams = 3;

        public static bool Validate(ArticleDO articleDO)
        {
            if (articleDO == null)
                return false;

            if (String.IsNullOrWhiteSpace(articleDO.Title) || String.IsNullOrWhiteSpace(articleDO.Text))
                return false;

            if (!String.IsNullOrWhiteSpace(articleDO.Title) && articleDO.Title.Length > TitleMaxLength)
                return false;

            if (articleDO.ConnectedTeamsIds == null || articleDO.ConnectedTeamsIds.Count == 0 ||
                articleDO.ConnectedTeamsIds.Count > MaxConnectedTeams)
                return false;

            return true;
        }
    }
}