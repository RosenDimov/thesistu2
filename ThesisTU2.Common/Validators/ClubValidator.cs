﻿using System;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class ClubValidator
    {
        public static bool Validate(ClubDO clubDTO)
        {
            if (clubDTO == null)
                return false;

            if (String.IsNullOrWhiteSpace(clubDTO.ClubName) || String.IsNullOrWhiteSpace(clubDTO.Ground) || String.IsNullOrWhiteSpace(clubDTO.Manager)
                || clubDTO.TrophiesCount < 0 || clubDTO.YearFounded <= 0)
                return false;

            return true;
        }
    }
}