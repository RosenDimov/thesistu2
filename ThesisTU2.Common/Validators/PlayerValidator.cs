﻿using System;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class PlayerValidator
    {
        public static bool Validate(PlayerDO playerDTO)
        {
            if (playerDTO == null)
                return false;

            if (playerDTO.Age < 0 || playerDTO.CountryID < 0 || String.IsNullOrWhiteSpace(playerDTO.FirstName) || String.IsNullOrWhiteSpace(playerDTO.LastName) ||
                playerDTO.Number < 0 || playerDTO.PositionID < 0)
                return false;

            return true;
        }
    }
}