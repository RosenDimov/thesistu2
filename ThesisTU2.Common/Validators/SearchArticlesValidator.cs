﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class SearchArticlesValidator
    {
        public static bool IsEmptySearch(SearchArticlesDO search)
        {
            return search == null || (string.IsNullOrWhiteSpace(search.Title) && string.IsNullOrWhiteSpace(search.Content) &&
                !search.StartDate.HasValue && !search.EndDate.HasValue);
        }
    }
}
