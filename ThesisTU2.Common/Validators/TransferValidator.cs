﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Models.Validators
{
    public static class TransferValidator
    {
        private const byte FreeTransferId = 4;

        public static bool Validate(TransferDO newTransfer)
        {
            if (newTransfer == null || 
                string.IsNullOrWhiteSpace(newTransfer.BuyerName) || 
                string.IsNullOrWhiteSpace(newTransfer.PlayerName) ||
                !newTransfer.DateCompleted.HasValue)
            {
                return false;
            }

            // free transfer cannot have amount
            if (newTransfer.TransferTypeID == FreeTransferId && 
                newTransfer.Amount.HasValue && 
                newTransfer.Amount.Value > 0)
                return false;

            if (newTransfer.TransferTypeID != FreeTransferId &&
                (!newTransfer.Amount.HasValue || newTransfer.Amount.Value == 0)
                )
                return false;

            //TransferDTO transfer = newTransfer.Transfer;
            //ClubDTO buyer = newTransfer.Buyer;
            //PlayerDTO player = newTransfer.Player;

            //if (buyer.ClubID <= 0 || player.ClubID <= 0 || player.PlayerID <= 0 || transfer.Type <= 0)
            //    return false;

            return true;
        }
    }
}