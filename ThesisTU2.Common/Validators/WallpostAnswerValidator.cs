﻿using System;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class WallpostAnswerValidator
    {
        private const int MaxWallpostAnswerLength = 300;

        public static bool Validate(WallpostAnswerDO wallpostAnswer)
        {
            if (wallpostAnswer == null || string.IsNullOrWhiteSpace(wallpostAnswer.Text) || wallpostAnswer.Text.Length > MaxWallpostAnswerLength)
                return false;

            return true;
        }
    }
}
