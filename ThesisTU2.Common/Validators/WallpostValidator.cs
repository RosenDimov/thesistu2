﻿using System;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Common.Validators
{
    public static class WallpostValidator
    {
        private const int WallpostMaxLength = 300;

        public static bool Validate(WallpostDO wallpost)
        {
            if (wallpost == null || string.IsNullOrWhiteSpace(wallpost.Text) || wallpost.Text.Length > WallpostMaxLength)
                return false;

            return true;
        }
    }
}
