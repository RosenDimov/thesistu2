﻿SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

:r "Create\CreateDB.sql"

USE [$(DatabaseName)]
GO

---------------------------------------------------------------
--Tools
---------------------------------------------------------------

---------------------------------------------------------------
--Tables
---------------------------------------------------------------

--Audit
:r "Create\Tables\Positions.sql"
:r "Create\Tables\Countries.sql"
:r "Create\Tables\TransferTypes.sql"
:r "Create\Tables\Clubs.sql"
:r "Create\Tables\Players.sql"
:r "Create\Tables\Transfers.sql"
:r "Create\Tables\Articles.sql"
:r "Create\Tables\ClubsArticles.sql"
:r "Create\Tables\Roles.sql"
:r "Create\Tables\Users.sql"
:r "Create\Tables\ArticleComments.sql"
:r "Create\Tables\Wallposts.sql"
:r "Create\Tables\WallpostsAnswers.sql"
:r "Create\Tables\UsersUnreadArticles.sql"
:r "Create\Tables\UsersUnreadWallposts.sql"
:r "Create\Tables\UsersFriendships.sql"

---------------------------------------------------------------
-- Functions
---------------------------------------------------------------

---------------------------------------------------------------
-- Procedures
---------------------------------------------------------------

---------------------------------------------------------------
-- Diagram
---------------------------------------------------------------

---------------------------------------------------------------
-- Views
---------------------------------------------------------------

