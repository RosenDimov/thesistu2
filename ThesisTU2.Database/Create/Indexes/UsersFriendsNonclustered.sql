CREATE NONCLUSTERED INDEX [UsersFriends_Link1]
ON UsersFriendships ([UserIDLink1], [UserIDLink2]);
GO

CREATE NONCLUSTERED INDEX [UsersFriends_Link2]
ON UsersFriendships ([UserIDLink2], [UserIDLink1]);
GO