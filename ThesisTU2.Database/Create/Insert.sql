﻿SET QUOTED_IDENTIFIER ON
GO

USE [$(DatabaseName)]
GO

SET NOCOUNT ON;
GO

---------------------------------------------------------------
--Insert
---------------------------------------------------------------

:r "Insert\Tables\Positions.sql"
:r "Insert\Tables\Countries.sql"
:r "Insert\Tables\TransferTypes.sql"
:r "Insert\Tables\Clubs.sql"
:r "Insert\Tables\Players.sql"
:r "Insert\Tables\Transfers.sql"
:r "Insert\Tables\Roles.sql"
:r "Insert\Tables\Articles.sql"
:r "Insert\Tables\ClubsArticles.sql"
:r "Insert\Tables\Users.sql"
:r "Insert\Tables\ArticleComments.sql"
:r "Insert\Tables\Wallposts.sql"
:r "Insert\Tables\WallpostsAnswers.sql"
:r "Insert\Tables\UsersUnreadArticles.sql"
:r "Insert\Tables\UsersUnreadWallposts.sql"

SET NOCOUNT OFF;
GO
