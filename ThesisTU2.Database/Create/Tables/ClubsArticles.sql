USE [FootballSystem]
GO
/****** Object:  Table [dbo].[ClubsArticles]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClubsArticles](
	[ClubID] [int] NOT NULL,
	[ArticleID] [int] NOT NULL,
 CONSTRAINT [PK_ClubsArticles] PRIMARY KEY CLUSTERED 
(
	[ClubID] ASC,
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[ClubsArticles]  WITH CHECK ADD  CONSTRAINT [FK_ClubsArticles_Articles] FOREIGN KEY([ArticleID])
REFERENCES [dbo].[Articles] ([ArticleID])
GO
ALTER TABLE [dbo].[ClubsArticles] CHECK CONSTRAINT [FK_ClubsArticles_Articles]
GO
ALTER TABLE [dbo].[ClubsArticles]  WITH CHECK ADD  CONSTRAINT [FK_ClubsArticles_Clubs] FOREIGN KEY([ClubID])
REFERENCES [dbo].[Clubs] ([ClubID])
GO
ALTER TABLE [dbo].[ClubsArticles] CHECK CONSTRAINT [FK_ClubsArticles_Clubs]
GO
