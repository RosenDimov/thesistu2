USE [FootballSystem]
GO
/****** Object:  Table [dbo].[TransferTypes]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransferTypes](
	[TransferTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TransferTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
