USE [FootballSystem]
GO
/****** Object:  Table [dbo].[UsersFriendships]    Script Date: 10.9.2014 г. 16:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersFriendships](
	[UserIDLink1] [int] NOT NULL,
	[UserIDLink2] [int] NOT NULL,
	[IsConfirmed] [bit] NOT NULL,
 CONSTRAINT [PK_UsersFriends] PRIMARY KEY CLUSTERED 
(
	[UserIDLink1] ASC,
	[UserIDLink2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[UsersFriendships]  WITH CHECK ADD  CONSTRAINT [FK_UsersFriends_UsersLink1] FOREIGN KEY([UserIDLink1])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersFriendships] CHECK CONSTRAINT [FK_UsersFriends_UsersLink1]
GO
ALTER TABLE [dbo].[UsersFriendships]  WITH CHECK ADD  CONSTRAINT [FK_UsersFriends_UsersLink2] FOREIGN KEY([UserIDLink2])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersFriendships] CHECK CONSTRAINT [FK_UsersFriends_UsersLink2]
GO
