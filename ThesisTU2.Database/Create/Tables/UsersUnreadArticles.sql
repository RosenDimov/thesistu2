USE [FootballSystem]
GO
/****** Object:  Table [dbo].[UsersUnreadArticles]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersUnreadArticles](
	[UserID] [int] NOT NULL,
	[ArticleID] [int] NOT NULL,
 CONSTRAINT [PK_UsersUnreadArticles] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[UsersUnreadArticles]  WITH CHECK ADD  CONSTRAINT [FK_UsersUnreadArticles_Articles] FOREIGN KEY([ArticleID])
REFERENCES [dbo].[Articles] ([ArticleID])
GO
ALTER TABLE [dbo].[UsersUnreadArticles] CHECK CONSTRAINT [FK_UsersUnreadArticles_Articles]
GO
ALTER TABLE [dbo].[UsersUnreadArticles]  WITH CHECK ADD  CONSTRAINT [FK_UsersUnreadArticles_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersUnreadArticles] CHECK CONSTRAINT [FK_UsersUnreadArticles_Users]
GO
