USE [FootballSystem]
GO
/****** Object:  Table [dbo].[UsersUnreadWallposts]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersUnreadWallposts](
	[UserID] [int] NOT NULL,
	[WallpostID] [int] NOT NULL,
 CONSTRAINT [PK_UsersUnreadWallposts] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[WallpostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[UsersUnreadWallposts]  WITH CHECK ADD  CONSTRAINT [FK_UsersUnreadWallposts_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UsersUnreadWallposts] CHECK CONSTRAINT [FK_UsersUnreadWallposts_Users]
GO
ALTER TABLE [dbo].[UsersUnreadWallposts]  WITH CHECK ADD  CONSTRAINT [FK_UsersUnreadWallposts_Wallposts] FOREIGN KEY([WallpostID])
REFERENCES [dbo].[Wallposts] ([WallpostID])
GO
ALTER TABLE [dbo].[UsersUnreadWallposts] CHECK CONSTRAINT [FK_UsersUnreadWallposts_Wallposts]
GO
