USE [FootballSystem]
GO
/****** Object:  Table [dbo].[Wallposts]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallposts](
	[WallpostID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](300) NOT NULL,
	[DatePosted] [datetime] NOT NULL,
	[AuthorID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Wallposts] PRIMARY KEY CLUSTERED 
(
	[WallpostID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Wallposts]  WITH CHECK ADD  CONSTRAINT [FK_Wallposts_Users] FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Wallposts] CHECK CONSTRAINT [FK_Wallposts_Users]
GO
ALTER TABLE [dbo].[Wallposts]  WITH CHECK ADD  CONSTRAINT [FK_Wallposts_UsersAuthor] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[Wallposts] CHECK CONSTRAINT [FK_Wallposts_UsersAuthor]
GO
