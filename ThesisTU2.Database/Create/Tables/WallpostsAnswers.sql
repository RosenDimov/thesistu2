USE [FootballSystem]
GO
/****** Object:  Table [dbo].[WallpostsAnswers]    Script Date: 1.9.2014 г. 20:56:16 ч. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WallpostsAnswers](
	[WallpostAnswerID] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](300) NOT NULL,
	[DatePosted] [datetime] NOT NULL,
	[AuthorID] [int] NOT NULL,
	[WallpostID] [int] NOT NULL,
 CONSTRAINT [PK_WallpostsAnswers] PRIMARY KEY CLUSTERED 
(
	[WallpostAnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[WallpostsAnswers]  WITH CHECK ADD  CONSTRAINT [FK_WallpostsAnswers_Users] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[WallpostsAnswers] CHECK CONSTRAINT [FK_WallpostsAnswers_Users]
GO
ALTER TABLE [dbo].[WallpostsAnswers]  WITH CHECK ADD  CONSTRAINT [FK_WallpostsAnswers_Wallposts] FOREIGN KEY([WallpostID])
REFERENCES [dbo].[Wallposts] ([WallpostID])
GO
ALTER TABLE [dbo].[WallpostsAnswers] CHECK CONSTRAINT [FK_WallpostsAnswers_Wallposts]
GO
