USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[ArticleComments] ON 

INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (1, N'he''s right', CAST(0x0000A388013872BF AS DateTime), 1, 1)
INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (2, N'it will be very difficult to win the league', CAST(0x0000A38801387A0A AS DateTime), 3, 1)
INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (6, N'they should have won', CAST(0x0000A397013267D5 AS DateTime), 6, 5)
INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (7, N'they should beat QPR next time', CAST(0x0000A39701327C94 AS DateTime), 8, 5)
INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (8, N'indeed it was', CAST(0x0000A39701381032 AS DateTime), 7, 3)
INSERT [dbo].[ArticleComments] ([ArticleCommentID], [Text], [DatePosted], [AuthorID], [ArticleID]) VALUES (9, N'I hope they win next time', CAST(0x0000A39701465FF0 AS DateTime), 1, 5)
SET IDENTITY_INSERT [dbo].[ArticleComments] OFF
