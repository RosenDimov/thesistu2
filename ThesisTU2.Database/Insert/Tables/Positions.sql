USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (1, N'Goalkeeper')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (2, N'Centre Back')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (3, N'Wingback')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (4, N'Sweeper')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (5, N'Midfielder')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (6, N'Winger')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (7, N'Attacking Midfielder')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (8, N'Second Striker')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (9, N'Forward')
INSERT [dbo].[Positions] ([PositionID], [Name]) VALUES (10, N'Striker')
SET IDENTITY_INSERT [dbo].[Positions] OFF
