USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([RoleId], [Name], [Permissions], [IsActive]) VALUES (1, N'Administrator', N'sys#admin,info#read,info#change,info#delete', 1)
INSERT [dbo].[Roles] ([RoleId], [Name], [Permissions], [IsActive]) VALUES (2, N'User', N'info#read', 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF
