USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[TransferTypes] ON 

INSERT [dbo].[TransferTypes] ([TransferTypeID], [Title]) VALUES (1, N'Full Ownership')
INSERT [dbo].[TransferTypes] ([TransferTypeID], [Title]) VALUES (2, N'Co-Ownership')
INSERT [dbo].[TransferTypes] ([TransferTypeID], [Title]) VALUES (3, N'Loan')
INSERT [dbo].[TransferTypes] ([TransferTypeID], [Title]) VALUES (4, N'Free Transfer')
SET IDENTITY_INSERT [dbo].[TransferTypes] OFF
