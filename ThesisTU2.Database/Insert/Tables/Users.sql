USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [RoleId], [Username], [PasswordHash], [PasswordSalt], [FavouriteTeamID], [CountryID], [AvatarPath], [Fullname], [IsActive]) VALUES (1, 1, N'Rokata', N'AK2BbjvqORMb0nREYaRloE45ASGFUGIQRvfG+8kruegAlnbyQ8ornv5RlmMGkRtPWg==', N'PpBzMopdmlQdPBeRoIqKnA==', 1, 21, N'0cf9fe56-9747-421f-9b37-5090404d4c03..png', N'Rosen Dimov', 1)
INSERT [dbo].[Users] ([UserId], [RoleId], [Username], [PasswordHash], [PasswordSalt], [FavouriteTeamID], [CountryID], [AvatarPath], [Fullname], [IsActive]) VALUES (3, 1, N'admin', N'AGJGRWYc1srAqrFkdRk/QAFJZkrs5exF9TnK3GSbv6bzd7ye8QJ377PH/w1Cl8Cq1g==', N'Cqk7KmOcvnuiwCnYQJpD/w==', 1, 21, NULL, N'Administrator', 1)
INSERT [dbo].[Users] ([UserId], [RoleId], [Username], [PasswordHash], [PasswordSalt], [FavouriteTeamID], [CountryID], [AvatarPath], [Fullname], [IsActive]) VALUES (6, 2, N'TestUser', N'AJAaj6Pn+zcicDH89XUsjp27sar4CKetKO7DxRvqeYUF0+k1IzJn0I9mtniGdd45GA==', N'zgXS16hXoVsHiaFYqsPpTA==', 3, 3, NULL, N'Test', 1)
INSERT [dbo].[Users] ([UserId], [RoleId], [Username], [PasswordHash], [PasswordSalt], [FavouriteTeamID], [CountryID], [AvatarPath], [Fullname], [IsActive]) VALUES (7, 2, N'ManUtdFan', N'APbwsIpzLgD5HtIgd3AfGOe9FZSwR64VqIqgf0xlkezTrPjhcKEuiczg2zszizsjZA==', N'B9p9tTXQbMOZIvzoSzj+CA==', 1, 1, NULL, N'David', 1)
INSERT [dbo].[Users] ([UserId], [RoleId], [Username], [PasswordHash], [PasswordSalt], [FavouriteTeamID], [CountryID], [AvatarPath], [Fullname], [IsActive]) VALUES (8, 2, N'SportingFan', N'AJF6M2JH5RuMG4GD9G8KqlVFtcyU3ncO+/AS25PZ36vJciu/nZ8E73CCUpjxuvRxKw==', N'CbrwGc33PPdsVUsXumz/Lw==', 71, 6, NULL, N'Carlos', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
