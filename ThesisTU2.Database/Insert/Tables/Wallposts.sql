USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[Wallposts] ON 

INSERT [dbo].[Wallposts] ([WallpostID], [Text], [DatePosted], [AuthorID], [UserID]) VALUES (1, N'test wallpost', CAST(0x0000A399010D8224 AS DateTime), 1, 1)
INSERT [dbo].[Wallposts] ([WallpostID], [Text], [DatePosted], [AuthorID], [UserID]) VALUES (2, N'Welcome to the system!', CAST(0x0000A3990110BC50 AS DateTime), 1, 8)
INSERT [dbo].[Wallposts] ([WallpostID], [Text], [DatePosted], [AuthorID], [UserID]) VALUES (4, N'Great to see you here', CAST(0x0000A39A00B54640 AS DateTime), 3, 1)
SET IDENTITY_INSERT [dbo].[Wallposts] OFF
