USE [FootballSystem]
GO
SET IDENTITY_INSERT [dbo].[WallpostsAnswers] ON 

INSERT [dbo].[WallpostsAnswers] ([WallpostAnswerID], [Text], [DatePosted], [AuthorID], [WallpostID]) VALUES (1, N'test answer', CAST(0x0000A39901157F10 AS DateTime), 3, 1)
INSERT [dbo].[WallpostsAnswers] ([WallpostAnswerID], [Text], [DatePosted], [AuthorID], [WallpostID]) VALUES (5, N'test answer 2', CAST(0x0000A3990116B470 AS DateTime), 6, 1)
INSERT [dbo].[WallpostsAnswers] ([WallpostAnswerID], [Text], [DatePosted], [AuthorID], [WallpostID]) VALUES (7, N'Nice to meet you too!', CAST(0x0000A39A00B62614 AS DateTime), 1, 4)
INSERT [dbo].[WallpostsAnswers] ([WallpostAnswerID], [Text], [DatePosted], [AuthorID], [WallpostID]) VALUES (9, N'Thanks.', CAST(0x0000A3990118F9B0 AS DateTime), 8, 2)
SET IDENTITY_INSERT [dbo].[WallpostsAnswers] OFF
