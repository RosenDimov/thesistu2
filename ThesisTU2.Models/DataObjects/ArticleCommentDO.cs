﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class ArticleCommentDO
    {
        public ArticleCommentDO() { }

        public ArticleCommentDO(ArticleComment comment) : this()
        {
            if (comment != null)
            {
                this.ArticleCommentID = comment.ArticleCommentID;
                this.ArticleID = comment.ArticleID;
                this.Text = comment.Text;
                this.DatePosted = comment.DatePosted;

                if (comment.User != null)
                {
                    this.Author = comment.User.Username;
                    this.AuthorID = comment.User.UserId;
                }
            }
        }

        public int ArticleCommentID { get; set; }
        public int ArticleID { get; set; }
        public string Text { get; set; }
        public System.DateTime DatePosted { get; set; }

        public string Author { get; set; }
        public int AuthorID { get; set; }
    }
}
