﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class ArticleDO
    {
        public ArticleDO() 
        {
            this.Comments = new List<ArticleCommentDO>();
            this.ConnectedTeams = new List<string>();
            this.ConnectedTeamsIds = new List<int>();
        }

        public ArticleDO(Article article) : this()
        {
            if (article != null)
            {
                this.ArticleID = article.ArticleID;
                this.Text = article.Text;
                this.Title = article.Title;
                this.DateAdded = article.DateAdded;

                if (article.ArticleComments != null && article.ArticleComments.Any())
                {
                    this.Comments.AddRange(article.ArticleComments.OrderByDescending(ac => ac.DatePosted)
                                                                  .Take(5)
                                                                  .Select(c => new ArticleCommentDO(c))
                                                                  .Reverse());
                }

                if (article.Clubs != null && article.Clubs.Any())
                {
                    this.ConnectedTeams.AddRange(article.Clubs.Select(c => c.ClubName));
                }
            }
        }

        public int ArticleID { get; set; }
        public string Text { get; set; }
        public string Title { get; set; }
        public System.DateTime DateAdded { get; set; }

        public List<ArticleCommentDO> Comments { get; set; }
        public List<string> ConnectedTeams { get; set; }
        public List<int> ConnectedTeamsIds { get; set; }
    }
}
