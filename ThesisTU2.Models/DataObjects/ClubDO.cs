﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class ClubDO
    {
        public ClubDO() { }

        public ClubDO(Club club) : this()
        {
            if (club != null)
            {
                this.ClubID = club.ClubID;
                this.ClubName = club.ClubName;
                this.YearFounded = club.YearFounded;

                if (club.Country != null)
                {
                    this.CountryID = club.Country.CountryID;
                    this.CountryName = club.Country.CountryName;
                }

                this.Manager = club.Manager;
                this.Ground = club.Ground;
                this.TrophiesCount = club.TrophiesCount;
                this.LogoPath = club.LogoPath;
            }
        }

        public int ClubID { get; set; }
        public string ClubName { get; set; }
        public int YearFounded { get; set; }
        public string CountryName { get; set; }
        public int CountryID { get; set; }
        public string Manager { get; set; }
        public string Ground { get; set; }
        public short TrophiesCount { get; set; }
        public string LogoPath { get; set; }

        public Club ToClub()
        {
            return new Club
            {
                ClubID = this.ClubID,
                ClubName = this.ClubName,
                CountryID = this.CountryID,
                Ground = this.Ground,
                Manager = this.Manager,
                TrophiesCount = this.TrophiesCount,
                YearFounded = this.YearFounded
            };
        }
    }
}
