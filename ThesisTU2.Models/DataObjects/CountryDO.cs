﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class CountryDO
    {
        public CountryDO() { }

        public CountryDO(Country country)
            : this()
        {
            if (country != null)
            {
                this.CountryID = country.CountryID;
                this.CountryName = country.CountryName;
            }
        }

        public int CountryID { get; set; }
        public string CountryName { get; set; }
    }
}
