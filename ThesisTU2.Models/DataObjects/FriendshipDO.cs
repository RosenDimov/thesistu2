﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class FriendshipDO
    {
        public FriendshipDO() { }

        public FriendshipDO(UsersFriendship friendship)
            : this()
        {
            if (friendship != null)
            {
                if (friendship.User != null)
                {
                    this.RequestSenderName = friendship.User.Username;
                    this.RequestSenderID = friendship.User.UserId;
                }
            }
        }

        public string RequestSenderName { get; set; }
        public int RequestSenderID { get; set; }
        public int User1ID { get; set; }
        public int User2ID { get; set; }
    }
}
