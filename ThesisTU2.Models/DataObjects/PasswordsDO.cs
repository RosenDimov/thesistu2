﻿using System;

namespace ThesisTU.Models.DataObjects
{
    public class PasswordsDO
    {
        public string NewPassword { get; set; }
        public string NewPasswordRepeat { get; set; }
        public string OldPassword { get; set; }
    }
}
