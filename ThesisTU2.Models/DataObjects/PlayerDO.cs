﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class PlayerDO
    {
        public PlayerDO() { }

        public PlayerDO(Player player)
            : this()
        {
            if (player != null)
            {
                this.PlayerID = player.PlayerID;
                this.FirstName = player.FirstName;
                this.LastName = player.LastName;
                this.Age = player.Age;

                if (player.Club != null)
                {
                    this.ClubID = player.Club.ClubID;
                    this.ClubName = player.Club.ClubName;
                }

                if (player.Country != null)
                {
                    this.CountryID = player.Country.CountryID;
                    this.CountryName = player.Country.CountryName;
                }

                if (player.Position != null)
                {
                    this.PositionID = player.Position.PositionID;
                    this.PositionName = player.Position.Name;
                }

                this.BirthDate = player.BirthDate;
                this.Number = player.Number;
                this.Goals = player.Goals;
                this.PicturePath = player.PicturePath;
            }
        }

        public int PlayerID { get; set; }
        public int ClubID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte Age { get; set; }
        public string ClubName { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public byte PositionID { get; set; }
        public string PositionName { get; set; }
        public Nullable<System.DateTime> BirthDate { get; set; }
        public byte Number { get; set; }
        public Nullable<int> Goals { get; set; }
        public string PicturePath { get; set; }

        public Player ToPlayer()
        {
            return new Player
            {
                Age = this.Age,
                BirthDate = this.BirthDate,
                ClubID = this.ClubID,
                CountryID = this.CountryID,
                FirstName = this.FirstName,
                Goals = this.Goals,
                LastName = this.LastName,
                Number = this.Number,
                PlayerID = this.PlayerID,
                PositionID = this.PositionID
            };
        }
    }
}
