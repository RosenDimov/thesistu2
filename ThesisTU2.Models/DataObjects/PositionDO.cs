﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class PositionDO
    {
        public PositionDO() { }

        public PositionDO(Position position)
            : this()
        {
            if (position != null)
            {
                this.PositionID = position.PositionID;
                this.Name = position.Name;
            }
        }

        public byte PositionID { get; set; }
        public string Name { get; set; }
    }
}
