﻿using System;

namespace ThesisTU.Models.DataObjects
{
    public class SearchArticlesDO
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
