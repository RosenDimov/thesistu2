﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class TransferDO
    {
        public TransferDO() { }

        public TransferDO(Transfer transfer) : this() 
        {
            if (transfer != null)
            {
                this.TransferID = transfer.TransferID;

                if (transfer.Player != null)
                {
                    this.PlayerID = transfer.Player.PlayerID;
                    this.PlayerName = string.Format("{0} {1}", transfer.Player.FirstName, transfer.Player.LastName);
                }

                if (transfer.Club1 != null)
                {
                    this.SellerName = transfer.Club1.ClubName;
                }

                if (transfer.Club != null)
                {
                    this.BuyerName = transfer.Club.ClubName;
                }

                this.Amount = transfer.Amount ?? Decimal.Zero;
                this.DateCompleted = transfer.DateCompleted;

                if (transfer.TransferType != null)
                {
                    this.TransferTypeID = transfer.TransferType.TransferTypeID;
                    this.Title = transfer.TransferType.Title;
                }
            }
        }

        public int TransferID { get; set; }
        public int PlayerID { get; set; }
        public string PlayerName { get; set; }
        public string SellerName { get; set; }
        public string BuyerName { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<System.DateTime> DateCompleted { get; set; }
        public byte TransferTypeID { get; set; }
        public string Title { get; set; }

        public Transfer ToTransfer()
        {
            return new Transfer
            {
                Amount = (this.TransferTypeID == 4) ? null : this.Amount,
                DateCompleted = this.DateCompleted,
                Type = this.TransferTypeID
            };
        }
    }
}
