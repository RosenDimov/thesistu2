﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class TransferTypeDO
    {
        public TransferTypeDO() { }

        public TransferTypeDO(TransferType type)
            : this()
        {
            if (type != null)
            {
                this.TransferTypeID = type.TransferTypeID;
                this.Title = type.Title;
            }
        }

        public byte TransferTypeID { get; set; }
        public string Title { get; set; }
    }
}
