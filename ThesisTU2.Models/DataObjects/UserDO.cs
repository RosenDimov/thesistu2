﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class UserDO
    {
        private const int LatestCommentsCount = 5;

        public UserDO()
        {
            this.LatestArticleComments = new List<ArticleCommentDO>();
        }

        public UserDO(User user)
            : this()
        {
            if (user != null)
            {

                this.UserId = user.UserId;
                this.Username = user.Username;
                this.Fullname = user.Fullname;
                this.AvatarPath = user.AvatarPath;

                if (user.Club != null)
                {
                    this.FavouriteTeam = user.Club.ClubName;
                }

                if (user.Country != null)
                {
                    this.CountryName = user.Country.CountryName;
                    this.CountryID = user.Country.CountryID;
                }

                if (user.ArticleComments != null)
                {
                    this.LatestArticleComments = user.ArticleComments.OrderByDescending(ac => ac.DatePosted)
                                                                     .Take(LatestCommentsCount)
                                                                     .Select(ac => new ArticleCommentDO(ac)).ToList();
                }
            }
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string AvatarPath { get; set; }
        public string ThumbPath { get; set; }
        public bool DeletePreviousAvatar { get; set; }
        public string FavouriteTeam { get; set; }
        public string CountryName { get; set; }
        public int CountryID { get; set; }
        public List<ArticleCommentDO> LatestArticleComments { get; set; }
    }
}
