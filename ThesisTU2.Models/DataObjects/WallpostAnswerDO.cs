﻿using System;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class WallpostAnswerDO
    {
        public WallpostAnswerDO() {}

        public WallpostAnswerDO(WallpostsAnswer answer)
        {
            if (answer != null)
            {
                this.WallpostAnswerID = answer.WallpostAnswerID;
                this.Text = answer.Text;
                this.DatePosted = answer.DatePosted;

                if (answer.User != null)
                {
                    this.AuthorID = answer.User.UserId;
                    this.AuthorName = answer.User.Username;
                    this.AuthorThumbImage = answer.User.ThumbPath;
                }

                this.WallpostID = answer.WallpostID;
            }
        }

        public int WallpostAnswerID { get; set; }
        public string Text { get; set; }
        public System.DateTime DatePosted { get; set; }
        public int AuthorID { get; set; }
        public string AuthorName { get; set; }
        public string AuthorThumbImage { get; set; }
        public int WallpostID { get; set; }
    }
}
