﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThesisTU.Models.Entities;

namespace ThesisTU.Models.DataObjects
{
    public class WallpostDO
    {
        public WallpostDO() 
        {
            this.WallpostsAnswers = new List<WallpostAnswerDO>();
        }

        public WallpostDO(Wallpost post) : this() 
        {
            this.WallpostID = post.WallpostID;
            this.Text = post.Text;
            this.DatePosted = post.DatePosted;

            if (post.User1 != null) 
            {
                this.AuthorID = post.User1.UserId;
                this.AuthorName = post.User1.Username;
                this.AuthorThumbImage = post.User1.ThumbPath;
            }

            this.UserID = post.UserID;

            if (post.WallpostsAnswers != null && post.WallpostsAnswers.Any())
            {
                this.AnswersCount = post.WallpostsAnswers.Count;
                this.WallpostsAnswers.AddRange(post.WallpostsAnswers.OrderByDescending(wpa => wpa.DatePosted)
                                                                    .Take(2)
                                                                    .Select(wa => new WallpostAnswerDO(wa))
                                                                    .Reverse());
            }
        }

        public int WallpostID { get; set; }
        public string Text { get; set; }
        public System.DateTime DatePosted { get; set; }
        public int AuthorID { get; set; }
        public string AuthorName { get; set; }
        public string AuthorThumbImage { get; set; }
        public int UserID { get; set; }
        public int AnswersCount { get; set; }

        public List<WallpostAnswerDO> WallpostsAnswers { get; set; }
    }
}
