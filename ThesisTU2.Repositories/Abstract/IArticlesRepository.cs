﻿using System;
using System.Collections.Generic;
using ThesisTU.Models.Entities;

namespace ThesisTU.Repositories.Abstract
{
    public interface IArticlesRepository : IRepository<Article>
    {
        IEnumerable<Article> GetLatest(int count);

        IEnumerable<Article> GetLatestForTeam(int count, int clubID);

        void AddComment(ArticleComment comment);

        IEnumerable<ArticleComment> GetComments(int id, int count, int skip);

        IEnumerable<Article> SearchArticles(string title, string content, DateTime? startDate, DateTime? endDate);
    }
}
