﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;

namespace ThesisTU.Repositories.Abstract
{
    public interface IClubsRepository : IRepository<Club>
    {
        IEnumerable<Club> GetClubsBySearchTerm(string search);

        IEnumerable<Club> GetClubsBySearchTermMatchBeginning(string search);

        Club GetClubByName(string name);

        string IsUniqueClub(string oldName, string oldManager, string newClubName, string newManagerName);

        IEnumerable<Club> GetClubsByNames(IEnumerable<string> names);

        IEnumerable<Club> GetClubsByIds(IEnumerable<int> clubIds);
    }
}
