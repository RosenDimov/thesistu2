﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;

namespace ThesisTU.Repositories.Abstract
{
    public interface ICountriesRepository : IRepository<Country>
    {
        IEnumerable<Country> GetCountriesSorted();
    }
}
