﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;

namespace ThesisTU.Repositories.Abstract
{
    public interface IPlayersRepository : IRepository<Player>
    {
        IEnumerable<Player> GetPlayersByNamesAndPosition(string search, int positionID);

        IEnumerable<Player> GetPlayersByName(string search);

        IEnumerable<Player> GetPlayersByNameMatchBeginning(string search);

        Player GetExactOneByName(string search);

        IEnumerable<Position> GetPlayerPositions();
    }
}
