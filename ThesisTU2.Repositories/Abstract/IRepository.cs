﻿using System;
using System.Linq;

namespace ThesisTU.Repositories.Abstract
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();

        T GetById(int id);

        void Add(T item);

        void Update(T item);

        void Delete(int id);
    }
}
