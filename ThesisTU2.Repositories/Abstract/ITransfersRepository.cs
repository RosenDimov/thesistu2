﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ThesisTU.Repositories.Abstract
{
    public interface ITransfersRepository : IRepository<Transfer>
    {
        IEnumerable<Transfer> GetTransfersByTeam(string team);

        IEnumerable<Transfer> GetTransfersByTeamAsBuyer(string team);

        IEnumerable<Transfer> GetTransfersByTeamAsSeller(string team);

        IEnumerable<Transfer> GetTransfersByPlayer(string playerSearch);

        IEnumerable<Transfer> GetTransfersByCategory(string transferTypeName);

        void AddNewTransfer(Transfer transfer, Player playerModel, Club buyerModel);

        //ValidTransferModel IsValidTransfer(string transferPlayer, string buyer, DateTime date);

        Transfer GetLatestTransferForPlayer(int playerId);

        Transfer GetSecondLatestTransferForPlayer(int search);

        IEnumerable<Transfer> GetLatestTransfers(int latestTransfersCount);

        IEnumerable<Transfer> GetMostExpensiveTransfers(int mostExpensiveTransfersCount);

        int GetTransfersCountForPlayer(int playerId);

        bool IsValidTransferDate(int transferId, int playerId, DateTime newDate);

        IEnumerable<TransferType> GetTransferTypes();

        bool HasPlayerNewerTransferDate(DateTime date, int playerId, out DateTime? latest);
    }
}
