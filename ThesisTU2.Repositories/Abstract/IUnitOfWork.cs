﻿using System;
using System.Transactions;

namespace ThesisTU.Repositories.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        IClubsRepository ClubsRepository { get; }

        IPlayersRepository PlayersRepository {get; }

        ITransfersRepository TransfersRepository { get; }

        ICountriesRepository CountriesRepository { get; }

        IUsersRepository UsersRepository { get; }

        IArticlesRepository ArticlesRepository { get; }

        IWallpostsRepository WallpostsRepository { get; }

        TransactionScope CreateTransactionScope(System.Transactions.IsolationLevel level = System.Transactions.IsolationLevel.ReadCommitted);

        void Save();
    }
}
