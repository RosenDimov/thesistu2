﻿using System;
using System.Collections.Generic;
using ThesisTU.Models.Entities;

namespace ThesisTU.Repositories.Abstract
{
    public interface IUsersRepository
    {
        User GetByName(string username);

        User GetById(int id);

        void Add(User user);

        IEnumerable<int> GetUsersIdsByFavouriteTeams(IEnumerable<int> favTeamIds);

        void UpdateUnreadArticles(IEnumerable<int> userIds, int articleId);

        void RemoveUnreadWallpostsEntries(int userId);

        void RemoveUnreadArticleEntries(int userId);

        void RemoveUnreadArticle(int userId, int articleId);

        void RemoveUnreadWallpost(int userId, int wallpostId);

        Tuple<bool, bool> GetFriendshipInfo(int user1Id, int user2Id);

        void AcceptFriendship(int user1Id, int user2Id);

        void CreateFriendship(int user1Id, int user2Id);

        bool UserExists(int id);
    }
}
