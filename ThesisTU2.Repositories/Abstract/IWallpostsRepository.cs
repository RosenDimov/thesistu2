﻿using System;
using System.Collections.Generic;
using ThesisTU.Models.Entities;

namespace ThesisTU.Repositories.Abstract
{
    public interface IWallpostsRepository : IRepository<Wallpost>
    {
        IEnumerable<Wallpost> GetLatestForUser(int userId, int count, int skip);

        IEnumerable<WallpostsAnswer> GetWallpostAnswers(int wallpostId, int count, int skip);

        int CountForUser(int userId);

        void AddWallpostAnswer(WallpostsAnswer wpa);
    }
}
