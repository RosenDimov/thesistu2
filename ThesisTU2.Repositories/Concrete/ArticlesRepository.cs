﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Repositories.Concrete
{
    public class ArticlesRepository : IArticlesRepository
    {
        private FootballSystemEntities db;

        public ArticlesRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public ArticlesRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public IQueryable<Article> GetAll()
        {
            return db.Articles;
        }

        public Article GetById(int id)
        {
            Article article = db.Articles.Include(a => a.ArticleComments).FirstOrDefault(a => a.ArticleID == id);
            return article;
        }

        public void Add(Article article)
        {
            if (article != null) 
                db.Articles.Add(article);
        }

        public void Update(Article article)
        {
            if (article != null)
                db.Entry(article).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Article article = GetById(id);
            db.Articles.Remove(article);
        }

        public IEnumerable<Article> GetLatest(int count)
        {
            var articles = db.Articles.OrderByDescending(a => a.DateAdded).Take(count);
            return articles;
        }

        public IEnumerable<Article> GetLatestForTeam(int count, int clubID)
        {
            if (count < 0)
                throw new ArgumentException("News count must be positive.");

            return db.Articles.Where(a => a.Clubs.Select(c => c.ClubID).Contains(clubID))
                              .OrderByDescending(a => a.DateAdded)
                              .Take(count);
        }

        public void AddComment(ArticleComment comment)
        {
            if (comment != null)
                db.ArticleComments.Add(comment);
        }

        public IEnumerable<ArticleComment> GetComments(int articleId, int count, int skip)
        {
            if (count < 0)
                throw new ArgumentException("Comments count must be positive.");

            if (skip < 0)
                throw new ArgumentException("Skip parameter must be positive.");

            var comments = db.ArticleComments.Where(ac => ac.ArticleID == articleId).OrderByDescending(ac => ac.DatePosted)
                                             .Skip(skip)
                                             .Take(count);
            return comments;
        }

        public IEnumerable<Article> SearchArticles(string title, string content, DateTime? startDate, DateTime? endDate)
        {
            var query = PredicateBuilder.True<Article>();
            int maxResultsCount = 50;

            if (!string.IsNullOrWhiteSpace(title))
            {
                query = query.And(a => a.Title.IndexOf(title) != -1);
            }

            if (!string.IsNullOrWhiteSpace(content))
            {
                query = query.And(a => a.Text.IndexOf(content) != -1);
            }

            if (startDate.HasValue)
            {
                query = query.And(a => a.DateAdded >= startDate);
            }

            if (endDate.HasValue)
            {
                query = query.And(a => a.DateAdded <= endDate);
            }

            var results = db.Articles.AsExpandable().Where(query).Take(maxResultsCount).ToList();

            return results;
        }
    }
}
