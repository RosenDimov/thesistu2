﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;

namespace ThesisTU.Repositories.Abstract
{
    public class ClubsRepository : IClubsRepository
    {
        private FootballSystemEntities db;

        public ClubsRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public ClubsRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public IQueryable<Club> GetAll()
        {
            return db.Clubs;
        }

        public Club GetById(int id)
        {
            Club club = db.Clubs.Include(c => c.Country).FirstOrDefault(c => c.ClubID == id);
            return club;
        }

        public void Add(Club club)
        {
            db.Clubs.Add(club);
        }

        public void Update(Club club)
        {
            db.Entry(club).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            //Club club = GetById(id);
            //db.Clubs.Remove(club);
        }

        public IEnumerable<Club> GetClubsBySearchTerm(string search)
        {
            var results = db.Clubs.Include(c => c.Country).Where(x => x.ClubName.IndexOf(search) != -1);
            return results;
        }

        public IEnumerable<Club> GetClubsBySearchTermMatchBeginning(string search)
        {
            var results = db.Clubs.Where(x => x.ClubName.StartsWith(search));
            return results;
        }

        public Club GetClubByName(string name)
        {
            Club clubFound = db.Clubs.FirstOrDefault(x => x.ClubName == name);
            return clubFound;
        }

        public string IsUniqueClub(string oldName, string oldManager, string newClubName, string newManagerName)
        {
            if (newClubName != oldName)
            {
                if (db.Clubs.Any(x => x.ClubName == newClubName))
                {
                    return "There is already a club with the specified name in the database. Please choose another name.";
                }
            }

            if (newManagerName != oldManager)
            {
                if (db.Clubs.Any(x => x.Manager == newManagerName))
                {
                    return "There is already a club with the specified manager in the database. A single manager can be appointed only at a single club.";
                }
            }

            return string.Empty;
        }

        public IEnumerable<Club> GetClubsByNames(IEnumerable<string> names)
        {
            if (names == null || names.Count() == 0)
                throw new ArgumentException("You must specify at least one club ID.");

            var clubs = db.Clubs.Where(c => names.Contains(c.ClubName));

            return clubs;
        }

        public IEnumerable<Club> GetClubsByIds(IEnumerable<int> clubIds)
        {
            if (clubIds == null || clubIds.Count() == 0)
                throw new ArgumentException("You must specify at least one club ID.");

            var clubs = db.Clubs.Where(c => clubIds.Contains(c.ClubID));

            return clubs;
        }
    }
}
