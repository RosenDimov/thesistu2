﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ThesisTU.Repositories.Abstract
{
    public class CountriesRepository : ICountriesRepository
    {
        private FootballSystemEntities db;

        public CountriesRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public CountriesRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public IEnumerable<Country> GetCountriesSorted()
        {
            var countries = db.Countries.OrderBy(x => x.CountryName);
            return countries;
        }

        public IQueryable<Country> GetAll()
        {
            return db.Countries;
        }

        public Country GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(Country item)
        {
            throw new NotImplementedException();
        }

        public void Update(Country item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}
