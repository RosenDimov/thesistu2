﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;

namespace ThesisTU.Repositories.Abstract
{
    public class PlayersRepository : IPlayersRepository
    {
        private FootballSystemEntities db;

        public PlayersRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public PlayersRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public IQueryable<Player> GetAll()
        {
            return db.Players;
        }

        public Player GetById(int id)
        {
            var player = db.Players.Include(x => x.Club)
                                   .Include(x => x.Country)
                                   .Include(x => x.Position).FirstOrDefault(x => x.PlayerID == id);
            return player;
        }

        public void Add(Player player)
        {
            db.Players.Add(player);
        }

        public void Update(Player player)
        {
            db.Entry(player).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
        }

        public IEnumerable<Player> GetPlayersByNamesAndPosition(string search, int positionID)
        {
            var result = db.Players.Include(x => x.Club)
                                   .Include(x => x.Position).Where(x => (x.FirstName + " " + x.LastName).IndexOf(search) != -1 && x.PositionID == positionID);
            return result;
        }

        public IEnumerable<Player> GetPlayersByName(string search)
        {
            var result = db.Players.Include(x => x.Club)
                                   .Include(x => x.Position).Where(x => (x.FirstName + " " + x.LastName).IndexOf(search) != -1);
            return result;
        }

        public IEnumerable<Player> GetPlayersByNameMatchBeginning(string search)
        {
            var result = db.Players.Where(x => (x.FirstName + " " + x.LastName).StartsWith(search));
            return result;
        }

        public Player GetExactOneByName(string playerNames)
        {
            var player = db.Players.FirstOrDefault(x => (x.FirstName + " " + x.LastName) == playerNames);
            return player;
        }

        public IEnumerable<Position> GetPlayerPositions()
        {
            return db.Positions.ToList();
        }
    }
}
