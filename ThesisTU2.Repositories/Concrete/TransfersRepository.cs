﻿using ThesisTU.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;

namespace ThesisTU.Repositories.Abstract
{
    public class TransfersRepository : ITransfersRepository
    {
        public FootballSystemEntities db;
        private const int RowsCount = 5;

        public TransfersRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public TransfersRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public Transfer GetById(int id)
        {
            var transfer = db.Transfers.Include(t => t.Club)
                                        .Include(t => t.Club1)
                                        .Include(t => t.TransferType)
                                        .Include(t => t.Player).FirstOrDefault(t => t.TransferID == id);
            return transfer;
        }

        public void Add(Transfer transfer)
        {
            if (transfer != null)
            {
                db.Transfers.Add(transfer);
            }
        }

        public void AddNewTransfer(Transfer transfer, Player playerModel, Club buyerModel)
        {
            transfer.PlayerID = playerModel.PlayerID;
            transfer.BuyerID = buyerModel.ClubID;
            transfer.SellerID = playerModel.ClubID;
            playerModel.ClubID = buyerModel.ClubID;

            db.Entry(playerModel).State = EntityState.Modified;
            Add(transfer);
        }

        public IQueryable<Transfer> GetAll()
        {
            return db.Transfers;
        }

        public void Update(Transfer transfer)
        {
            db.Entry(transfer).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var transfer = GetById(id);
            db.Transfers.Remove(transfer);
        }

        //public bool IsValidTransfer(string transferPlayer, string buyer, DateTime dateCompleted, out string err)
        //{
        //    err = string.Empty;
        //    var player = db.Players.FirstOrDefault(x => (x.FirstName + " " + x.LastName) == transferPlayer);

        //    if (player == null)
        //    {
        //        err = "Player does not exist in the databse.";
        //        return false;
        //    }

        //    var buyingTeam = db.Clubs.FirstOrDefault(x => x.ClubName == buyer);

        //    if (buyingTeam == null)
        //    {
        //        err = "The buying team was not found in the database.";
        //        return false;
        //    }

        //    bool hasNewerTransferDate = db.Transfers.Any(t => t.PlayerID == player.PlayerID && t.DateCompleted >= dateCompleted);

        //    if (hasNewerTransferDate)
        //    {
        //        DateTime? latestDate = db.Transfers.Where(t => t.PlayerID == player.PlayerID && t.DateCompleted >= dateCompleted).Select(t => t.DateCompleted).OrderByDescending(d => d).First();
        //        err = string.Format("There is a transfer with a newer date for this player. The first available date is {0}", latestDate.Value.AddDays(1).ToShortDateString());
        //        return false;
        //    }
        //    else if (player.Club.ClubName == buyer)
        //    {
        //        err = "Player's current club and buying club cannot be the same.";
        //        return false;
        //    }

        //    return true;
        //}

        public bool HasPlayerNewerTransferDate(DateTime date, int playerId, out DateTime? latest)
        {
            DateTime? latestDate = db.Transfers.Where(t => t.PlayerID == playerId && t.DateCompleted >= date).Select(t => t.DateCompleted).OrderByDescending(d => d).FirstOrDefault();
            latest = latestDate;
            return latest.HasValue;
        }

        //private void SetValidTransferModel(ValidTransferModel model, bool isValid, string errorMessage = null, Player player = null, Club club = null)
        //{
        //    model.IsValid = isValid;
        //    model.ErrorMessage = errorMessage;
        //    model.SoldPlayer = player;
        //    model.BuyerClub = club;
        //}

        public IEnumerable<Transfer> GetTransfersByTeam(string team)
        {
            var club = db.Clubs.FirstOrDefault(x => x.ClubName == team);

            if (club == null)
                return Enumerable.Empty<Transfer>();

            return db.Transfers.Include(t => t.Player)
                               .Include(t => t.Club)
                               .Include(t => t.Club1).Where(x => x.Club.ClubName == team || x.Club1.ClubName == team);
        }

        public IEnumerable<Transfer> GetTransfersByTeamAsBuyer(string team)
        {
            var club = db.Clubs.Include(x => x.Transfers).FirstOrDefault(x => x.ClubName == team);

            if (club == null)
                return Enumerable.Empty<Transfer>();

            return club.Transfers.ToList();
        }

        public IEnumerable<Transfer> GetTransfersByTeamAsSeller(string team)
        {
            var club = db.Clubs.Include(x => x.Transfers1).FirstOrDefault(x => x.ClubName == team);

            if (club == null)
                return Enumerable.Empty<Transfer>();

            return club.Transfers1.ToList();
        }

        public IEnumerable<Transfer> GetTransfersByPlayer(string playerSearch)
        {
            var players = db.Players.Where(x => (x.FirstName + " " + x.LastName).IndexOf(playerSearch) != -1).Select(x => x.PlayerID);

            if (players.Count() == 0)
                return Enumerable.Empty<Transfer>();

            var transfersFound = db.Transfers.Include(x => x.Player)
                                              .Include(x => x.Club)
                                              .Include(x => x.Club1).Where(x => players.Contains(x.PlayerID));
            return transfersFound;
        }

        public IEnumerable<Transfer> GetTransfersByCategory(string transferTypeName)
        {
            var transfersByCategory = db.Transfers.Include(x => x.Player).Include(x => x.Club).Include(x => x.Club1)
                                                                    .Where(x => x.TransferType.Title == transferTypeName)
                                                                    .OrderByDescending(x => x.DateCompleted)
                                                                    .Take(10);
            return transfersByCategory;
        }

        public Transfer GetLatestTransferForPlayer(int playerId)
        {
            Transfer latestTransfer = db.Transfers.Where(t => t.PlayerID == playerId)
                                                       .OrderByDescending(t => t.DateCompleted)
                                                       .FirstOrDefault();

            return (latestTransfer != null) ? latestTransfer : null;
        }

        public Transfer GetSecondLatestTransferForPlayer(int playerId)
        {
            IEnumerable<Transfer> latestTransfersOrdered = db.Transfers.Where(t => t.PlayerID == playerId)
                                                       .OrderByDescending(t => t.DateCompleted);

            if (latestTransfersOrdered.Count() > 1)
            {
                return latestTransfersOrdered.ElementAt(1);
            }

            return null;
        }

        public IEnumerable<Transfer> GetLatestTransfers(int latestTransfersCount)
        {
            var latestTransfers = db.Transfers.Include(x => x.Player)
                                              .Include(x => x.Club)
                                              .Include(x => x.Club1).OrderByDescending(x => x.DateCompleted).Take(latestTransfersCount);
            return latestTransfers;
        }

        public IEnumerable<Transfer> GetMostExpensiveTransfers(int mostExpensiveTransfersCount)
        {
            var mostExpensiveTransfers = db.Transfers.Include(x => x.Player)
                                              .Include(x => x.Club)
                                              .Include(x => x.Club1).OrderByDescending(x => x.Amount).Take(mostExpensiveTransfersCount);
            return mostExpensiveTransfers;
        }

        public int GetTransfersCountForPlayer(int playerId)
        {
            return db.Transfers.Count(t => t.PlayerID == playerId);
        }

        public bool IsValidTransferDate(int transferId, int playerId, DateTime newDate)
        {
            int index = 0;
            var playerTransfers = db.Transfers.Where(t => t.PlayerID == playerId).
                                                OrderBy(t => t.DateCompleted).ToList();

            for (int i = 0; i < playerTransfers.Count; i++)
                if (transferId == playerTransfers[i].TransferID)
                {
                    index = i;
                    break;
                }

            int count = playerTransfers.Count;

            if (count == 1)
                return true;
            else if (playerTransfers[0].TransferID == transferId)
            {
                return playerTransfers[1].DateCompleted > newDate;
            }
            else if (playerTransfers[count - 1].TransferID == transferId)
            {
                return playerTransfers[count - 2].DateCompleted < newDate;
            }
            else
            {
                return (playerTransfers[index - 1].DateCompleted < newDate && newDate < playerTransfers[index + 1].DateCompleted);
            }
        }

        public IEnumerable<TransferType> GetTransferTypes()
        {
            return db.TransferTypes.ToList();
        }
    }
}
