﻿using ThesisTU.Repositories.Abstract;
using ThesisTU.Models.Entities;
using System;
using System.Transactions;

namespace ThesisTU.Repositories.Concrete
{
    public class UnitOfWork : IUnitOfWork
    {
        private FootballSystemEntities context = new FootballSystemEntities();
        private IClubsRepository clubsRepository;
        private IPlayersRepository playersRepository;
        private ITransfersRepository transfersRepository;
        private ICountriesRepository countriesRepository;
        private IUsersRepository usersRepository;
        private IArticlesRepository articlesRepository;
        private IWallpostsRepository wallpostsRepository;

        private bool disposed;

        public UnitOfWork() { }

        public IClubsRepository ClubsRepository
        {
            get
            {
                if (this.clubsRepository == null)
                    this.clubsRepository = new ClubsRepository(context);
                return this.clubsRepository;
            }
        }

        public IPlayersRepository PlayersRepository
        {
            get
            {
                if (this.playersRepository == null)
                    this.playersRepository = new PlayersRepository(context);
                return this.playersRepository;
            }
        }

        public ITransfersRepository TransfersRepository
        {
            get
            {
                if (this.transfersRepository == null)
                    this.transfersRepository = new TransfersRepository(context);
                return this.transfersRepository;
            }
        }

        public ICountriesRepository CountriesRepository
        {
            get
            {
                if (this.countriesRepository == null)
                    this.countriesRepository = new CountriesRepository(context);
                return this.countriesRepository;
            }
        }

        public IUsersRepository UsersRepository
        {
            get
            {
                if (this.usersRepository == null)
                    this.usersRepository = new UsersRepository(context);
                return this.usersRepository;
            }
        }

        public IArticlesRepository ArticlesRepository
        {
            get
            {
                if (this.articlesRepository == null)
                    this.articlesRepository = new ArticlesRepository(context);
                return this.articlesRepository;
            }
        }

        public IWallpostsRepository WallpostsRepository
        {
            get
            {
                if (this.wallpostsRepository == null)
                    this.wallpostsRepository = new WallpostsRepository(context);
                return this.wallpostsRepository;
            }
        }

        public TransactionScope CreateTransactionScope(System.Transactions.IsolationLevel level = System.Transactions.IsolationLevel.ReadCommitted)
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = level;
            transactionOptions.Timeout = TransactionManager.MaximumTimeout;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
