﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Repositories.Concrete
{
    public class UsersRepository : IUsersRepository
    {
        private FootballSystemEntities db;

        public UsersRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public UsersRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public User GetByName(string username)
        {
            return db.Users.FirstOrDefault(u => u.Username == username);
        }

        public User GetById(int id)
        {
            return db.Users.SingleOrDefault(e => e.UserId == id);
        }

        public bool UserExists(int id)
        {
            return db.Users.Any(u => u.UserId == id);
        }

        public IEnumerable<int> GetUsersIdsByFavouriteTeams(IEnumerable<int> favTeamIds)
        {
            if (favTeamIds == null || favTeamIds.Count() == 0)
                throw new ArgumentException("You must specify at least one favourite team ID.");

            return db.Users.Where(u => favTeamIds.Contains(u.FavouriteTeamID.Value)).Select(u => u.UserId);
        }

        public void UpdateUnreadArticles(IEnumerable<int> userIds, int articleId) 
        {
            if (userIds == null || userIds.Count() == 0)
                throw new ArgumentException("You must specify at least one user ID.");

            string sqlQuery = "INSERT INTO UsersUnreadArticles VALUES (@userId, @articleId)";

            foreach (int userId in userIds)
            {
                SqlParameter userIdParam = new SqlParameter("@userId", userId);
                SqlParameter articleIdParam = new SqlParameter("@articleId", articleId);

                db.Database.ExecuteSqlCommand(sqlQuery, userIdParam, articleIdParam);
            }
        }

        public void RemoveUnreadArticleEntries(int userId)
        {
            string sqlQuery = "DELETE FROM UsersUnreadArticles WHERE UserID = @userId";

            SqlParameter userIdParam = new SqlParameter("@userId", userId);

            db.Database.ExecuteSqlCommand(sqlQuery, userIdParam);
        }

        public void RemoveUnreadWallpostsEntries(int userId)
        {
            string sqlQuery = "DELETE FROM UsersUnreadWallposts WHERE UserID = @userId";

            SqlParameter userIdParam = new SqlParameter("@userId", userId);

            db.Database.ExecuteSqlCommand(sqlQuery, userIdParam);
        }

        public void RemoveUnreadArticle(int userId, int articleId)
        {
            string sqlQuery = "DELETE FROM UsersUnreadArticles WHERE UserID = @userId AND ArticleID = @articleId";

            SqlParameter userIdParam = new SqlParameter("@userId", userId);
            SqlParameter articleIdParam = new SqlParameter("@articleId", articleId);

            db.Database.ExecuteSqlCommand(sqlQuery, userIdParam, articleIdParam);
        }

        public void RemoveUnreadWallpost(int userId, int wallpostId)
        {
            string sqlQuery = "DELETE FROM UsersUnreadWallposts WHERE UserID = @userId AND WallpostID = @wallpostId";

            SqlParameter userIdParam = new SqlParameter("@userId", userId);
            SqlParameter wallpostIdParam = new SqlParameter("@wallpostId", wallpostId);

            db.Database.ExecuteSqlCommand(sqlQuery, userIdParam, wallpostIdParam);
        }

        public Tuple<bool, bool> GetFriendshipInfo(int user1Id, int user2Id)
        {
            UsersFriendship friendshipInfo = db.UsersFriendships.FirstOrDefault(uf => (uf.UserIDLink1 == user1Id && uf.UserIDLink2 == user2Id) ||
                                                                   (uf.UserIDLink1 == user2Id && uf.UserIDLink2 == user1Id));

            bool hasFriendshipEntry = friendshipInfo != null;
            bool isConfirmedFriendship = hasFriendshipEntry ? friendshipInfo.IsConfirmed : false;

            return Tuple.Create(hasFriendshipEntry, isConfirmedFriendship);
        }

        public void AcceptFriendship(int user1Id, int user2Id)
        {
            UsersFriendship friendship = db.UsersFriendships.FirstOrDefault(uf => uf.UserIDLink1 == user1Id && uf.UserIDLink2 == user2Id);

            if (friendship == null)
                throw new ArgumentException("The users are not friends!");

            friendship.IsConfirmed = true;
        }

        public void CreateFriendship(int user1Id, int user2Id)
        {
            UsersFriendship uf = new UsersFriendship();
            uf.UserIDLink1 = user1Id;
            uf.UserIDLink2 = user2Id;
            uf.IsConfirmed = false;

            db.UsersFriendships.Add(uf);
        }

        public void Add(User user)
        {
            if (user != null)
            {
                db.Users.Add(user);
            }
        }
    }
}
