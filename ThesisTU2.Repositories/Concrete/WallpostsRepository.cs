﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Repositories.Concrete
{
    public class WallpostsRepository : IWallpostsRepository
    {
        private FootballSystemEntities db;

        public WallpostsRepository()
        {
            this.db = new FootballSystemEntities();
        }

        public WallpostsRepository(FootballSystemEntities context)
        {
            this.db = context;
        }

        public IQueryable<Wallpost> GetAll()
        {
            return db.Wallposts;
        }

        public Wallpost GetById(int id)
        {
            var wallpost = db.Wallposts.Include(w => w.WallpostsAnswers).FirstOrDefault(w => w.WallpostID == id);

            return wallpost;
        }

        public void Add(Wallpost wallpost)
        {
            if (wallpost != null)
            {
                db.Wallposts.Add(wallpost);
            }
        }

        public void AddWallpostAnswer(WallpostsAnswer wpa)
        {
            if (wpa != null)
            {
                db.WallpostsAnswers.Add(wpa);
            }
        }

        public void Update(Wallpost wallpost)
        {
            db.Entry(wallpost).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var wallpost = GetById(id);

            if (wallpost == null)
                throw new InvalidOperationException("No wallpost with the id specified was found.");

            var answers = wallpost.WallpostsAnswers;

            foreach (WallpostsAnswer wa in answers)
            {
                db.WallpostsAnswers.Remove(wa);
            }

            db.Wallposts.Remove(wallpost);
        }

        public IEnumerable<Wallpost> GetLatestForUser(int userId, int count, int skip)
        {
            if (count < 0)
                throw new ArgumentException("Wallposts count must be positive.");

            if (skip < 0)
                throw new ArgumentException("Skip parameter must be positive.");

            var wallposts = db.Wallposts.Where(wp => wp.UserID == userId).Include(wp => wp.WallpostsAnswers)
                                        .OrderByDescending(wp => wp.DatePosted)
                                        .Skip(skip)
                                        .Take(count);

            return wallposts;
        }

        public IEnumerable<WallpostsAnswer> GetWallpostAnswers(int wallpostId, int count, int skip)
        {
            if (count < 0)
                throw new ArgumentException("Wallposts count must be positive.");

            if (skip < 0)
                throw new ArgumentException("Skip parameter must be positive.");

            var answers = db.WallpostsAnswers.Where(wpa => wpa.WallpostID == wallpostId)
                                             .OrderByDescending(wpa => wpa.DatePosted)
                                             .Skip(skip)
                                             .Take(count);

            return answers;
        }

        public int CountForUser(int userId)
        {
            return db.Wallposts.Count(wp => wp.UserID == userId);
        }
    }
}
