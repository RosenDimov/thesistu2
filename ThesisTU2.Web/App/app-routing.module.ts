import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CanActivateGuard } from './can-activate.guard';
import { SelectivePreloadingStrategy } from './selective-preloading-strategy';
import { SuccessComponent } from './success.component';
import { PageNotFoundComponent } from './not-found.component';
import { UnauthorizedComponent } from './unauthorized.component';
import { RemovedComponent } from './removed.component';

const appRoutes: Routes = [
    { path: 'profile', loadChildren: './profiles/profiles.module#ProfilesModule', data: { preload: true } },
    { path: 'clubs', loadChildren: './clubs/clubs.module#ClubsModule' },
    { path: 'players', loadChildren: './players/players.module#PlayersModule' },
    { path: 'transfers', loadChildren: './transfers/transfers.module#TransfersModule' },
    { path: 'success', component: SuccessComponent },
    { path: 'unauthorized', component: UnauthorizedComponent },
    { path: 'removed', component: RemovedComponent },
    { path: '', redirectTo: '/latest', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];
@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, { preloadingStrategy: SelectivePreloadingStrategy })
    ],
    exports: [
        RouterModule
    ],
    providers: [
        CanActivateGuard,
        SelectivePreloadingStrategy
    ]
})
export class AppRoutingModule { }