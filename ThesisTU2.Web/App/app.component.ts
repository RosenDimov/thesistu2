import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from './core/auth.service';
import { SignalRService } from './core/signalr.service';
import { NotificationsService } from './business-services/notifications.service';
import { NotificationsManagerService } from './core/notifications-manager.service';
import { UserContext } from './core/models/user-context';

@Component({
    selector: 'football-app',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy {

    notificationsCount = 0;
    userContext: UserContext;
    notificationSubscription: Subscription;
    notificationsCountChangeSubscription: Subscription;
    friendRequestSubscription: Subscription;

    constructor(private authService: AuthService,
        private signalRService: SignalRService,
        private notificationsService: NotificationsService,
        private notificationsManagerService: NotificationsManagerService) {
        this.userContext = authService.getUserContext();
    }

    ngOnInit() {
        this.signalRService.initialize();
        this.getNotificationsCount();
        this.notificationSubscription = this.signalRService.notificationAvailable.subscribe(() => {
            this.notificationsCount++;
        });
        this.notificationsCountChangeSubscription =
            this.notificationsManagerService.notificationsCountChange.subscribe(() => {
                this.notificationsCount--;
            });
        this.friendRequestSubscription =
            this.signalRService.friendRequestAvailable.subscribe(() => {
                this.notificationsCount++;
            });
    }

    ngOnDestroy() {
        this.notificationSubscription.unsubscribe();
        this.notificationsCountChangeSubscription.unsubscribe();
        this.friendRequestSubscription.unsubscribe();
    }

    getNotificationsCount() {
        this.notificationsService.getNewCount().subscribe((count) => {
            this.notificationsCount = count;
        }, (error) => {
            alert(error);
            this.notificationsCount = 0;
        });
    }
}
