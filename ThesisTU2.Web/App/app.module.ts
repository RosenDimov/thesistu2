import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { BusinessServicesModule } from './business-services/business-services.module';
import { ArticlesModule } from './articles/articles.module';
import { AppComponent } from './app.component';
import { SuccessComponent } from './success.component';
import { PageNotFoundComponent } from './not-found.component';
import { UnauthorizedComponent } from './unauthorized.component';
import { RemovedComponent } from './removed.component';

@NgModule({
    imports: [
        BrowserModule,
        HttpClientModule,
        ArticlesModule,
        CoreModule,
        BusinessServicesModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent,
        SuccessComponent,
        PageNotFoundComponent,
        UnauthorizedComponent,
        RemovedComponent
    ],
    bootstrap: [ AppComponent ],
    exports: [],
    providers: [ DatePipe ]
})
export class AppModule { }
