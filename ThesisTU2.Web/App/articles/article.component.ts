﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from '../core/auth.service';
import { SignalRService } from '../core/signalr.service';
import { ArticlesService } from '../business-services/articles.service';
import { Article } from '../business-services/models/article';
import { ArticleInfo } from '../business-services/models/article-info';

@Component({
    templateUrl: './article.component.html',
})
export class ArticleComponent implements OnInit, OnDestroy {
    
    articleCommentAvaiableSubscription: Subscription;
    article: Article;
    newComment = "";
    id: number;
    commentMaxLength: number;
    hasMoreComments = true;
    totalCommentsCount: number;
    skipCommentsCount = 0;

    constructor(private authService: AuthService,
        private signalRService: SignalRService,
        private articesService: ArticlesService,
        private router: Router,
        route: ActivatedRoute) {
        this.article = new Article();
        this.id = +route.snapshot.params['id'];
        this.commentMaxLength = this.authService.getMaxCommentLength();
    }

    ngOnInit() {
        this.getArticle();
        this.subscribeForNewArticleComment();
    }

    ngOnDestroy() {
        this.articleCommentAvaiableSubscription.unsubscribe();
    }

    addComment() {
        this.articesService.addArticleComment(this.newComment, this.id).subscribe(() => {
            this.newComment = "";
        });
    }

    getComments() {
        this.articesService.getArticleComments(this.id, this.skipCommentsCount).subscribe((comments) => {
            this.toggleMoreCommentsButton(comments.length);

            if (comments.length > 0) {
                for (let comment of comments) {
                    this.article.Comments.splice(0, 0, comment);
                }
            }
        })
    }

    getArticle() {
        this.articesService.getArticle(this.id).subscribe((data) => {
            this.article = data.Article;
            this.totalCommentsCount = data.TotalCount;
            this.toggleMoreCommentsButton(data.Article.Comments.length);
            console.log('inside getArticle()');
        }, (error) => {
            this.router.navigate(['/not-found']);
        })
    }

    toggleMoreCommentsButton(dataLength: number) {
        this.skipCommentsCount += dataLength;

        if (dataLength == 0 || this.skipCommentsCount >= this.totalCommentsCount) {
            this.hasMoreComments = false;
        }
    }

    private subscribeForNewArticleComment() {
        this.articleCommentAvaiableSubscription =
            this.signalRService.articleCommentAvailable.subscribe((articleComment) => {
                if (articleComment !== null && typeof articleComment !== 'undefined' && this.id == articleComment.ArticleID) { // without last check it can post to every article
                    this.article.Comments.push(articleComment);
                    this.updateArticleAnswersCount();
                }
            })
    }

    private updateArticleAnswersCount() {
        this.skipCommentsCount++;
        this.totalCommentsCount++;
    }
}