﻿import { Component, OnInit } from '@angular/core';

import { AuthService } from '../core/auth.service';
import { ArticlesService } from '../business-services/articles.service';
import { Article } from '../business-services/models/article';

@Component({
    templateUrl: './articles-list.component.html',
})
export class ArticlesListComponent implements OnInit {

    indexItems: number;
    articles: Article[];

    constructor(authService: AuthService,
        private articesService: ArticlesService) {
        this.indexItems = authService.getIndexPageItemsCount();
    }

    ngOnInit() {
        this.articesService.getLatestArticles(this.indexItems).subscribe((articles => {
            this.articles = articles;
        }));
    }
}