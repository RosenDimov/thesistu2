import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlesListComponent } from './articles-list.component';
import { NewArticleComponent } from './new-article.component';
import { ArticleComponent } from './article.component';
import { SearchArticlesComponent } from './search-articles.component';

const articlesRoutes: Routes = [
    { path: 'latest', component: ArticlesListComponent },
    { path: 'articles/new', component: NewArticleComponent },
    { path: 'articles/search', component: SearchArticlesComponent },
    { path: 'articles/:id', component: ArticleComponent },
];
@NgModule({
    imports: [
        RouterModule.forChild(articlesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ArticlesRoutingModule {
    
}
