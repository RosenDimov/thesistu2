import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ArticlesRoutingModule } from './articles-routing.module';
import { ArticlesListComponent } from './articles-list.component';
import { NewArticleComponent } from './new-article.component';
import { ArticleComponent } from './article.component';
import { SearchArticlesComponent } from './search-articles.component';

@NgModule({
    imports: [
        ArticlesRoutingModule,
        SharedModule
    ],
    declarations: [
        ArticlesListComponent,
        NewArticleComponent,
        ArticleComponent,
        SearchArticlesComponent
    ],
    exports: [
    ]
})
export class ArticlesModule { }
