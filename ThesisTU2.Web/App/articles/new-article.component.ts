﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../core/auth.service';
import { ArticlesService } from '../business-services/articles.service';
import { Article } from '../business-services/models/article';

@Component({
    moduleId: module.id,
    templateUrl: './new-article.component.html',
})
export class NewArticleComponent {

    newArticle: Article;
    teams: string;
    errMessage: string;
    hasServerError: boolean;

    constructor(private articesService: ArticlesService,
        private router: Router) {
        this.newArticle = new Article();
    }

    create() {
        if (this.teams) {
            this.newArticle.ConnectedTeamsIds = this.teams.split(',').map(id => parseInt(id));
        }
        else {
            alert('You must enter at least one team.');
            return;
        }

        this.articesService.addArticle(this.newArticle).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.errMessage = response.error.Message;
            this.hasServerError = true;
            this.newArticle.ConnectedTeamsIds = [];
        });
    }
}