﻿import { Component } from '@angular/core';

import { ArticlesService } from '../business-services/articles.service';
import { Article } from '../business-services/models/article';
import { SearchArticlesObject } from '../business-services/models/search-articles-object';

@Component({
    moduleId: module.id,
    templateUrl: './search-articles.component.html',
})
export class SearchArticlesComponent {

    results: Article[];
    search: SearchArticlesObject;
    hasServerError: boolean;
    errMessage: string;

    constructor(private articesService: ArticlesService) {
        this.search = new SearchArticlesObject();
    }

    searchArticles() {
        this.articesService.searchArticles(this.search).subscribe((data) => {
            this.results = data;
            this.hasServerError = false;
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    setServerError(msg: string) {
        this.hasServerError = true;
        this.errMessage = msg;
    }
}