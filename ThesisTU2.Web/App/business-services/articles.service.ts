﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Article } from './models/article';
import { ArticleComment } from './models/article-comment';
import { ArticleInfo } from './models/article-info';
import { SearchArticlesObject } from './models/search-articles-object';

@Injectable()
export class ArticlesService {

    baseUrl = 'api/articles';

    constructor(private http: HttpClient) { }

    addArticle(article: Article): Observable<Object> {
        return this.http.post(`${this.baseUrl}/`, article);
    }

    getLatestArticles(count: number): Observable<Article[]> {
        return this.http.get<Article[]>(`${this.baseUrl}/latest/${count}`);
    }

    getArticle(id: number): Observable<ArticleInfo> {
        return this.http.get<ArticleInfo>(`${this.baseUrl}/byid/${id}`);
    }

    addArticleComment(comment: string, articleId: number): Observable<Object> {
        return this.http.post(`${this.baseUrl}/comment`, { Text: comment, ArticleID: articleId });
    }

    getArticleComments(articleId: number, skip: number): Observable<ArticleComment[]> {
        return this.http.get<ArticleComment[]>(`api/article/comments/${articleId}/${skip}`);
    }

    searchArticles(searchObject: SearchArticlesObject): Observable<Article[]> {
        return this.http.post<Article[]>(`${this.baseUrl}/search`, searchObject);
    }
}