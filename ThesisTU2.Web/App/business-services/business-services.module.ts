import { NgModule } from '@angular/core';

import { ArticlesService } from './articles.service';
import { WallpostsService } from './wallposts.service';
import { UsersService } from './users.service';
import { ClubsService } from './clubs.service';
import { PlayersService } from './players.service';
import { TransfersService } from './transfers.service';
import { NomsService } from './noms.service';
import { NotificationsService } from './notifications.service';
import { FileUploadService } from './file-upload.service';

@NgModule({
    providers: [
        ArticlesService,
        WallpostsService,
        UsersService,
        ClubsService,
        PlayersService,
        TransfersService,
        NomsService,
        NotificationsService,
        FileUploadService
    ]
})
export class BusinessServicesModule { }
