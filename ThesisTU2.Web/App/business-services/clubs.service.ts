﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Club } from './models/club';
import { ClubWithNews } from './models/clubwithnews';

@Injectable()
export class ClubsService {

    baseUrl = 'api/clubs';

    constructor(private http: HttpClient) { }

    getClubsByTerm(term: string): Observable<Club[]> {
        return this.http.get<Club[]>(`${this.baseUrl}/byterm/${term}`);
    }

    getClub(id: number): Observable<Club> {
        return this.http.get<Club>(`${this.baseUrl}/byid/${id}`);
    }

    getClubWithNews(id: number): Observable<ClubWithNews> {
        return this.http.get<ClubWithNews>(`${this.baseUrl}/byidwithnews/${id}`);
    }

    addClub(club: Club): Observable<Object> {
        return this.http.post(`${this.baseUrl}`, club);
    }

    putClub(club: Club): Observable<Object> {
        return this.http.put(`${this.baseUrl}`, club);
    }
}