﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

@Injectable()
export class FileUploadService {

    constructor(private http: HttpClient) { }

    uploadFile(file: File, url: string): Observable<Response> {

        if (!file)
            return null;

        let formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');

        return this.http.post<Response>(url, formData, { headers: headers });
    }
}