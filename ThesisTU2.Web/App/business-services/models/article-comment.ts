﻿export class ArticleComment {
    ArticleCommentID: number;
    ArticleID: number;
    Text: string;
    DatePosted: Date;
    Author: string;
    AuthorID: number;
}