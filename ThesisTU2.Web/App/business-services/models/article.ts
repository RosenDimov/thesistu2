﻿import { ArticleComment } from './article-comment';

export class Article {
    ArticleID: number;
    Text: string;
    Title: string;
    DateAdded: Date;
    Comments: ArticleComment[];
    ConnectedTeams: string[];
    ConnectedTeamsIds: number[];
}