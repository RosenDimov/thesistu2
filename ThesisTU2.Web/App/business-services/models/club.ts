﻿export class Club {
    ClubID: number;
    ClubName: string;
    YearFounded: number;
    CountryName: string;
    CountryID: number;
    Manager: string;
    Ground: string;
    TrophiesCount: number;
    LogoPath: string;
}