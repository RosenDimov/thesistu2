﻿import { Club } from './club';
import { Article } from './article';

export class ClubWithNews {
    Club: Club;
    Articles: Article[];
}