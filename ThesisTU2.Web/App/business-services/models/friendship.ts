﻿export class Friendship {
    RequestSenderName: string;
    RequestSenderID: number;
    User1ID: number;
    User2ID: number;
}