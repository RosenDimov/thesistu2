﻿import { Article } from './article';
import { Wallpost } from './wallpost';
import { Friendship } from './friendship';

export class NotificationsInfo {
    Articles: Article[];
    Wallposts: Wallpost[];
    FriendRequests: Friendship[];
}