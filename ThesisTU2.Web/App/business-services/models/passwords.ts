﻿export class Passwords {
    NewPassword: string;
    NewPasswordRepeat: string;
    OldPassword: string;
}