﻿export class Player {
     PlayerID: number;
     ClubID: number;
     FirstName: string;
     LastName: string;
     Age: number;
     ClubName: string;
     CountryID: number;
     CountryName: string;
     PositionID: number;
     PositionName: string;
     BirthDate: string;
     Number: number;
     Goals: number;
     PicturePath: string;
}