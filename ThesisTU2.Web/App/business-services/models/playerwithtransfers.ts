﻿import { Player } from './player';
import { Transfer } from './transfer';

export class PlayerWithTransfers {
    Player: Player;
    Transfers: Transfer[];
}