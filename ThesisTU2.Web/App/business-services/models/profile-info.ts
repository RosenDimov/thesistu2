﻿import { Article } from './article';
import { UserInfo } from './user-info';

export class ProfileInfo {
    UserInfo: UserInfo;
    LatestNews: Article[];
}