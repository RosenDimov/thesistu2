﻿export class SearchArticlesObject {
    Title: string;
    Content: string;
    StartDate: string;
    EndDate: string;
}