﻿export class Transfer {
    TransferID: number;
    PlayerID: number;
    PlayerName: string;
    SellerName: string;
    BuyerName: string;
    Amount: number;
    DateCompleted: string;
    TransferTypeID: number;
    Title: string;
}