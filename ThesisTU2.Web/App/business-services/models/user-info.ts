﻿import { ArticleComment } from './article-comment';

export class UserInfo {
    UserId: number;
    Username: string;
    Fullname: string;
    AvatarPath: string;
    ThumbPath: string;
    DeletePreviousAvatar: boolean;
    FavouriteTeam: string;
    CountryName: string;
    CountryID: number;
    LatestArticleComments: ArticleComment[];
}