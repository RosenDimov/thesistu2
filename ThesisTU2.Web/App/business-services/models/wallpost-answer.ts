﻿export class WallpostAnswer {
    WallpostAnswerID: number;
    Text: string;
    DatePosted: Date;
    AuthorID: number;
    AuthorName: string;
    AuthorThumbImage: string;
    WallpostID: number;
}