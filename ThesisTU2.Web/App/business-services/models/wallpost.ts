﻿import { WallpostAnswer } from './wallpost-answer';

export class Wallpost {
    WallpostID: number;
    Text: string;
    DatePosted: Date;
    AuthorID: number;
    AuthorName: string;
    AuthorThumbImage: string;
    UserID: number;
    AnswersCount: number;
    WallpostsAnswers: WallpostAnswer[];
    show: boolean;
}