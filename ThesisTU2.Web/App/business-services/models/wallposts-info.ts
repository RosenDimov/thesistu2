﻿import { Wallpost } from './wallpost';

export class WallpostsInfo {
    Wallposts: Wallpost[];
    WallpostsCount: number;
}