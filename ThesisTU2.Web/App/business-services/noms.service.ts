﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Country } from './models/country';
import { Position } from './models/position';
import { TransferType } from './models/transfer-type';

@Injectable()
export class NomsService {

    baseUrl = 'api/noms';

    constructor(private http: HttpClient) { }

    getCountries(): Observable<Country[]> {
        return this.http.get<Country[]>(`${this.baseUrl}/countries/`);
    }

    getPlayerPositions(): Observable<Position[]> {
        return this.http.get<Position[]>(`${this.baseUrl}/positions/`);
    }

    getTransferTypes(): Observable<TransferType[]> {
        return this.http.get<TransferType[]>(`${this.baseUrl}/transfertypes/`);
    }
}