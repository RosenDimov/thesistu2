﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { NotificationsInfo } from './models/notifications-info';

@Injectable()
export class NotificationsService {

    baseUrl = 'api/notifications';

    constructor(private http: HttpClient) { }

    getNewCount(): Observable<number> {
        return this.http.get<number>(`${this.baseUrl}/countforuser/`);
    }

    getUnreadNotifications(): Observable<NotificationsInfo> {
        return this.http.get<NotificationsInfo>(`${this.baseUrl}/unread/`);
    }

    clearUnreadNotifications(): Observable<Object> {
        return this.http.get(`${this.baseUrl}/removeforuser/`);
    }

    deleteUnreadNotification(id: number): Observable<Object> {
        return this.http.delete(`${this.baseUrl}/deleteunreadarticle/${id}`);
    }

    deleteUnreadWallpost(id: number): Observable<Object> {
        return this.http.delete(`${this.baseUrl}/deleteunreadwallpost/${id}`);
    }

    acceptFriendRequest(id: number): Observable<Object> {
        return this.http.get(`${this.baseUrl}/acceptfriend/${id}`);
    }
}