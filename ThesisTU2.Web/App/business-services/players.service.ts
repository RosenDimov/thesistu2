﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Player } from './models/player';
import { PlayerWithTransfers } from './models/playerwithtransfers';

@Injectable()
export class PlayersService {

    baseUrl = 'api/players';

    constructor(private http: HttpClient) { }

    getPlayer(id: number): Observable<Player> {
        return this.http.get<Player>(`${this.baseUrl}/byid/${id}`);
    }

    getPlayerWithTransfers(id: number): Observable<PlayerWithTransfers> {
        return this.http.get<PlayerWithTransfers>(`${this.baseUrl}/byidwithtransfers/${id}`);
    }

    getPlayersByName(term: string): Observable<Player[]> {
        return this.http.get<Player[]>(`${this.baseUrl}/byname/${term}`);
    }

    getPlayersByNameAndPosition(term: string, positionID: number): Observable<Player[]> {
        return this.http.get<Player[]>(`${this.baseUrl}/bynamespos/${term}/${positionID}`);
    }

    addPlayer(player: Player): Observable<Object> {
        return this.http.post(`${this.baseUrl}`, player);
    }

    putPlayer(player: Player): Observable<Object> {
        return this.http.put(`${this.baseUrl}`, player);
    }
}