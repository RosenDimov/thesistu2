﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Transfer } from './models/transfer';

@Injectable()
export class TransfersService {

    baseUrl = 'api/transfers';

    constructor(private http: HttpClient) { }

    getTransfer(transferID: number): Observable<Transfer> {
        return this.http.get<Transfer>(`${this.baseUrl}/byid/${transferID}`);
    }

    getTransfersByPlayer(term: string): Observable<Transfer[]> {
        return this.http.get<Transfer[]>(`${this.baseUrl}/byplayer/${term}`);
    }

    getTransfersByTeam(term: string): Observable<Transfer[]> {
        return this.http.get<Transfer[]>(`${this.baseUrl}/byteam/${term}`);
    }

    getTransfersByTeamBuyer(term: string): Observable<Transfer[]> {
        return this.http.get<Transfer[]>(`${this.baseUrl}/byteambuyer/${term}`);
    }

    getTransfersByTeamSeller(term: string): Observable<Transfer[]> {
        return this.http.get<Transfer[]>(`${this.baseUrl}/byteamseller/${term}`);
    }

    addTransfer(transfer: Transfer): Observable<Object> {
        return this.http.post(`${this.baseUrl}`, transfer);
    }

    putTransfer(transfer: Transfer): Observable<Object> {
        return this.http.put(`${this.baseUrl}`, transfer);
    }

    deleteTransfer(id: number): Observable<Object> {
        return this.http.delete(`${this.baseUrl}/${id}`);
    }
}