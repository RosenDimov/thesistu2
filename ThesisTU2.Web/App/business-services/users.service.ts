﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { UserInfo } from './models/user-info';
import { Passwords } from './models/passwords';
import { FriendshipInfo } from './models/friendship-info';
import { Friendship } from './models/friendship';

@Injectable()
export class UsersService {

    baseUrl = 'api/users';

    constructor(private http: HttpClient) { }

    getCurrentUser(): Observable<UserInfo> {
        return this.http.get<UserInfo>(`${this.baseUrl}/current`);
    }

    putUser(user: UserInfo): Observable<Object> {
        return this.http.put(`${this.baseUrl}/`, user);
    }

    changeUserPassword(passwords: Passwords): Observable<Object> {
        return this.http.post(`${this.baseUrl}/`, passwords);
    };

    getLoggedUserId(): Observable<number> {
        return this.http.get<number>('api/loggedid/');
    }

    getFriendshipInfo(user1ID: number, user2ID: number): Observable<FriendshipInfo> {
        return this.http.get<FriendshipInfo>(`${this.baseUrl}/friendship/${user1ID}/${user2ID}`);
    }

    sentFriendshipRequest(friendshipInfo: any): Observable<Object> {
        return this.http.post(`${this.baseUrl}/newfriendship`, friendshipInfo);
    }
}