﻿import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import '../rxjs-operators';

import { Wallpost } from './models/wallpost';
import { WallpostAnswer } from './models/wallpost-answer';
import { ProfileInfo } from './models/profile-info';
import { WallpostsInfo } from './models/wallposts-info';

@Injectable()
export class WallpostsService {

    baseUrl = 'api/profile';

    constructor(private http: HttpClient) { }

    getWallpost(id: number): Observable<Wallpost> {
        return this.http.get<Wallpost>(`${this.baseUrl}/wallpost/${id}`);
    }

    getWallposts(userId: number, skip: number): Observable<Wallpost[]> {
        return this.http.get<Wallpost[]>(`${this.baseUrl}/wallposts/${userId}/${skip}`);
    }

    getWallpostAnswers(wallpostId: number, skip: number): Observable<WallpostAnswer[]> {
        return this.http.get<WallpostAnswer[]>(`${this.baseUrl}/wallpostanswers/${wallpostId}/${skip}`);
    }

    getProfileInfo(id: number): Observable<ProfileInfo> {
        return this.http.get<ProfileInfo>(`${this.baseUrl}/byid/${id}`);
    }

    getProfileWallposts(id: number): Observable<WallpostsInfo> {
        return this.http.get<WallpostsInfo>(`${this.baseUrl}/wallpostsinfo/${id}`);
    }

    addWallpost(wallpost: Wallpost): Observable<Object> {
        return this.http.post(`${this.baseUrl}/wallpost`, wallpost);
    }

    addWallpostAnswer(wallpostAnswer: any): Observable<Object> {
        return this.http.post(`${this.baseUrl}/wallpostanswer`, wallpostAnswer);
    }
}