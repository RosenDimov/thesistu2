import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';

import { AuthService } from './core/auth.service';

@Injectable()
export class CanActivateGuard implements CanActivate {

    constructor(private router: Router,
        private authService: AuthService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        let permission = route.data['permission'];

        if (!permission)
            return true;

        let hasPermission = this.authService.hasPermission(permission);

        if (!hasPermission) {
            this.router.navigate(['/unauthorized']);
        }

        return hasPermission;
    }
}
