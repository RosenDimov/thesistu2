﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Club } from '../business-services/models/club';
import { Article } from '../business-services/models/article';
import { ClubsService } from '../business-services/clubs.service';

@Component({
    templateUrl: './club.component.html',
})
export class ClubComponent implements OnInit {
    
    club: Club;
    articles: Article[];

    constructor(private clubsService: ClubsService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.getClub();
    }

    private getClub() {
        let id = parseInt(this.route.snapshot.params["id"]);

        this.clubsService.getClubWithNews(id).subscribe((data) => {
            this.club = data.Club;
            this.articles = data.Articles;
        }, () => {
            this.router.navigate(["/notfound"]);
        });
    }
}