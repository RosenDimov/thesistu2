import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewClubComponent } from './new-club.component';
import { SearchClubsComponent } from './search-clubs.component';
import { ClubComponent } from './club.component';
import { CanActivateGuard } from '../can-activate.guard';

const clubsRoutes: Routes = [
    { path: 'new', data: { permission: 'change', isEditMode: false }, canActivate: [CanActivateGuard], component: NewClubComponent },
    { path: 'edit/:id', data: { permission: 'change', isEditMode: true }, canActivate: [CanActivateGuard], component: NewClubComponent },
    { path: 'search', component: SearchClubsComponent },
    { path: ':id', component: ClubComponent },
];
@NgModule({
    imports: [
        RouterModule.forChild(clubsRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ClubsRoutingModule { }