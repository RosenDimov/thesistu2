import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ClubsRoutingModule } from './clubs-routing.module';
import { NewClubComponent } from './new-club.component';
import { SearchClubsComponent } from './search-clubs.component';
import { ClubComponent } from './club.component';

@NgModule({
    imports: [
        ClubsRoutingModule,
        SharedModule
    ],
    declarations: [
        NewClubComponent,
        SearchClubsComponent,
        ClubComponent
    ],
    exports: [
    ]
})
export class ClubsModule { }
