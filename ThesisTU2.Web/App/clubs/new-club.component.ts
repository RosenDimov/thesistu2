﻿import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Country } from '../business-services/models/country';
import { Club } from '../business-services/models/club';
import { ClubsService } from '../business-services/clubs.service';
import { NomsService } from '../business-services/noms.service';
import { FileUploadService } from '../business-services/file-upload.service';

@Component({
    templateUrl: './new-club.component.html'
})
export class NewClubComponent implements OnInit {

    countries: Country[];
    newClub: Club;
    isEdit: boolean;
    hasServerError: boolean;
    errMessage: string;
    file: File = null;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private nomsService: NomsService,
        private clubsService: ClubsService,
        private fileUploadService: FileUploadService) {

        this.newClub = new Club();
        this.isEdit = this.route.snapshot.data['isEditMode'];
    }

    ngOnInit() {
        this.getCountries();
    }

    save() {
        if (this.file !== null) {
            this.uploadFile().subscribe((data) => {
                this.newClub.LogoPath = data.json().Filename;
                this.saveClub();
            }, (response) => {
                this.setServerError(response.error.Message);
            });
        }
        else {
            this.saveClub();
        }
    }

    private uploadFile(): Observable<Response> {
        return this.fileUploadService.uploadFile(this.file, "api/files/clubs");
    }

    //setUploadFile(event: any) {
    //    this.file = event.target.files[0];
    //}

    private getCountries() {
        this.nomsService.getCountries().subscribe((data) => {
            this.countries = data;
            this.newClub.CountryID = data[0].CountryID;

            if (this.isEdit) {
                this.getExistingClub();
            }
        });
    }

    private getExistingClub() {
        let id = parseInt(this.route.snapshot.params['id']);

        if (typeof id === 'number' && !isNaN(id)) {
            this.clubsService.getClub(id).subscribe((data) => {
                this.newClub = data;
                this.newClub.LogoPath = null;
            }, (response) => {
                this.setServerError(response.error.Message);
            })
        }
        else {
            this.router.navigate(['/']); 
        }
    }

    saveClub() {
        if (this.isEdit) {
            this.updateClub(this.newClub);
        }
        else {
            this.createClub(this.newClub);
        }
    }

    private updateClub(club: Club) {
        this.clubsService.putClub(club).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private createClub(club: Club) {
        this.clubsService.addClub(club).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private setServerError(message: string) {
        this.hasServerError = true;
        this.errMessage = message;
    }
}