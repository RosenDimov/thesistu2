﻿import { Component } from '@angular/core';

import { Club } from '../business-services/models/club';
import { ClubsService } from '../business-services/clubs.service';

@Component({
    templateUrl: './search-clubs.component.html'
})
export class SearchClubsComponent {

    searchTerm: string;
    results: Club[];

    constructor(private clubsService: ClubsService) {
    }

    search() {
        this.clubsService.getClubsByTerm(this.searchTerm).subscribe((data) => {
            this.results = data;
        }, (response) => {
            alert(response.error.Message);
        });
    }
}