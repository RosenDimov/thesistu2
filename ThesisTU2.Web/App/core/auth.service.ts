﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

import { UserContext } from './models/user-context';

@Injectable()
export class AuthService {

    private userContext: UserContext;
    private permissionsSubject = new Subject();
    permissionsAvailable = this.permissionsSubject.asObservable();

    constructor() {
        this.userContext = <UserContext>window["config"];
    }

    getUserContext(): UserContext {
        return this.userContext;
    }

    getIndexPageItemsCount(): number {
        let config = this.userContext.Config;
        return config.IndexItems;
    }

    getMaxCommentLength(): number {
        let config = this.userContext.Config;
        return config.MaxCommentLength;
    }

    setPermissions(permissions: string[]) {
        this.userContext.Config.Permissions = permissions;
        this.permissionsSubject.next(permissions);
    }

    hasPermission(permission: string): boolean {
        permission = permission.trim();
        let permissions = this.userContext.Config.Permissions;
        let hasPermission = permissions.some(item => {
            return item.trim().indexOf(permission) !== -1;
        });

        return hasPermission;
    }
}