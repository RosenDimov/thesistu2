import { NgModule } from '@angular/core';

import { AuthService } from './auth.service';
import { SignalRService } from './signalr.service';
import { NotificationsManagerService } from './notifications-manager.service';

@NgModule({
    providers: [
        AuthService,
        SignalRService,
        NotificationsManagerService
    ]
})
export class CoreModule { }
