﻿export class Settings {
    Permissions: string[];
    IndexItems: number;
    MaxCommentLength: number;
    DefaultThumbImagePath: string;
    DefaultOriginalImagePath: string;
}