﻿import { Settings } from './settings';

export class UserContext {
    Config: Settings;
    IsUserAdmin: boolean;
    Username: string;
    UserId: number;
}