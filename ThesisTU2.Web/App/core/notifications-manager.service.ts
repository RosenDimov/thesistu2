﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class NotificationsManagerService {
    
    private notificationsCountChangeSubject = new Subject();
    notificationsCountChange = this.notificationsCountChangeSubject.asObservable();

    constructor() { }

    decreaseNotificationsCount() {
        this.notificationsCountChangeSubject.next();
    }
}