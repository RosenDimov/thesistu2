﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';

import { Wallpost } from '../business-services/models/wallpost';
import { WallpostAnswer } from '../business-services/models/wallpost-answer';
import { ArticleComment } from '../business-services/models/article-comment';

@Injectable()
export class SignalRService {

    private isInitialized = false;
    private notificationSource = new Subject();
    private wallpostSource = new Subject<Wallpost>();
    private wallpostAnswerSource = new Subject<WallpostAnswer>();
    private friendRequestSource = new Subject();
    private articleCommentSource = new Subject<ArticleComment>();

    notificationAvailable = this.notificationSource.asObservable();
    wallpostAvailable = this.wallpostSource.asObservable();
    wallpostAnswerAvailable = this.wallpostAnswerSource.asObservable();
    friendRequestAvailable = this.friendRequestSource.asObservable();
    articleCommentAvailable = this.articleCommentSource.asObservable();

    constructor() { }

    initialize() {
        if (this.isInitialized) {
            return;
        }

        let fhub = $.connection.footballHub;

        fhub.client.notifyUpdate = () => {
            this.notificationSource.next();
        }

        fhub.client.notifyWallpost = (wallpost: Wallpost) => {
            this.wallpostSource.next(wallpost);
        }

        fhub.client.notifyWallpostAnswer = (wallpostAnswer: WallpostAnswer) => {
            this.wallpostAnswerSource.next(wallpostAnswer);
        }

        fhub.client.notifyFriendRequest = () => {
            this.friendRequestSource.next();
        }

        fhub.client.notifyArticleComment = (articleComment: ArticleComment) => {
            this.articleCommentSource.next(articleComment);
        }

        $.connection.hub.start();
        this.isInitialized = true;
    }
}