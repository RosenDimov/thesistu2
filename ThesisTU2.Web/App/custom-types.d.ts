﻿interface SignalR {
    footballHub: any;
}

interface JQuery {
    tokenInput(sourceUrl: string, config: any): void;
    modal(option: string): any;
}