import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';

var browserPlatform = platformBrowserDynamic();
browserPlatform.bootstrapModule(AppModule);