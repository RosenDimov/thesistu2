import { Component } from '@angular/core';

@Component({
  template: `<div class="alert alert-danger">
                 <p>Page not found.</p>
             </div>`,
})
export class PageNotFoundComponent  {
}
