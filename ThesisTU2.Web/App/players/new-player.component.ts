﻿import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Country } from '../business-services/models/country';
import { Position } from '../business-services/models/position';
import { Player } from '../business-services/models/player';
import { PlayersService } from '../business-services/players.service';
import { NomsService } from '../business-services/noms.service';
import { FileUploadService } from '../business-services/file-upload.service';

@Component({
    templateUrl: './new-player.component.html'
})
export class NewPlayerComponent implements OnInit {

    countries: Country[];
    positions: Position[];
    newPlayer: Player;
    isEdit: boolean;
    hasServerError: boolean;
    errMessage: string;
    file: File = null;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private nomsService: NomsService,
        private fileUploadService: FileUploadService,
        private playersService: PlayersService,
        private datePipe: DatePipe) {

        this.newPlayer = new Player();
        this.isEdit = this.route.snapshot.data['isEditMode'];
    }

    ngOnInit() {
        this.getCountries();
    }

    save() {
        if (this.file !== null) {
            this.uploadFile().subscribe((data) => {
                this.newPlayer.PicturePath = data.Filename;
                this.savePlayer();
            }, (response) => {
                this.setServerError(response.error.Message);
            });
        }
        else {
            this.savePlayer();
        }
    }

    private uploadFile(): Observable<any> {
        return this.fileUploadService.uploadFile(this.file, "api/files/players");
    }

    private getCountries() {
        this.nomsService.getCountries().subscribe((data) => {
            this.countries = data;
            this.newPlayer.CountryID = data[0].CountryID;
            this.getPlayersPositions();
        });
    }

    private getPlayersPositions() {
        this.nomsService.getPlayerPositions().subscribe((positions) => {
            this.positions = positions;
            this.newPlayer.PositionID = this.positions[0].PositionID;

            if (this.isEdit) {
                this.getExistingPlayer();
            }
        })
    }

    private getExistingPlayer() {
        let id = parseInt(this.route.snapshot.params['id']);

        if (typeof id === 'number' && !isNaN(id)) {
            this.playersService.getPlayer(id).subscribe((data) => {
                this.newPlayer = data;
                this.newPlayer.BirthDate = this.datePipe.transform(this.newPlayer.BirthDate, 'MM/dd/yyyy');
                this.newPlayer.PicturePath = null;
            }, (response) => {
                this.setServerError(response.error.Message);
            })
        }
        else {
            this.router.navigate(['/']);
        }
    }

    savePlayer() {
        if (this.isEdit) {
            this.updatePlayer(this.newPlayer);
        }
        else {
            this.createPlayer(this.newPlayer);
        }
    }

    private updatePlayer(player: Player) {
        this.playersService.putPlayer(player).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private createPlayer(player: Player) {
        this.playersService.addPlayer(player).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private setServerError(message: string) {
        this.hasServerError = true;
        this.errMessage = message;
    }
}