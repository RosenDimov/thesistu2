﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Player } from '../business-services/models/player';
import { Transfer } from '../business-services/models/transfer';
import { PlayersService } from '../business-services/players.service';

@Component({
    templateUrl: './player.component.html'
})
export class PlayerComponent implements OnInit {

    player: Player;
    transfers: Transfer[];

    constructor(private playersService: PlayersService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.getPlayer();
    }

    private getPlayer() {
        let id = parseInt(this.route.snapshot.params["id"]);

        this.playersService.getPlayerWithTransfers(id).subscribe((data) => {
            this.player = data.Player;
            this.transfers = data.Transfers;
        }, () => {
            this.router.navigate(["/notfound"]);
        });
    }
}