﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewPlayerComponent } from './new-player.component';
import { SearchPlayersComponent } from './search-players.component';
import { PlayerComponent } from './player.component';

const playersRoutes: Routes = [
    { path: 'new', data: { permission: 'change', isEditMode: false }, component: NewPlayerComponent },
    { path: 'edit/:id', data: { permission: 'change', isEditMode: true }, component: NewPlayerComponent },
    { path: 'search', component: SearchPlayersComponent },
    { path: ':id', component: PlayerComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(playersRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PlayersRoutingModule { }
