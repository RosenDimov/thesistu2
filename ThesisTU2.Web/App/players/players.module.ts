﻿import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { PlayersRoutingModule } from './players-routing.module';
import { NewPlayerComponent } from './new-player.component';
import { SearchPlayersComponent } from './search-players.component';
import { PlayerComponent } from './player.component';

@NgModule({
    imports: [
        SharedModule,
        PlayersRoutingModule
    ],
    declarations: [
        NewPlayerComponent,
        SearchPlayersComponent,
        PlayerComponent
    ],
    providers: [
    ]
})
export class PlayersModule { }