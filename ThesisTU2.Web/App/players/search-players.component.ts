﻿import { Component, OnInit } from '@angular/core';

import { Player } from '../business-services/models/player';
import { Position } from '../business-services/models/position';
import { PlayersService } from '../business-services/players.service';
import { NomsService } from '../business-services/noms.service';

@Component({
    templateUrl: './search-players.component.html',
})
export class SearchPlayersComponent implements OnInit {

    searchTerm: string;
    position = 0;
    filterByPosition: boolean;
    positions: Position[];
    results: Player[];

    constructor(private playersService: PlayersService,
        private nomsService: NomsService) {
    }

    ngOnInit() {
        this.nomsService.getPlayerPositions().subscribe((positions) => {
            this.positions = positions;
            this.position = this.positions[0].PositionID;
        });
    }

    search() {
        if (this.filterByPosition) {
            this.getPlayersByNameAndPosition();
        }
        else {
            this.getPlayersByName();
        }
    }

    private getPlayersByNameAndPosition() {
        this.playersService.getPlayersByNameAndPosition(this.searchTerm, this.position).subscribe((data) => {
            this.results = data;
        }, (response) => {
            alert(response.error.Message);
        });
    }

    private getPlayersByName() {
        this.playersService.getPlayersByName(this.searchTerm).subscribe((data) => {
            this.results = data;
        }, (response) => {
            alert(response.error.Message);
        });
    }
}