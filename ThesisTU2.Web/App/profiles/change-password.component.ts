﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Passwords } from '../business-services/models/passwords';
import { UsersService } from '../business-services/users.service';

@Component({
    templateUrl: './change-password.component.html',
})
export class ChangePasswordComponent {

    passwords: Passwords;
    hasServerError: boolean;
    errMessage: string;

    constructor(private usersService: UsersService,
        private router: Router) {
        this.passwords = new Passwords();
    }

    change() {
        this.usersService.changeUserPassword(this.passwords).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private setServerError(msg: string) {
        this.hasServerError = true;
        this.errMessage = msg;
    }
}