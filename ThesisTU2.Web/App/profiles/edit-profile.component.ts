﻿import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { NomsService } from '../business-services/noms.service';
import { UsersService } from '../business-services/users.service';
import { Country } from '../business-services/models/country';
import { UserInfo } from '../business-services/models/user-info';
import { FileUploadService } from '../business-services/file-upload.service';

@Component({
    templateUrl: './edit-profile.component.html',
})
export class EditProfileComponent implements OnInit {

    countries: Country[];
    user: UserInfo;
    hasServerError: boolean;
    errMessage: string;
    file: File = null;

    constructor(private usersService: UsersService,
        private nomsService: NomsService,
        private fileUploadService: FileUploadService,
        private route: ActivatedRoute,
        private router: Router) {
        this.user = new UserInfo();
    }

    ngOnInit() {
        this.getCountries();
    }

    save() {
        if (this.file !== null) {
            // upload file
            this.uploadFile().subscribe((data) => {
                let uploadedFileInfo = data.json();
                this.user.AvatarPath = uploadedFileInfo.FilenameOriginal;
                this.user.ThumbPath = uploadedFileInfo.FilenameThumb;
                this.updateUser();
            }, (response) => {
                this.setServerError(response.error.Message);
            });
        }
        else {
            this.updateUser();
        }
    }

    private updateUser() {
        this.usersService.putUser(this.user).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    setUploadFile(event: any) {
        this.file = event.target.files[0];
    }

    private uploadFile(): Observable<Response> {
        return this.fileUploadService.uploadFile(this.file, "api/files/users");
    }

    private getCountries() {
        this.nomsService.getCountries().subscribe((countries) => {
            this.countries = countries;
            this.user.CountryID = this.countries[0].CountryID;
            this.getUser();
        })
    }

    private getUser() {
        this.usersService.getCurrentUser().subscribe((user) => {
            this.user = user;
            this.user.AvatarPath = null;
        }, (response) => {
            this.setServerError(response.error.Message);
        });
    }

    private setServerError(msg: string) {
        this.hasServerError = true;
        this.errMessage = msg;
    }
}