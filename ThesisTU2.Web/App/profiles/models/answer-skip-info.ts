﻿export class AnswerSkipInfo {
    skip: number;
    total: number;
    showMoreButton: boolean;
    remainingCount: number;
}