﻿import { Component, OnInit } from '@angular/core';

import { NotificationsInfo } from '../business-services/models/notifications-info';
import { NotificationsService } from '../business-services/notifications.service';
import { NotificationsManagerService } from '../core/notifications-manager.service';

@Component({
    templateUrl: './notifications.component.html',
})
export class NotificationsComponent implements OnInit {

    notificationsObj: NotificationsInfo;
    hasArticles = false;
    hasMessages = false;
    clickedArticleLinks: number[] = [];
    clickedWallpostLinks: number[] = [];
    hasServerError: boolean;
    errMessage: string;

    constructor(private notificationsService: NotificationsService,
        private notificationsManagerService: NotificationsManagerService) {
    }

    ngOnInit() {
        this.getNotifications();
    }

    private getNotifications() {
        this.notificationsService.getUnreadNotifications().subscribe((data) => {
            this.notificationsObj = data;

            if (data.Articles.length > 0)
                this.hasArticles = true;

            if (data.Wallposts.length > 0)
                this.hasMessages = true;
        }, (response) => {
            this.hasServerError = true;
            this.errMessage = response.error.Message;
        })
    }

    private removeArticleNotification(articleId: number) {
        // indicates whether it has already been clicked on and removed (when clicked with the scroller and opened in new tab)
        let read = this.clickedArticleLinks.some(id => id === articleId);

        if (!read) {
            this.clickedArticleLinks.push(articleId);
            this.notificationsManagerService.decreaseNotificationsCount();

            this.notificationsService.deleteUnreadNotification(articleId).subscribe(null, (response) => {
                alert(response.error.Message);
            });
        }
    }

    private removeWallpostNotification(wallpostId: number) {
        // indicates whether it has already been clicked on and removed (when clicked with the scroller and opened in new tab)
        let read = this.clickedWallpostLinks.some(id => id === wallpostId);

        if (!read) {
            this.clickedWallpostLinks.push(wallpostId);
            this.notificationsManagerService.decreaseNotificationsCount();

            this.notificationsService.deleteUnreadWallpost(wallpostId).subscribe(null, (response) => {
                alert(response.error.Message);
            })
        }
    }

    private acceptRequest(requestSenderId: number) {
        let subscription = this.notificationsService.acceptFriendRequest(requestSenderId).subscribe(() => {
            for (let i = 0; i < this.notificationsObj.FriendRequests.length; i++) {
                if (this.notificationsObj.FriendRequests[i].RequestSenderID === requestSenderId) {
                    this.notificationsObj.FriendRequests.splice(i, 1);
                }
            }

            $('#friend-approved').modal('show');
            this.notificationsManagerService.decreaseNotificationsCount();
        });
    }
}