﻿import { Component, EventEmitter, OnChanges, Input, Output, ChangeDetectionStrategy, SimpleChanges, ChangeDetectorRef } from '@angular/core';

import { Wallpost } from '../business-services/models/wallpost';
import { WallpostsService } from '../business-services/wallposts.service';
import { AnswerSkipInfo } from './models/answer-skip-info';
import { AuthService } from '../core/auth.service';

@Component({
    templateUrl: './profile-wallpost.component.html',
    selector: 'profile-wallpost',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileWallpostComponent implements OnChanges {

    @Input() wallpost: Wallpost;
    @Output() textareaHidden = new EventEmitter<number>();
    skipInfoEntry: AnswerSkipInfo;
    text: string;
    show: boolean;
    defaultImage: string;

    constructor(private wallpostsService: WallpostsService,
        private authService: AuthService,
        private changeDetector: ChangeDetectorRef) {
        let userContext = this.authService.getUserContext();
        this.defaultImage = userContext.Config.DefaultThumbImagePath;
    }

    ngOnChanges(changes: SimpleChanges) {
        //console.log("Calling ngOnChanges");

        let change = changes["wallpost"];
        let oldValue = <Wallpost>change.previousValue;
        let newValue = <Wallpost>change.currentValue;
        let isVisibilityChange = oldValue && oldValue.show !== newValue.show;

        if (this.skipInfoEntry) {
            if (!isVisibilityChange) {
                this.skipInfoEntry.skip++;
                this.skipInfoEntry.total++;
            }
        }
        else {
            this.initAnswersSkipInfoStructure();
        }
    }

    getWallpostAnswers() {
        if (this.skipInfoEntry.remainingCount <= 0)
            return;

        this.wallpostsService.getWallpostAnswers(this.wallpost.WallpostID, this.skipInfoEntry.skip).subscribe((data) => {
            if (data.length <= 0)
                return;

            for (let item of data) {
                this.wallpost.WallpostsAnswers.splice(0, 0, item);
            }

            this.skipInfoEntry.skip += data.length;
            this.skipInfoEntry.showMoreButton = (this.skipInfoEntry.total > this.skipInfoEntry.skip);
            this.skipInfoEntry.remainingCount -= data.length;
            this.changeDetector.markForCheck();
        });
    }

    addWallpostAnswer() {

        let answerObj = {
            Text: this.text,
            WallpostID: this.wallpost.WallpostID
        };

        this.wallpostsService.addWallpostAnswer(answerObj).subscribe((result) => {
            //var targetWallpost = _.find($scope.wallposts, function (wp) { return wp.WallpostID == wallpostId; });
            //targetWallpost.WallpostsAnswers.push(result);
            this.text = "";
        }, (err) => {
            alert(err.Message);
        });
    }

    showTextarea() {
        this.textareaHidden.emit(this.wallpost.WallpostID);
    }

    private initAnswersSkipInfoStructure() {
        var initialAnswersCount = this.wallpost.WallpostsAnswers.length; // it could be less than answersInitialCount variable (2)
        var totalAnswersCount = this.wallpost.AnswersCount; // total

        this.skipInfoEntry = {
            skip: initialAnswersCount,
            total: totalAnswersCount,
            showMoreButton: (totalAnswersCount > initialAnswersCount),
            remainingCount: (totalAnswersCount - initialAnswersCount)
        };
    }

    ngOnDestroy() {
        let test = this;
        console.log("Calling ng on destroy");
    }
}