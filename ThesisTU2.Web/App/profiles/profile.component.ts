import { Component, OnInit, OnDestroy, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import '../rxjs-operators';
import { fromJS, Map } from "immutable";

import { AuthService } from '../core/auth.service';
import { SignalRService } from '../core/signalr.service';
import { UserContext } from '../core/models/user-context';
import { WallpostsService } from '../business-services/wallposts.service';
import { UsersService } from '../business-services/users.service';
import { UserInfo } from '../business-services/models/user-info';
import { Wallpost } from '../business-services/models/wallpost';
import { WallpostAnswer } from '../business-services/models/wallpost-answer';
import { Article } from '../business-services/models/article';
import { AnswerSkipInfo } from './models/answer-skip-info';

@Component({
    templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit, OnDestroy, AfterViewChecked {

    userContext: UserContext;
    userInfo: UserInfo;
    wallposts: Wallpost[];
    news: Article[];
    defaultImageFull: string;
    hasMoreWallposts: boolean; //  = true;
    hasViewPermissions: boolean; //  = false;
    hasPendingFriendRequest: boolean; //  = false;
    friendRequestSent: boolean; // = false;
    skipWallpstsCount: number; // = 0;
    takeWallpostsCount = 5;
    currentUserId: number;
    currentLoggedUserId: number;
    totalWallposts: number;
    newWallpost: Wallpost;
    answers: { [key: number]: string };
    friendshipError: string // = "";

    wallpostAvailableSubscription: Subscription;
    wallpostAnswerAvailableSubscription: Subscription;

    constructor(private authService: AuthService,
        private route: ActivatedRoute,
        private router: Router,
        private wallpostsService: WallpostsService,
        private usersService: UsersService,
        private signalRService: SignalRService,
        private cd: ChangeDetectorRef) {

        this.userContext = this.authService.getUserContext();
        this.defaultImageFull = this.userContext.Config.DefaultOriginalImagePath;
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.currentUserId = +params['id'];
            this.initComponentState();
            this.setViewPermissions();
        });

        this.subscribeForNewWallpost();
        this.subscribeForNewWallpostAnswer();
    }

    ngOnDestroy() {
        this.wallpostAvailableSubscription.unsubscribe();
        this.wallpostAnswerAvailableSubscription.unsubscribe();
    }

    private initComponentState() {
        this.hasMoreWallposts = true;
        this.hasViewPermissions = false;
        this.hasPendingFriendRequest = false;
        this.friendRequestSent = false;
        this.skipWallpstsCount = 0;
        this.newWallpost = new Wallpost();
        this.newWallpost.UserID = this.currentUserId;
        this.friendshipError = "";
    }

    private setViewPermissions() {
        this.usersService.getLoggedUserId().subscribe((id) => {
            this.currentLoggedUserId = id;
            console.log("1 - setViewPermissions callback");

            if (this.currentUserId === this.currentLoggedUserId) {
                this.hasViewPermissions = true;
                this.getProfileInfo();
            }
            else {
                this.getFriendshipInfo();
            }
        });
    }

    private getFriendshipInfo() {
        this.usersService.getFriendshipInfo(this.currentUserId, this.currentLoggedUserId).subscribe((friendshipInfo) => {
            console.log("2 - getFriendshipInfo callback");
            if (friendshipInfo.HasFriendshipEntry) {
                if (friendshipInfo.IsConfirmedFriendship)
                    this.hasViewPermissions = true;
                else {
                    this.hasPendingFriendRequest = true;
                }
            }

            this.getProfileInfo();
        });
    }

    private getProfileInfo() {
        this.wallpostsService.getProfileInfo(this.currentUserId).subscribe((data) => {
            console.log("3 - getProfileInfo callback");
            this.userInfo = data.UserInfo;
            this.news = data.LatestNews;
            this.getProfileWallposts();
        }, (error) => {
            this.router.navigate(['/notfound']);
        })
    }

    private getProfileWallposts() {
        if (!this.hasViewPermissions)
            return;

        this.wallpostsService.getProfileWallposts(this.currentUserId).subscribe((data) => {
            console.log("4 - getProfileWallposts callback");
            this.wallposts = data.Wallposts;
            this.answers = {};

            this.totalWallposts = data.WallpostsCount;

            this.toggleMoreWpButton(data.Wallposts.length);
        });
    }

    private getWallposts() {
        if (!this.hasMoreWallposts) {
            return;
        }

        this.wallpostsService.getWallposts(this.currentUserId, this.skipWallpstsCount).subscribe((data) => {
            this.toggleMoreWpButton(data.length);

            if (data.length > 0) {
                for (let i in data)
                    this.wallposts.push(data[i]);
            }

            let currentLength = this.wallposts.length;
            let dataLength = data.length;
        });
    }

    private addWallpost() {
        this.wallpostsService.addWallpost(this.newWallpost).subscribe(() => {
            this.newWallpost.Text = "";
        }, (error) => {
            alert(error.Message);
        });
    }

    sendFriendRequest() {
        let friendshipInfo = { User1ID: this.currentLoggedUserId, User2ID: this.currentUserId };

        this.usersService.sentFriendshipRequest(friendshipInfo).subscribe(() => {
            this.friendRequestSent = true;
        }, (response) => {
            this.friendshipError = response.error.Message;
            $("#friendship-exists").modal('show');
        });
    }

    private toggleMoreWpButton(dataLength: number) {
        // check if this is called if we add new wallpost
        this.skipWallpstsCount += dataLength;

        if (dataLength == 0 || this.skipWallpstsCount >= this.totalWallposts) {
            this.hasMoreWallposts = false;
        }
    }

    private updateWallpostCount() {
        this.skipWallpstsCount++;
        this.totalWallposts++;
    }

    private subscribeForNewWallpost() {
        this.wallpostAvailableSubscription =
            this.signalRService.wallpostAvailable.subscribe((wallpost) => {
                if (wallpost !== null && typeof wallpost !== 'undefined' && this.currentUserId == wallpost.UserID) { // without last check it can post to anyone's profile page
                    this.wallposts.splice(0, 0, wallpost);
                    this.updateWallpostCount();
                    this.cd.detectChanges();
                }
            });
    }

    private subscribeForNewWallpostAnswer() {
        this.wallpostAnswerAvailableSubscription =
            this.signalRService.wallpostAnswerAvailable.subscribe((wallpostAnswer) => {
                var wallpostID = wallpostAnswer.WallpostID;

                let targetWallpostIndex = this.wallposts.findIndex(wp => wp.WallpostID === wallpostID);
                let map = fromJS(this.wallposts[targetWallpostIndex]);
                let copy = <Wallpost>map.toJS();
                copy.WallpostsAnswers.push(wallpostAnswer);
                copy.show = this.currentLoggedUserId === wallpostAnswer.AuthorID;
                this.wallposts[targetWallpostIndex] = copy;
            });
    }

    onTextareaToggle(wallpostId: number) {

        let nextVisibleIndex = this.wallposts.findIndex(wp => wp.WallpostID === wallpostId);
        let currentVisibleIndex = this.wallposts.findIndex(wp => wp.show);

        if (currentVisibleIndex > 0) {
            let map = fromJS(this.wallposts[currentVisibleIndex]);
            let toHideCopy = <Wallpost>map.toJS();
            toHideCopy.show = false;
            this.wallposts[currentVisibleIndex] = toHideCopy;
        }

        let map = fromJS(this.wallposts[nextVisibleIndex]);
        let toShowCopy = <Wallpost>map.toJS();
        toShowCopy.show = true;
        this.wallposts[nextVisibleIndex] = toShowCopy;
    }

    ngAfterViewChecked() {
        console.log("Calling after view checked");
    }
}