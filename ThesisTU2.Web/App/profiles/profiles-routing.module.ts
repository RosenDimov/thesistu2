import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileComponent } from './profile.component';
import { NotificationsComponent } from './notifications.component';
import { WallpostComponent } from './wallpost.component';
import { EditProfileComponent } from './edit-profile.component';
import { ChangePasswordComponent } from './change-password.component';

const profilesRoutes: Routes = [
    { path: 'notifications', component: NotificationsComponent },
    { path: 'edit', component: EditProfileComponent },
    { path: 'changepass', component: ChangePasswordComponent },
    { path: ':id', component: ProfileComponent },
    { path: 'wallpost/:id', component: WallpostComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(profilesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class ProfilesRoutingModule {
    
}
