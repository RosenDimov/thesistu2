import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { ProfilesRoutingModule } from './profiles-routing.module';
import { ProfileComponent } from './profile.component';
import { NotificationsComponent } from './notifications.component';
import { WallpostComponent } from './wallpost.component';
import { EditProfileComponent } from './edit-profile.component';
import { ChangePasswordComponent } from './change-password.component';
import { ProfileWallpostComponent } from './profile-wallpost.component';

@NgModule({
    imports: [
        ProfilesRoutingModule,
        SharedModule
    ],
    declarations: [
        ProfileComponent,
        NotificationsComponent,
        WallpostComponent,
        EditProfileComponent,
        ChangePasswordComponent,
        ProfileWallpostComponent
    ],
    exports: [
    ]
})
export class ProfilesModule { }
