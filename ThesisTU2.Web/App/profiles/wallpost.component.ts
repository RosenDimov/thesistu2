﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { Wallpost } from '../business-services/models/wallpost';
import { SignalRService } from '../core/signalr.service';
import { WallpostsService } from '../business-services/wallposts.service';

@Component({
    templateUrl: './wallpost.component.html',
})
export class WallpostComponent implements OnInit, OnDestroy {

    wallpost: Wallpost;
    newWallpostAnswerSubscription: Subscription;
    hasServerError: boolean;
    errMessage: string;
    id: number;
    answer: string;

    constructor(private signalRService: SignalRService,
        private wallpostsService: WallpostsService,
        route: ActivatedRoute) {
        this.id = +route.snapshot.params['id'];
    }

    ngOnInit() {
        this.getWallpost();
        this.subscribeForWallpostAnswer();
    }

    ngOnDestroy() {
        this.newWallpostAnswerSubscription.unsubscribe();
    }

    private getWallpost() {
        this.wallpostsService.getWallpost(this.id).subscribe((data) => {
            this.wallpost = data;
        }, (error) => {
            alert("An error occurred!");
        })
    }

    addWallpostAnswer() {
        let answerObj = {
            Text: this.answer,
            WallpostID: this.wallpost.WallpostID
        };

        this.wallpostsService.addWallpostAnswer(answerObj).subscribe(() => {
            this.answer = "";
        }, (response) => {
            alert(response.error.Message);
        });
    }

    private subscribeForWallpostAnswer() {
        this.newWallpostAnswerSubscription =
            this.signalRService.wallpostAnswerAvailable.subscribe((wallpostAnswer) => {
                if (wallpostAnswer !== null && typeof wallpostAnswer !== 'undefined' && wallpostAnswer.WallpostID === this.wallpost.WallpostID) {
                    this.wallpost.WallpostsAnswers.push(wallpostAnswer);
                }
            })
    }
}