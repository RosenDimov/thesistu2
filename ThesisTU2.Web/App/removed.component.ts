import { Component } from '@angular/core';

@Component({
    template: `<div class="alert alert-success" role="alert">
                  <strong>Success!</strong> The item was successfully removed from the database.
               </div>`,
})
export class RemovedComponent  {
}
