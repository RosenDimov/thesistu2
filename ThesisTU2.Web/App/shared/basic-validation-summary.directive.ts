import { Directive, ElementRef, Input, OnInit, AfterViewChecked } from '@angular/core';
import { AbstractControl, NgModel } from '@angular/forms';

@Directive({
    selector: '[val-summary]',
    providers: [ NgModel ]
})
export class BasicValidationSummaryDirective implements OnInit, AfterViewChecked {
    
    @Input('val-summary') labelName: string;
    control: AbstractControl;
    elementName: string;
    errorMessageElement: HTMLDivElement;
    isValidated = false;
    oldValue: string;

    constructor(private el: ElementRef,
        model: NgModel) {
        this.control = model.control;
        this.oldValue = this.control.value;
        let nativeElement = this.el.nativeElement;
        this.elementName = nativeElement.attributes["name"].value;
    }

    ngOnInit() {
        this.insertErrorMessageElement();
        this.insertLabel();
    }

    ngAfterViewChecked() {
        // if the field is not changed, but blurred from the first time - not validated and touched
        // otherwise it was validated once, just check if the value is new
        if ((!this.isValidated && this.control.touched) || this.oldValue != this.control.value) {
            this.oldValue = this.control.value;
            this.setValidationMessage();
            this.isValidated = true;
        }
    }

    private setValidationMessage() {
        let errorMessage = '';
        const control = this.control;

        if (control && (control.touched || control.dirty) && !control.valid) {
            const messages = this.validationMessages;
            for (const key in control.errors) {
                // if delimiter is <br/> - this triggers infinite ngAfterViewChecked
                errorMessage = this.addErrorMessage(errorMessage, control, key);
            }
        }

        if (errorMessage) {
            this.errorMessageElement.innerHTML = errorMessage;
            this.errorMessageElement.style.display = 'block';
        }
        else {
            this.errorMessageElement.style.display = 'none';
        }
    }

    private insertErrorMessageElement() {
        let nativeElement = this.el.nativeElement;
        this.errorMessageElement = document.createElement("div");
        nativeElement.insertAdjacentElement('afterend', this.errorMessageElement);
    }

    private insertLabel() {
        // insert label
        let nativeElement = this.el.nativeElement;

        let requiredAttr = nativeElement.attributes["required"];
        let parent = <HTMLElement>nativeElement.parentElement;
        let labelElement = document.createElement("label");
        let finalLabelName = requiredAttr ? `${this.labelName} *` : this.labelName;
        labelElement.innerHTML = finalLabelName;
        labelElement.className = "col-sm-2 control-label";
        labelElement.htmlFor = this.elementName;
        parent.insertAdjacentElement('beforebegin', labelElement);
    }

    private addErrorMessage(currentResult: string, control: AbstractControl, key: string) {
        let validationInfo = control.errors[key];
        let errorMessage: string;

        switch (key) {
            case "required":
                errorMessage = this.addRequiredValidationMessage(currentResult);
                break;
            case "minlength":
                errorMessage = this.addMinLengthValidationMessage(currentResult, validationInfo.requiredLength);
                break;
            case "min":
                errorMessage = this.addMinValidationMessage(currentResult, validationInfo.minValue);
                break;
            case "max":
                errorMessage = this.addMaxValidationMessage(currentResult, validationInfo.maxValue);
                break;
            case "integer":
                errorMessage = this.addIntegerValidationMessage(currentResult);
                break;
            case "float":
                errorMessage = this.addFloatValidationMessage(currentResult);
                break;
        }

        return errorMessage;
    }

    private addRequiredValidationMessage(currentResult: string) {
        let validationMessageTemplate = this.validationMessages['required'];
        let requiredMessage = validationMessageTemplate.replace("{label}", this.labelName);

        return this.concatNewMessage(currentResult, requiredMessage);
    }

    private addMinLengthValidationMessage(currentResult: string, minLength: any) {
        let validationMessageTemplate = this.validationMessages['minlength'];
        let minLengthMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{minLengthValue}", minLength);

        return this.concatNewMessage(currentResult, minLengthMessage);
    }

    private addMinValidationMessage(currentResult: string, minValue: any) {
        let validationMessageTemplate = this.validationMessages['min'];
        let minMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{min}", minValue);

        return this.concatNewMessage(currentResult, minMessage);
    }

    private addMaxValidationMessage(currentResult: string, maxValue: any) {
        let validationMessageTemplate = this.validationMessages['max'];
        let maxMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{max}", maxValue);

        return this.concatNewMessage(currentResult, maxMessage);
    }

    private addIntegerValidationMessage(currentResult: string) {
        let validationMessageTemplate = this.validationMessages['integer'];
        let integerMessage = validationMessageTemplate.replace("{label}", this.labelName);

        return this.concatNewMessage(currentResult, integerMessage);
    }

    private addFloatValidationMessage(currentResult: string) {
        let validationMessageTemplate = this.validationMessages['float'];
        let floatMessage = validationMessageTemplate.replace("{label}", this.labelName);

        return this.concatNewMessage(currentResult, floatMessage);
    }

    private concatNewMessage(currentResult: string, newMessage: string) {
        return `${currentResult}${newMessage} `;
    }

    validationMessages = {
        'required': '{label} is required.',
        'minlength': '{label} must be at least {minLengthValue} characters long.',
        'min': '{label} count must be at least {min}.',
        'max': '{label} count must be at most {max}.',
        'integer': '{label} must be an integer.',
        'float': '{label} must be a floating-point number.'
        //'maxlength': '{label} cannot be more than {maxLengthValue} characters long.'
    };
}