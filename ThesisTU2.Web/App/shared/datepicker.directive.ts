import { Directive, ElementRef, OnInit, EventEmitter, Output } from '@angular/core';

@Directive({
    selector: '[datepicker]'
})
export class DatepickerDirective implements OnInit {
    @Output() ngModelChange = new EventEmitter<string>();
    jQueryEl: JQuery;

    constructor(el: ElementRef) {
        this.jQueryEl = $(el.nativeElement);
    }

    ngOnInit() {
        this.jQueryEl.datepicker({
            dateFormat: 'mm/dd/yy',
            nextText: '',
            prevText: '',
            yearRange: "-50:+0",
            changeYear: true,
            onSelect: (date: string) => {
                this.ngModelChange.emit(date);
            }
        })
    }

    ngOnDestroy() {
        this.jQueryEl.datepicker('destroy');
    };
}
