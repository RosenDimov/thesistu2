import { Component, forwardRef } from '@angular/core';
import { AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'file-upload',
    templateUrl: './file-upload.component.html',
    providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => FileUploadComponent), multi: true }]
})
export class FileUploadComponent implements ControlValueAccessor {

    private value: File;
    private ngChange: Function = (_: number): void => {  };
    private ngTouched: Function = (): void => {  };

    setUploadFile(event: any) {
        this.ngChange(event.target.files[0]);
    }

    public writeValue(value: File): void {
        this.value = value;
    }

    public registerOnChange(fn: () => any): void {
        this.ngChange = fn;
    }

    public registerOnTouched(fn: () => any): void {
        this.ngTouched = fn;
    }
}