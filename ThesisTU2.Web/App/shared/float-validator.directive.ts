import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

export function floatValidator(): ValidatorFn {
    const FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    const validationInfoObject = { 'float': { valid: false } };

    return (control: AbstractControl): { [key: string]: any } => {
        if (!control.value) {
            return null;
        }

        let isNumber = FLOAT_REGEXP.test(control.value);
        return isNumber ? null : validationInfoObject;
    };
}

@Directive({
    selector: '[float]',
    providers: [{ provide: NG_VALIDATORS, useExisting: FloatValidatorDirective, multi: true }]
})
export class FloatValidatorDirective implements Validator {
    private valFn: ValidatorFn;

    ngOnInit() {
        this.valFn = floatValidator();
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}