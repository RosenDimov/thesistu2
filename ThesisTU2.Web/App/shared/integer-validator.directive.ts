import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

export function integerValidator(): ValidatorFn {
    var INTEGER_REGEXP = /^\-?\d+$/;
    const validationInfoObject = { 'integer': { valid: false } };

    return (control: AbstractControl): { [key: string]: any } => {
        // if input is something like '15.' the received value will be null - behaviour of JS as a whole
        // this is valid only for inputs of type number
        if (!control.value) {
            return null;
        }

        let isNumber = INTEGER_REGEXP.test(control.value);
        return isNumber ? null : validationInfoObject;
    };
}

@Directive({
    selector: '[integer]',
    providers: [{ provide: NG_VALIDATORS, useExisting: IntegerValidatorDirective, multi: true }]
})
export class IntegerValidatorDirective implements Validator {
    private valFn: ValidatorFn;

    ngOnInit() {
        this.valFn = integerValidator();
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}