import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

export function maxValidator(max: number): ValidatorFn {
    const FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    const validationInfoObject = { 'max': { maxValue: max } };

    return (control: AbstractControl): { [key: string]: any } => {
        if (!control.value) {
            return null;
        }

        let isNumber = FLOAT_REGEXP.test(control.value);

        if (!isNumber) {
            return validationInfoObject;
        }
        else {
            let numericVal = parseFloat(control.value);
            return (numericVal > max) ? validationInfoObject : null;
        }
    };
}

@Directive({
    selector: '[max]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MaxValidatorDirective, multi: true }]
})
export class MaxValidatorDirective implements Validator {
    @Input() max: number;
    private valFn: ValidatorFn;

    ngOnInit() {
        this.valFn = maxValidator(this.max);
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['max'];
        if (change) {
            let maxValue = parseFloat(change.currentValue);
            this.valFn = maxValidator(maxValue);
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}