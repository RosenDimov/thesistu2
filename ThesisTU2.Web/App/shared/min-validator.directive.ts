import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

export function minValidator(min: number): ValidatorFn {
    const FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    const validationInfoObject = { 'min': { minValue: min } };

    return (control: AbstractControl): { [key: string]: any } => {
        if (!control.value) {
            return null;
        }

        let isNumber = FLOAT_REGEXP.test(control.value);

        if (!isNumber) {
            return validationInfoObject;
        }
        else {
            let numericVal = parseFloat(control.value);
            return (numericVal < min) ? validationInfoObject : null;
        }
    };
}

@Directive({
    selector: '[min]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true }]
})
export class MinValidatorDirective implements Validator {
    @Input() min: number;
    private valFn: ValidatorFn;

    ngOnInit() {
        this.valFn = minValidator(this.min);
    }

    ngOnChanges(changes: SimpleChanges): void {
        const change = changes['min'];
        if (change) {
            let minValue = parseFloat(change.currentValue);
            this.valFn = minValidator(minValue);
        }
    }

    validate(control: AbstractControl): { [key: string]: any } {
        return this.valFn(control);
    }
}