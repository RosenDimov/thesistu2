import { Directive, ElementRef, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Directive({
    selector: '[suggestions-multiple]'
})
export class MultipleSuggestionsDirective implements OnInit {
    @Input('suggestions-multiple') sourceUrl: string;
    @Output() ngModelChange = new EventEmitter<string>();
    jQueryEl: JQuery;

    constructor(el: ElementRef) {
        this.jQueryEl = $(el.nativeElement);
    }

    ngOnInit() {
        let that = this;

        let handler = function handleModelChanges() {
            let currentValue = this.val();
            that.ngModelChange.emit(currentValue);
        };

        this.jQueryEl.tokenInput(this.sourceUrl, {
            queryParam: "term",
            minChars: 3,
            noResultsText: "No results found",
            resultsLimit: 5,
            tokenLimit: 3,
            preventDuplicates: true,
            onAdd: handler,
            onDelete: handler
        });
    }
}
