import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'notAvailable'
})
export class NotAvailablePipe implements PipeTransform {

    transform(value: any): string {
        return value == null ? "N/A" : value;
    }
}