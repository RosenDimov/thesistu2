import { Directive, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { AuthService } from '../core/auth.service';

@Directive({
    selector: '[has-permission]'
})
export class PermissionsDirective implements OnInit, OnDestroy {

    @Input('has-permission') permission: string;
    private permissionsSubscription: Subscription;
    private notPermissionFlag: boolean;
    private value: string;
    private jQueryEl: JQuery;

    constructor(private authService: AuthService,
        element: ElementRef) {
        this.jQueryEl = $(element.nativeElement);
    }

    ngOnInit() {
        if (typeof this.permission !== "string") {
            throw "hasPermission value must be a string";
        }

        this.value = this.permission.trim();
        this.notPermissionFlag = this.value[0] === '!';
        this.value = this.notPermissionFlag ? this.value.slice(1).trim() : this.value;

        this.subscribeForPermissionChanges();
    }

    ngOnDestroy() {
        this.permissionsSubscription.unsubscribe();
    }

    private subscribeForPermissionChanges() {

        this.permissionsSubscription =
            this.authService.permissionsAvailable.subscribe(() => {
                this.toggleVisibilityBasedOnPermission();
            });

        this.toggleVisibilityBasedOnPermission();
    }

    private toggleVisibilityBasedOnPermission() {
        let hasPermission = this.authService.hasPermission(this.value);

        if (hasPermission && !this.notPermissionFlag || !hasPermission && this.notPermissionFlag)
            this.jQueryEl.show();
        else
            this.jQueryEl.hide();
    }
}