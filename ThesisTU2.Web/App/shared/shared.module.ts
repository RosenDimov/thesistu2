import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PermissionsDirective } from './permissions.directive';
import { DatepickerDirective } from './datepicker.directive';
import { SuggestionsDirective } from './suggestions.directive';
import { MultipleSuggestionsDirective } from './multiple-suggestions.directive';
import { BasicValidationSummaryDirective } from './basic-validation-summary.directive';
import { MinValidatorDirective } from './min-validator.directive';
import { MaxValidatorDirective } from './max-validator.directive';
import { IntegerValidatorDirective } from './integer-validator.directive';
import { FloatValidatorDirective } from './float-validator.directive';
import { FileUploadComponent } from './file-upload.component';
import { TransferAmountPipe } from './transfer-amount.pipe';
import { NotAvailablePipe } from './not-available.pipe';

@NgModule({
    imports: [
    ],
    declarations: [
        PermissionsDirective,
        DatepickerDirective,
        SuggestionsDirective,
        MultipleSuggestionsDirective,
        BasicValidationSummaryDirective,
        MinValidatorDirective,
        MaxValidatorDirective,
        IntegerValidatorDirective,
        FloatValidatorDirective,
        FileUploadComponent,
        TransferAmountPipe,
        NotAvailablePipe
    ],
    exports: [
        PermissionsDirective,
        DatepickerDirective,
        SuggestionsDirective,
        MultipleSuggestionsDirective,
        BasicValidationSummaryDirective,
        MinValidatorDirective,
        MaxValidatorDirective,
        IntegerValidatorDirective,
        FloatValidatorDirective,
        FileUploadComponent,
        TransferAmountPipe,
        NotAvailablePipe,
        CommonModule,
        FormsModule
    ]
})
export class SharedModule { } 
