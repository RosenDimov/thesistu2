import { Directive, ElementRef, OnInit, OnDestroy, EventEmitter, Input, Output } from '@angular/core';

@Directive({
    selector: '[suggestions]'
})
export class SuggestionsDirective implements OnInit, OnDestroy {
    @Input('suggestions') sourceUrl: string;
    @Output() ngModelChange = new EventEmitter<string>();
    jQueryEl: JQuery;

    constructor(el: ElementRef) {
        this.jQueryEl = $(el.nativeElement);
    }

    ngOnInit() {
        this.jQueryEl.autocomplete({
            minLength: 4,
            source: this.sourceUrl,
            select: (event, selectedItem) => {
                this.ngModelChange.emit(selectedItem.item.value);
            }
        });
    }

    ngOnDestroy() {
        this.jQueryEl.autocomplete('destroy');
    };
}
