import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'transferAmount'
})
export class TransferAmountPipe implements PipeTransform {

    transform(value: number): string {
        if (value === 0 || value == null) {
            return "None";
        }
        else {
            return `\u20AC${value} mln.` // format with currency euros
        }
    }
}