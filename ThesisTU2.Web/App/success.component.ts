import { Component } from '@angular/core';

@Component({
    template: `<div class="alert alert-success" role="alert">
                   <strong>Success!</strong> Item was successfully added to the database.
               </div>`,
})
export class SuccessComponent  {
}
