﻿import { Component, OnInit, DoCheck } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { TransferType } from '../business-services/models/transfer-type';
import { Transfer } from '../business-services/models/transfer';
import { TransfersService } from '../business-services/transfers.service';
import { NomsService } from '../business-services/noms.service';

@Component({
    templateUrl: './new-transfer.component.html',
})
export class NewTransferComponent implements OnInit, DoCheck {

    transferTypes: TransferType[];
    transfer: Transfer;
    isEdit: boolean;
    hasServerError: boolean;
    showAmount: boolean;
    freeTransferValue: number;
    errMessage: string;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private nomsService: NomsService,
        private transfersService: TransfersService,
        private datePipe: DatePipe) {

        this.transfer = new Transfer();
        this.isEdit = this.route.snapshot.data['isEditMode'];
    }

    ngOnInit() {
        this.getTransferTypes();
    }

    ngDoCheck() {
        if (!this.freeTransferValue || !this.transfer.TransferTypeID)
            return;

        this.toggleAmountField();
    }

    save() {
        if (this.isEdit) {
            this.updateTransfer();
        }
        else {
            this.createTransfer();
        }
    }

    private getTransferTypes() {
        this.nomsService.getTransferTypes().subscribe((transferTypes) => {
            this.transferTypes = transferTypes;
            this.transfer.TransferTypeID = this.transferTypes[0].TransferTypeID;

            for (let index in transferTypes) {
                if (transferTypes[index].Title.indexOf('Free') != -1) {
                    this.freeTransferValue = transferTypes[index].TransferTypeID;
                    break;
                }
            }

            if (this.isEdit) {
                this.getExistingTransfer();
            }
        })
    }

    private getExistingTransfer() {
        let id = parseInt(this.route.snapshot.params['id']);

        if (typeof id === 'number' && !isNaN(id)) {
            this.transfersService.getTransfer(id).subscribe((data) => {
                this.transfer = data;
                this.transfer.DateCompleted = this.datePipe.transform(this.transfer.DateCompleted, 'MM/dd/yyyy');
            }, (response) => {
                this.setServerError(response.error.Message);
            })
        }
        else {
            this.router.navigate(['/']);
        }
    }

    private createTransfer() {
        this.transfersService.addTransfer(this.transfer).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private updateTransfer() {
        this.transfersService.putTransfer(this.transfer).subscribe(() => {
            this.router.navigate(['/success']);
        }, (response) => {
            this.setServerError(response.error.Message);
        })
    }

    private toggleAmountField() {
        if (this.transfer.TransferTypeID == this.freeTransferValue) {
            this.showAmount = false;
            this.transfer.Amount = null;
        }
        else {
            this.showAmount = true;
        }
    }

    private setServerError(message: string) {
        this.hasServerError = true;
        this.errMessage = message;
    }
}