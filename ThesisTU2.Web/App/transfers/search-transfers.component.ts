﻿import { Component } from '@angular/core';

import { Transfer } from '../business-services/models/transfer';
import { TransfersService } from '../business-services/transfers.service';

@Component({
    templateUrl: './search-transfers.component.html',
})
export class SearchTransfersComponent {

    results: Transfer[];
    transferCriteria = "player";
    clubRole = "any";
    searchTerm: string;

    constructor(private transfersService: TransfersService) {
    }

    search() {
        if (this.transferCriteria === 'player') {
            this.getTransfersByPlayer();
        }
        else if (this.transferCriteria === 'club') {
            if (this.clubRole === 'buyer') {
                this.getTransfersByTeamBuyer();
            }
            else if (this.clubRole === 'seller') {
                this.getTransfersByTeamSeller();
            }
            else {
                this.getTransfersByTeam();
            }
        }
    }

    private getTransfersByPlayer() {
        this.transfersService.getTransfersByPlayer(this.searchTerm).subscribe((data) => {
            this.setResults(data);
        }, (response) => {
            alert(response.error.Message);
        })
    }

    private getTransfersByTeamBuyer() {
        this.transfersService.getTransfersByTeamBuyer(this.searchTerm).subscribe((data) => {
            this.setResults(data);
        }, (response) => {
            alert(response.error.Message);
        })
    }

    private getTransfersByTeamSeller() {
        this.transfersService.getTransfersByTeamSeller(this.searchTerm).subscribe((data) => {
            this.setResults(data);
        }, (response) => {
            alert(response.error.Message);
        })
    }

    private getTransfersByTeam() {
        this.transfersService.getTransfersByTeam(this.searchTerm).subscribe((data) => {
            this.setResults(data);
        }, (response) => {
            alert(response.error.Message);
        })
    }

    private setResults(data: Transfer[]) {
        this.results = data;
    }
}