﻿import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Transfer } from '../business-services/models/transfer';
import { TransfersService } from '../business-services/transfers.service';

@Component({
    templateUrl: './transfer.component.html',
})
export class TransferComponent implements OnInit {

    transfer: Transfer;
    id: number;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private transfersService: TransfersService) {

        this.id = parseInt(this.route.snapshot.params['id']);
    }

    ngOnInit() {
        this.getTransfer();
    }

    private getTransfer() {
        this.transfersService.getTransfer(this.id).subscribe((transfer) => {
            if (transfer == null)
                this.router.navigate(['/notfound']);

            this.transfer = transfer;
        }, (error) => {
            this.router.navigate(['/notfound']);
        });
    }

    remove() {
        this.transfersService.deleteTransfer(this.id).subscribe(() => {
            let element = $(".modal.in");
            element.remove();

            this.router.navigate(['/removed']);
        }, (response) => {
            alert(response.error.Message);
        })
    }
}