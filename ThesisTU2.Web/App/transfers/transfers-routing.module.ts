﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewTransferComponent } from './new-transfer.component';
import { SearchTransfersComponent } from './search-transfers.component';
import { TransferComponent } from './transfer.component';

const transfersRoutes: Routes = [
    { path: 'new', data: { permission: 'change', isEditMode: false }, component: NewTransferComponent },
    { path: 'edit/:id', data: { permission: 'change', isEditMode: true }, component: NewTransferComponent },
    { path: 'search', component: SearchTransfersComponent },
    { path: ':id', component: TransferComponent }
];
@NgModule({
    imports: [
        RouterModule.forChild(transfersRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class TransfersRoutingModule { }