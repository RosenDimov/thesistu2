﻿import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { TransfersRoutingModule } from './transfers-routing.module';
import { NewTransferComponent } from './new-transfer.component';
import { SearchTransfersComponent } from './search-transfers.component';
import { TransferComponent } from './transfer.component';

@NgModule({
    imports: [
        SharedModule,
        TransfersRoutingModule
    ],
    declarations: [
        NewTransferComponent,
        SearchTransfersComponent,
        TransferComponent
    ],
    providers: [
    ]
})
export class TransfersModule { }