import { Component } from '@angular/core';

@Component({
    template: `<div class="alert alert-danger">
                   <strong>You are not allowed to view this page!</strong>
               </div>`,
})
export class UnauthorizedComponent  {
}
