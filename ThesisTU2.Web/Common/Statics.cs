﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ThesisTU.Web.Common
{
    public class Statics
    {
        #region Public

        public static readonly int MaxImageLength = 102400; // 100k
        public static readonly string ResizedAvatarOptions = "width=64;height=64;format=jpg";

        public static string RootPath
        {
            get
            {
                return GetAppConfigValue<string>("RootPath");
            }
        }
        public static int IndexItems
        {
            get
            {
                return GetAppConfigValue<int>("IndexItems");
            }
        }
        public static string UsersThumbsPath
        {
            get
            {
                return GetAppConfigValue<string>("UsersThumbsPath");
            }
        }
        public static string UsersOriginalPath
        {
            get
            {
                return GetAppConfigValue<string>("UsersOriginalPath");
            }
        }
        public static string DefaultOriginalImagePath
        {
            get
            {
                return GetAppConfigValue<string>("DefaultOriginalImagePath");
            }
        }
        public static string DefaultThumbImagePath
        {
            get
            {
                return GetAppConfigValue<string>("DefaultThumbImagePath");
            }
        }
        public static string ClubsPath
        {
            get
            {
                return GetAppConfigValue<string>("ClubsPath");
            }
        }
        public static string PlayersPath
        {
            get
            {
                return GetAppConfigValue<string>("PlayersPath");
            }
        }


        #endregion

        #region Private

        private static ConcurrentDictionary<string, object> _valueCache = new ConcurrentDictionary<string, object>();
        private static object _syncRoot = new object();

        private static T GetAppConfigValue<T>(string appConfigKey)
        {
            if (!_valueCache.ContainsKey(appConfigKey))
            {
                lock (_syncRoot)
                {
                    if (!_valueCache.ContainsKey(appConfigKey))
                    {
                        string appConfigValue = System.Web.Configuration.WebConfigurationManager.AppSettings[appConfigKey];

                        T configValue = (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(appConfigValue);

                        _valueCache.TryAdd(appConfigKey, configValue);
                    }
                }
            }

            return (T)_valueCache[appConfigKey];
        }

        #endregion
    }
}