﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThesisTU.Models.Authentication;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Models.Entities;
using ThesisTU.Web.Models;
using ThesisTU.Common.Validation;
using System.IO;
using System.Transactions;
using System.Web.Configuration;
using ThesisTU.Web.Common;

namespace ThesisTU.Web.Controllers
{
    public class AccountController : Controller
    {
        private IUserContextProvider userContextProvider;

        private IUnitOfWork unitOfWork;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="unitOfWork">Базов интерфейс за достъп до базата данни</param>
        /// <param name="userContextProvider">Интерфейс за достъп до потребителските данни</param>
        public AccountController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
        {
            this.unitOfWork = unitOfWork;
            this.userContextProvider = userContextProvider;
        }

        /// <summary>
        /// Вход в системата
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            UserContext context = this.userContextProvider.GetCurrentUserContext();

            if (context != null)
                return Redirect("~/");

            return View();
        }

        /// <summary>
        /// Обработка на данните при опит за вход в системата
        /// </summary>
        /// <param name="username">Въведено потребителско име</param>
        /// <param name="password">Въведена парола</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            string username = loginModel.Username;
            string password = loginModel.Password;
            
            User user = this.unitOfWork.UsersRepository.GetByName(username);

            if (user != null && user.IsActive && user.VerifyPassword(password))
            {
                this.userContextProvider.SetCurrentUserContext(new UserContext(user));

                return Redirect("~/");
            }
            else
            {
                ModelState.AddModelError("", "Incorrect username or password.");
                return View(loginModel);
            }
        }

        /// <summary>
        /// Изпълнява изход от системата за текущия потребител
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Logout()
        {
            this.userContextProvider.ClearCurrentUserContext();

            return Redirect("~/");
        }

        [HttpGet]
        public ActionResult Register()
        {
            UserContext context = this.userContextProvider.GetCurrentUserContext();

            if (context != null)
                return Redirect("~/");

            ViewBag.CountryID = new SelectList(unitOfWork.CountriesRepository.GetAll().OrderBy(c => c.CountryName).ToList(), "CountryID", "CountryName");

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel regModel, HttpPostedFileBase avatarFile)
        {
            if (ModelState.IsValid)
            {
                string username = regModel.UserName.Trim();

                bool usernameValid = UserRegistration.ValidateUsername(username);

                if (!usernameValid)
                    ModelState.AddModelError("", "The username must begin with a letter and contain only letters, digits or _!");

                
                bool isValidRegistration = true;

                if (usernameValid)
                {
                    User userFound = unitOfWork.UsersRepository.GetByName(username);

                    if (userFound != null)
                    {
                        isValidRegistration = false;
                        ModelState.AddModelError("", "The username is already in use!");
                    }
                }

                Club club = unitOfWork.ClubsRepository.GetClubByName(regModel.FavouriteTeam);

                if (club == null)
                {
                    isValidRegistration = false;
                    ModelState.AddModelError("", "The club was not found in the system!");
                }

                string fullname = regModel.Fullname.Trim();

                if (!UserRegistration.ValidateFullname(fullname)) 
                {
                    isValidRegistration = false;
                    ModelState.AddModelError("", "The name must contain only letters, apostrophes or dashes!");
                }

                if (isValidRegistration)
                {
                    using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
                    {
                        User newUser = new User();
                        newUser.RoleId = 2; // user role
                        newUser.Username = username;
                        newUser.SetPassword(regModel.Password);
                        newUser.FavouriteTeamID = club.ClubID;
                        newUser.CountryID = regModel.CountryID;
                        newUser.Fullname = fullname;
                        newUser.IsActive = true;

                        if (UserRegistration.ValidateImage(avatarFile))
                        {
                            UploadAvatar(avatarFile, newUser);
                        }

                        unitOfWork.UsersRepository.Add(newUser);
                        unitOfWork.Save();
                        scope.Complete();
                    }
                    
                    return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.CountryID = new SelectList(unitOfWork.CountriesRepository.GetAll().OrderBy(c => c.CountryName).ToList(), "CountryID", "CountryName");
                    return View(regModel);
                }
            }

            ViewBag.CountryID = new SelectList(unitOfWork.CountriesRepository.GetAll().OrderBy(c => c.CountryName).ToList(), "CountryID", "CountryName");
            return View(regModel);
        }

        private void UploadAvatar(HttpPostedFileBase avatarFile, User newUser)
        {
            string extension = Path.GetExtension(avatarFile.FileName);
            string rootPath = Statics.RootPath;
            string virtualPathOrignal = Statics.UsersOriginalPath;
            newUser.AvatarPath = string.Format("{0}{1}{2}", virtualPathOrignal, Guid.NewGuid(), extension);
            string physicalPathOriginal = Path.Combine(Server.MapPath(rootPath + virtualPathOrignal), Path.GetFileName(newUser.AvatarPath));
            avatarFile.SaveAs(physicalPathOriginal);

            string thumbPath = Statics.UsersThumbsPath;
            string virtualPathThumb = rootPath + thumbPath;

            avatarFile.InputStream.Seek(0, SeekOrigin.Begin);
            ImageResizer.ImageJob ijob = new ImageResizer.ImageJob(avatarFile, virtualPathThumb + "<guid>.<ext>",
                new ImageResizer.Instructions("width=64;height=64;format=jpg"));
            ijob.Build();

            newUser.ThumbPath = thumbPath + Path.GetFileName(ijob.FinalPath);
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
