﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Transactions;
using System.Web;
using System.Web.Http;
using ThesisTU.Common.Validators;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Web.SignalR;

namespace ThesisTU.Web.Controllers
{
    public class ArticlesController : BaseController
    {
        public ArticlesController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int MinTitleSearchLength = 3;
        private const int MinContentSearchLength = 10;
        private const int CommentsTakeCount = 5;

        [Route("api/articles/latest/{count:int:min(1)}")]
        public IEnumerable<ArticleDO> GetLatestArticles(int count)
        {
            var articles = unitOfWork.ArticlesRepository.GetLatest(count).Select(a => new ArticleDO(a));

            return articles;
        }

        [Route("api/articles/byid/{id:int}")]
        public IHttpActionResult GetById(int id)
        {
            Article article = unitOfWork.ArticlesRepository.GetById(id);

            if (article == null)
                return BadRequest("Article not found.");

            int totalCommentsCount = article.ArticleComments.Count;

            return Ok(new
            {
                Article = new ArticleDO(article),
                TotalCount = totalCommentsCount
            });
        }

        [HttpPost]
        [Route("api/articles/")]
        public IHttpActionResult AddArticle([FromBody]ArticleDO article)
        {
            if (userContext == null)
                return Unauthorized();

            if (!ArticleValidator.Validate(article))
                return BadRequest("Invalid article object.");

            try
            {
                var conntectedClubs = unitOfWork.ClubsRepository.GetClubsByIds(article.ConnectedTeamsIds);

                if (conntectedClubs.Count() == 0)
                    throw new InvalidOperationException("None of the specified clubs was found in the database.");

                int newArticleId = 0;

                using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
                {
                    Article art = new Article();
                    art.Title = article.Title;
                    art.Text = article.Text;
                    art.DateAdded = DateTime.Now;

                    foreach (Club club in conntectedClubs)
                        art.Clubs.Add(club);

                    unitOfWork.ArticlesRepository.Add(art);
                    unitOfWork.Save();
                    newArticleId = art.ArticleID;

                    scope.Complete();
                }

                var usersWithCurrentFavTeam = unitOfWork.UsersRepository.GetUsersIdsByFavouriteTeams(conntectedClubs.Select(c => c.ClubID)).ToList();
                unitOfWork.UsersRepository.UpdateUnreadArticles(usersWithCurrentFavTeam, newArticleId);

                Broadcaster.Instance.BroadcastToClients(usersWithCurrentFavTeam);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [HttpPost]
        [Route("api/articles/comment/")]
        public IHttpActionResult AddComment([FromBody]ArticleCommentDO comment)
        {
            if (userContext == null)
                return Unauthorized();

            if (!ArticleCommentValidator.Validate(comment))
                return BadRequest("Invalid comment.");

            try
            {
                ArticleComment comm = new ArticleComment();
                comm.ArticleID = comment.ArticleID;
                comm.AuthorID = userContext.UserId;
                comm.DatePosted = DateTime.Now;
                comm.Text = comment.Text;

                unitOfWork.ArticlesRepository.AddComment(comm);
                unitOfWork.Save();

                comment = new ArticleCommentDO(comm);
                // navigation prop User cannot be accessed right now in EF so take user entity
                var user = unitOfWork.UsersRepository.GetById(userContext.UserId);
                comment.Author = user.Username;

                Broadcaster.Instance.BroadcastArticleComment(comment);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [HttpPost]
        [Route("api/articles/search")]
        public IHttpActionResult SearchArticles([FromBody]SearchArticlesDO search)
        {
            if (SearchArticlesValidator.IsEmptySearch(search))
                return BadRequest("You must specify at least one search criteria!");

            if (!string.IsNullOrWhiteSpace(search.Title) && search.Title.Length < MinTitleSearchLength)
                return BadRequest(string.Format("Title search must be at least {0} characters long.", MinTitleSearchLength));

            if (!string.IsNullOrWhiteSpace(search.Content) && search.Content.Length < MinContentSearchLength)
                return BadRequest(string.Format("Content search must be at least {0} characters long.", MinContentSearchLength));

            if (search.StartDate.HasValue && search.EndDate.HasValue && search.StartDate >= search.EndDate)
                return BadRequest("Start date must precede end date in time.");

            var results = unitOfWork.ArticlesRepository.SearchArticles(search.Title, search.Content, search.StartDate, search.EndDate).Select(a => new ArticleDO(a));

            return Ok(results);
        }

        [Route("api/article/comments/{articleId:int}/{skip:int}")]
        public IHttpActionResult GetComments(int articleId, int skip)
        {
            if (userContext == null)
                return Unauthorized();

            var result = unitOfWork.ArticlesRepository.GetComments(articleId, CommentsTakeCount, skip: skip).Select(ac => new ArticleCommentDO(ac));

            return Ok(result);
        }
    }
}
