﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Web.Controllers
{
    /// <summary>
    /// Базов WebApi контролер
    /// </summary>
    public class BaseController : ApiController
    {
        protected IUnitOfWork unitOfWork;
        protected IUserContextProvider userContextProvider;
        protected UserContext userContext;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="unitOfWork">Базов интерфейс за достъп до базата данни</param>
        /// <param name="userContextProvider">Интерфейс за достъп до потребителските данни</param>
        public BaseController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
        {
            this.unitOfWork = unitOfWork;
            this.userContextProvider = userContextProvider;
            this.userContext = userContextProvider.GetCurrentUserContext();
        }

        private User _systemUser;

        /// <summary>
        /// Достъп до системен потребител
        /// </summary>
        protected User SystemUser
        {
            get
            {
                if (_systemUser == null)
                {
                    _systemUser = this.unitOfWork.UsersRepository.GetByName("systemUser");
                }

                return _systemUser;
            }
        }

        /// <summary>
        /// Метод, който проверява дали текущият потребител е администратор
        /// </summary>
        /// <param name="u">Идентификатор на потребител</param>
        /// <returns></returns>
        protected bool HasAdminRights()
        {
            return this.userContext != null && this.userContext.Permissions.Contains("sys#admin");
        }

        [Route("api/permissions")]
        public string[] GetUserPermissions()
        {
            if (this.userContext == null)
                return null;

            return this.userContext.Permissions;
        }

        [Route("api/loggedid")]
        public IHttpActionResult GetLoggedUserId()
        {
            if (this.userContext == null)
                return Unauthorized();

            return Ok(this.userContext.UserId);
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
