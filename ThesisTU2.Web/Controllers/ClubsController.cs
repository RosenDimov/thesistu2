﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using ThesisTU.Common.Validators;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Web.Common;

namespace ThesisTU.Web.Controllers
{
    public class ClubsController : BaseController
    {
        public ClubsController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int LatestNewsCount = 3;
        private const int MinSearchLength = 3;
        private string lengthErrMessage = string.Format("The search term must be at least {0} characters long.", MinSearchLength);

        [HttpPost]
        public IHttpActionResult Add([FromBody]ClubDO clubDO)
        {
            if (!HasAdminRights())
                return BadRequest("You are not allowed to add clubs to the database.");

            if (!ClubValidator.Validate(clubDO))
                return BadRequest("Invalid club object.");

            string error = unitOfWork.ClubsRepository.IsUniqueClub(string.Empty, string.Empty, clubDO.ClubName, clubDO.Manager);

            if (!string.IsNullOrWhiteSpace(error))
                return BadRequest(error);

            Club club = clubDO.ToClub();
            club.LogoPath = Statics.ClubsPath + clubDO.LogoPath;
            unitOfWork.ClubsRepository.Add(club);
            unitOfWork.Save();

            return Created(string.Empty, clubDO);
        }

        [HttpPut]
        public IHttpActionResult Update(ClubDO clubDO)
        {
            if (!ClubValidator.Validate(clubDO))
                return BadRequest("Invalid club object.");

            var targetClub = unitOfWork.ClubsRepository.GetById(clubDO.ClubID);

            if (targetClub == null)
                return BadRequest("Club was not found.");

            string error = unitOfWork.ClubsRepository.IsUniqueClub(targetClub.ClubName, targetClub.Manager, clubDO.ClubName, clubDO.Manager);

            if (!string.IsNullOrWhiteSpace(error))
                return BadRequest(error);

            targetClub.ClubName = clubDO.ClubName;
            targetClub.CountryID = clubDO.CountryID;
            targetClub.Ground = clubDO.Ground;
            targetClub.Manager = clubDO.Manager;
            targetClub.TrophiesCount = clubDO.TrophiesCount;
            targetClub.YearFounded = clubDO.YearFounded;

            if (clubDO.LogoPath != null)
            {
                if (targetClub.LogoPath != null)
                {
                    string currentLogoPhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath(Statics.RootPath + targetClub.LogoPath);
                    File.Delete(currentLogoPhysicalPath);
                }
                
                targetClub.LogoPath = Statics.ClubsPath + clubDO.LogoPath;
            }

            unitOfWork.ClubsRepository.Update(targetClub);
            unitOfWork.Save();

            return Ok();
        }

        [Route("api/clubs/byterm/{search}")]
        public IHttpActionResult GetClubsBySearchTerm(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var clubs = unitOfWork.ClubsRepository.GetClubsBySearchTerm(search).Select(c => new ClubDO(c));

            return Ok(clubs);
        }

        [Route("api/clubs/bytermbegin/{search}")]
        public IHttpActionResult GetClubsBySearchTermMatchBeginning(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var results = unitOfWork.ClubsRepository.GetClubsBySearchTermMatchBeginning(search).Select(c => new ClubDO(c));
            return Ok(results);
        }

        [Route("api/clubs/byname/{search}")]
        public ClubDO GetClubByName(string search)
        {
            Club clubFound = unitOfWork.ClubsRepository.GetClubByName(search);

            return clubFound != null ? new ClubDO(clubFound) : null;
        }

        [Route("api/clubs/byid/{search:int}")]
        public ClubDO GetById(int search)
        {
            Club club = unitOfWork.ClubsRepository.GetById(search);
            return club != null ? new ClubDO(club) : null;
        }

        [Route("api/clubs/byidwithnews/{search:int}")]
        public IHttpActionResult GetByIdWithLatestNews(int search)
        {
            var club = unitOfWork.ClubsRepository.GetById(search);

            if (club == null)
                return BadRequest("Club was not found in the database.");

            var result = new
            {
                Club = new ClubDO(club),
                Articles = club.Articles.OrderByDescending(a => a.DateAdded).Take(LatestNewsCount).Select(a => new ArticleDO(a))
            };

            return Ok(result);
        }

        [HttpGet]
        public string IsUnique(string oldName, string oldManager, string newClubName, string newManagerName)
        {
            string output = unitOfWork.ClubsRepository.IsUniqueClub(oldName, oldManager, newClubName, newManagerName);
            return output;
        }

        private bool IsValidSearch(string search)
        {
            return !string.IsNullOrWhiteSpace(search) && search.Length >= MinSearchLength;
        }
    }
}

