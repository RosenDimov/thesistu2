﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using ThesisTU.Common.Utils;
using ThesisTU.Web.Common;

namespace ThesisTU.Web.Controllers
{
    public class FilesController : ApiController
    {
        [HttpPost]
        [Route("api/files/clubs/")]
        public async Task<IHttpActionResult> UploadClubPicture()
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest("Unsupported media type.");

            string outputFilename;

            try
            {
                outputFilename = await UploadFile(Statics.ClubsPath, ImageUploadUtils.HasClubLogoValidSize, ImageUploadUtils.ClubLogoMaxWidth,
                    ImageUploadUtils.ClubLogoMaxHeight);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(new { Filename = outputFilename });
        }

        [HttpPost]
        [Route("api/files/players/")]
        public async Task<IHttpActionResult> UploadPlayerPicture()
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest("Unsupported media type.");

            string outputFilename;

            try
            {
                outputFilename = await UploadFile(Statics.PlayersPath, ImageUploadUtils.HasPlayerImageValidSize, ImageUploadUtils.PlayerImageMaxWidth,
                    ImageUploadUtils.PlayerImageMaxHeight);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(new { Filename = outputFilename });
        }

        [HttpPost]
        [Route("api/files/users/")]
        public async Task<IHttpActionResult> UploadUserAvatar()
        {
            if (!Request.Content.IsMimeMultipartContent())
                return BadRequest("Unsupported media type.");

            string outputFilenameOriginal, outputFilenameThumb;

            try
            {
                outputFilenameThumb = UploadUserThumbImage(HttpContext.Current.Request.Files[0]);
                outputFilenameOriginal = await UploadFile(Statics.UsersOriginalPath, ImageUploadUtils.HasUserAvatarValidSize, ImageUploadUtils.UserAvatarWidth,
                    ImageUploadUtils.UserAvatarHeight);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(new
            {
                FilenameOriginal = outputFilenameOriginal,
                FilenameThumb = outputFilenameThumb
            });
        }

        private async Task<string> UploadFile(string uploadDir, Func<Stream, bool> sizeValidationFunc, int maxImageWidth, int maxImageHeight)
        {
            HttpPostedFile file = HttpContext.Current.Request.Files[0];

            if (file.ContentLength > Statics.MaxImageLength)
                throw new InvalidOperationException(string.Format("The size of the image cannot be more than {0} kb in size.", Statics.MaxImageLength / 1024));

            if (!ImageUploadUtils.HasValidExtension(file.FileName))
                throw new InvalidOperationException("File must be .jpg, .png or .gif!");

            using (Stream fs = file.InputStream)
            {
                if (!sizeValidationFunc(fs))
                    throw new InvalidOperationException(string.Format("File must not more than {0}x{1} pixels in size", maxImageWidth, maxImageHeight));
            }

            var provider = GetMultipartProvider(uploadDir);
            var result = await Request.Content.ReadAsMultipartAsync(provider); // this method actually uploads the file to the server

            var uploadedFileInfo = new FileInfo(result.FileData[0].LocalFileName);

            string newFullFilename = FileUtils.GetNewFullFilename(Path.GetExtension(file.FileName), uploadedFileInfo.DirectoryName);

            File.Move(uploadedFileInfo.FullName, newFullFilename);

            return Path.GetFileName(newFullFilename);
        }

        private string UploadUserThumbImage(HttpPostedFile file)
        {
            if (file.ContentLength > Statics.MaxImageLength)
                throw new InvalidOperationException(string.Format("The size of the image cannot be more than {0} kb in size.", Statics.MaxImageLength / 1024));

            if (!ImageUploadUtils.HasValidExtension(file.FileName))
                throw new InvalidOperationException("File must be .jpg, .png or .gif!");

            if (!ImageUploadUtils.HasUserAvatarValidSize(file.InputStream))
                throw new InvalidOperationException(string.Format("File must not more than {0}x{1} pixels in size", ImageUploadUtils.UserAvatarWidth, 
                    ImageUploadUtils.UserAvatarHeight));

            file.InputStream.Seek(0, SeekOrigin.Begin);

            string uploadDir = HttpContext.Current.Server.MapPath(Statics.RootPath + Statics.UsersThumbsPath);

            ImageResizer.ImageJob ijob = new ImageResizer.ImageJob(file, uploadDir + "<guid>.<ext>",
                new ImageResizer.Instructions(Statics.ResizedAvatarOptions));
            ijob.Build();

            return Path.GetFileName(ijob.FinalPath);
        }

        private MultipartFormDataStreamProvider GetMultipartProvider(string dir)
        {
            var root = HttpContext.Current.Server.MapPath(Statics.RootPath + dir);
            return new MultipartFormDataStreamProvider(root);
        }

        //private string GetDeserializedFileName(MultipartFileData fileData)
        //{
        //    var fileName = GetFileName(fileData);
        //    return JsonConvert.DeserializeObject(fileName).ToString();
        //}

        //public string GetFileName(MultipartFileData fileData)
        //{
        //    return fileData.Headers.ContentDisposition.FileName;
        //}
    }
}
