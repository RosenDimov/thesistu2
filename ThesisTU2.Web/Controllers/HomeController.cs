﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThesisTU.Models.Authentication;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Models.Entities;
using System.Web.Configuration;
using ThesisTU.Web.Common;

namespace ThesisTU.Web.Controllers
{
    public class HomeController : Controller
    {
        private IUnitOfWork unitOfWork;
        private IUserContextProvider userContextProvider;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="unitOfWork">Базов интерфейс за достъп до базата данни</param>
        /// <param name="userContextProvider">Интерфейс за достъп до потребителските данни</param>
        public HomeController(
            IUnitOfWork unitOfWork,
            IUserContextProvider userContextProvider
            )
        {
            this.unitOfWork = unitOfWork;
            this.userContextProvider = userContextProvider;
        }

        /// <summary>
        /// Инициализация
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Index()
        {
            UserContext userContext = this.userContextProvider.GetCurrentUserContext();

            if (userContext == null)
                return RedirectToAction("Login", "Account");

            User user = this.unitOfWork.UsersRepository.GetById(userContext.UserId);

            object config = new
            {
                Permissions = userContext.Permissions,
                IndexItems = Statics.IndexItems,
                MaxCommentLength = 300,
                DefaultThumbImagePath = Statics.DefaultThumbImagePath,
                DefaultOriginalImagePath = Statics.DefaultOriginalImagePath
            };

            IDictionary<string, object> model = new ExpandoObject();

            model.Add("Config", config);
            model.Add("IsUserAdmin", userContext.IsUserAdmin);
            model.Add("Username", user.Username);
            model.Add("UserId", userContext.UserId);

            return View(model);
        }
    }
}
