﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Web.Controllers
{
    public class JsonAutocompleteController : Controller
    {
        private IUnitOfWork unitOfWork;
        private const int MaxTokensCount = 5;

        public JsonAutocompleteController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public JsonResult GetTeamsNames(string term)
        {
            var results = unitOfWork.ClubsRepository.GetClubsBySearchTermMatchBeginning(term).Select(c => c.ClubName);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTeamsNamesTokens(string term)
        {
            var results = unitOfWork.ClubsRepository.GetClubsBySearchTermMatchBeginning(term).Take(MaxTokensCount).Select(c => new { id = c.ClubID, name = c.ClubName });

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPlayersNames(string term)
        {
            var results = unitOfWork.PlayersRepository.GetPlayersByNameMatchBeginning(term).Select(p => p.FirstName + " " + p.LastName);

            return Json(results, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            unitOfWork.Dispose();

            base.Dispose(disposing);
        }
    }
}
