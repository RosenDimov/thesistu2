﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Web.Controllers
{
    public class NomsController : BaseController
    {
        public NomsController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        [Route("api/noms/countries")]
        public IEnumerable<CountryDO> GetCountries()
        {
            return unitOfWork.CountriesRepository.GetAll().ToList().Select(c => new CountryDO(c));
        }

        [Route("api/noms/positions")]
        public IEnumerable<PositionDO> GetPlayerPositions()
        {
            return unitOfWork.PlayersRepository.GetPlayerPositions().Select(p => new PositionDO(p));
        }

        [Route("api/noms/transfertypes")]
        public IEnumerable<TransferTypeDO> GetTransferTypes()
        {
            return unitOfWork.TransfersRepository.GetTransferTypes().Select(tt => new TransferTypeDO(tt));
        }
    }
}
