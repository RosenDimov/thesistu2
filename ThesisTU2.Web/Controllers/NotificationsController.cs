﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Transactions;
using System.Web.Http;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Repositories.Abstract;

namespace ThesisTU.Web.Controllers
{
    public class NotificationsController : BaseController
    {
        public NotificationsController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int WallpostSubstringLength = 50;

        [Route("api/notifications/countforuser/")]
        public int GetArticleCountForLoggedUser()
        {
            if (this.userContext == null)
                throw new SecurityException("You must be logged to view notifications count.");

            var user = unitOfWork.UsersRepository.GetById(userContext.UserId);
            int unreadArticlesCount = user.Articles.Count;
            int unreadWallpostsCount = user.Wallposts2.Count;
            int pendingFriendRequests = user.UsersFriendships1.Where(uf => !uf.IsConfirmed).Count();

            return (unreadArticlesCount + unreadWallpostsCount + pendingFriendRequests);
        }

        [HttpGet]
        [Route("api/notifications/removeforuser/")]
        public IHttpActionResult RemoveUnreadEntries()
        {
            if (this.userContext == null)
                throw new SecurityException("You must be logged to view new notifications.");

            try
            {
                using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
                {
                    unitOfWork.UsersRepository.RemoveUnreadArticleEntries(this.userContext.UserId);
                    unitOfWork.UsersRepository.RemoveUnreadWallpostsEntries(this.userContext.UserId);
                    scope.Complete();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }

        [Route("api/notifications/unread/")]
        public object GetUnreadNotificationsForUser()
        {
            if (this.userContext == null)
                throw new SecurityException("You must be logged to view notifications count.");

            var user = unitOfWork.UsersRepository.GetById(userContext.UserId);

            var unreadArticles = user.Articles.Select(a => new { Title = a.Title, ArticleID = a.ArticleID, DateAdded = a.DateAdded });

            var unreadWallposts = user.Wallposts2.Select(wp =>
                                                            new
                                                            {
                                                                WallpostID = wp.WallpostID,
                                                                Username = wp.User1.Username,
                                                                Fullname = wp.User1.Fullname,
                                                                ShortText = (wp.Text.Length > WallpostSubstringLength) ? wp.Text.Substring(0, WallpostSubstringLength) + "..." : wp.Text
                                                            });

            var pendingFriendshipRequests = user.UsersFriendships1.Where(uf => !uf.IsConfirmed).Select(uf => new FriendshipDO(uf));

            return new
            {
                Articles = unreadArticles,
                Wallposts = unreadWallposts,
                FriendRequests = pendingFriendshipRequests
            };
        }

        [Route("api/notifications/deleteunreadarticle/{id:int}")]
        [HttpDelete]
        public IHttpActionResult RemoveUnreadArticle(int id)
        {
            try
            {
                unitOfWork.UsersRepository.RemoveUnreadArticle(this.userContext.UserId, id);
                return Ok();
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("api/notifications/deleteunreadwallpost/{id:int}")]
        [HttpDelete]
        public IHttpActionResult RemoveUnreadWallpost(int id)
        {
            try
            {
                unitOfWork.UsersRepository.RemoveUnreadWallpost(this.userContext.UserId, id);
                return Ok();
            }

            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        [Route("api/notifications/acceptfriend/{friendId:int}")]
        public IHttpActionResult AcceptFriendRequest(int friendId)
        {
            if (userContext == null)
                return Unauthorized();

            try
            {
                unitOfWork.UsersRepository.AcceptFriendship(friendId, userContext.UserId);
                unitOfWork.Save();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
    }
}
