﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Transactions;
using System.Web.Http;
using ThesisTU.Common.Validators;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Web.Common;

namespace ThesisTU.Web.Controllers
{
    public class PlayersController : BaseController
    {
        public PlayersController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int LatestTransfersCount = 3;
        private const int MinSearchLength = 3;
        private string lengthErrMessage = string.Format("The search term must be at least {0} characters long.", MinSearchLength);

        [Route("api/players/byid/{search:int}")]
        public PlayerDO GetById(int search)
        {
            var player = unitOfWork.PlayersRepository.GetById(search);
            return player != null ? new PlayerDO(player) : null;
        }

        [Route("api/players/byidwithtransfers/{search:int}")]
        public IHttpActionResult GetByIdWithLatestTransfers(int search)
        {
            var player = unitOfWork.PlayersRepository.GetById(search);

            if (player == null)
                return BadRequest("Player was not found in the database.");

            var result = new
            {
                Player = new PlayerDO(player),
                Transfers = player.Transfers.OrderByDescending(t => t.DateCompleted).Take(LatestTransfersCount).Select(t => new TransferDO(t))
            };

            return Ok(result);
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]PlayerDO playerDO)
        {
            if (!PlayerValidator.Validate(playerDO))
                return BadRequest("Invalid player object.");

            Club club = unitOfWork.ClubsRepository.GetClubByName(playerDO.ClubName);

            if (club != null)
            {
                using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
                {
                    playerDO.ClubID = club.ClubID;
                    Player player = playerDO.ToPlayer();
                    player.PicturePath = Statics.PlayersPath + playerDO.PicturePath;
                    unitOfWork.PlayersRepository.Add(player);
                    unitOfWork.Save();

                    scope.Complete();
                }
            }
            else
                return BadRequest("Player's club name does not exist in the database!");

            return Created(string.Empty, playerDO);
        }

        [HttpPut]
        public IHttpActionResult Update(PlayerDO playerDO)
        {
            if (!PlayerValidator.Validate(playerDO))
                return BadRequest("Invalid player object.");

            Player player = unitOfWork.PlayersRepository.GetById(playerDO.PlayerID);

            if (player == null)
                return BadRequest("The player does not exist.");

            player.Age = playerDO.Age;
            player.BirthDate = playerDO.BirthDate;
            player.ClubID = playerDO.ClubID;
            player.CountryID = playerDO.CountryID;
            player.FirstName = playerDO.FirstName;
            player.Goals = playerDO.Goals;
            player.LastName = playerDO.LastName;
            player.Number = playerDO.Number;
            player.PlayerID = playerDO.PlayerID;
            player.PositionID = playerDO.PositionID;

            if (playerDO.PicturePath != null)
            {
                if (player.PicturePath != null)
                {
                    string currentPicturePhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath(Statics.RootPath + player.PicturePath);
                    File.Delete(currentPicturePhysicalPath);
                }

                player.PicturePath = Statics.PlayersPath + playerDO.PicturePath;
            }

            unitOfWork.PlayersRepository.Update(player);
            unitOfWork.Save();

            return Ok();
        }

        [Route("api/players/bynamespos/{search}/{positionID:int}")]
        public IHttpActionResult GetPlayersByNamesAndPosition(string search, int positionID)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var result = unitOfWork.PlayersRepository.GetPlayersByNamesAndPosition(search, positionID).Select(p => new PlayerDO(p));
            return Ok(result);
        }

        [Route("api/players/byname/{search}")]
        public IHttpActionResult GetPlayersByName(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var result = unitOfWork.PlayersRepository.GetPlayersByName(search).Select(p => new PlayerDO(p));
            return Ok(result);
        }

        [Route("api/players/bynamebegin/{search}")]
        public IHttpActionResult GetPlayersByNameMatchBeginning(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var result = unitOfWork.PlayersRepository.GetPlayersByNameMatchBeginning(search).Select(p => new PlayerDO(p));
            return Ok(result);
        }

        [Route("api/players/bynameone/{search}")]
        public PlayerDO GetExactOneByName(string search)
        {
            var player = unitOfWork.PlayersRepository.GetExactOneByName(search);
            return player != null ? new PlayerDO(player) : null;
        }

        private bool IsValidSearch(string search)
        {
            return !string.IsNullOrWhiteSpace(search) && search.Length >= MinSearchLength;
        }
    }
}
