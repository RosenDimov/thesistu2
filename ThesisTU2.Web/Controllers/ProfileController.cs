﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThesisTU.Common.Validators;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.DataObjects;
using ThesisTU.Models.Entities;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Web.SignalR;

namespace ThesisTU.Web.Controllers
{
    public class ProfileController : BaseController
    {
        public ProfileController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int LatestWallpostsCount = 400;
        private const int WallpostAnswersTakeCount = 2;
        private const int LatestNewsCount = 5;

        [Route("api/profile/wallpost/{id:int}")]
        public WallpostDO GetWallpost(int id)
        {
            var wallpost = unitOfWork.WallpostsRepository.GetById(id);

            return wallpost != null ? new WallpostDO(wallpost) : null;
        }

        [Route("api/profile/byid/{id:int}")]
        public IHttpActionResult GetProfileBasicInfo(int id)
        {
            if (userContext == null)
                return Unauthorized();

            var userEntity = unitOfWork.UsersRepository.GetById(id);

            if (userEntity == null)
                return BadRequest("No user with the specified ID exists.");

            var user = new UserDO(userEntity);
            var latestNews = unitOfWork.ArticlesRepository.GetLatestForTeam(LatestNewsCount, userEntity.FavouriteTeamID.Value).Select(a => new ArticleDO(a));

            var result = new
                        {
                            UserInfo = user,
                            LatestNews = latestNews
                        };

            return Ok(result);
        }

        [Route("api/profile/wallpostsinfo/{id:int}")]
        public IHttpActionResult GetProfileWallpostsInfo(int id)
        {
            if (userContext == null || (userContext.UserId != id && !AreUsersFriends(userContext.UserId, id)))
                return Unauthorized();

            var wallposts = unitOfWork.WallpostsRepository.GetLatestForUser(id, LatestWallpostsCount, skip: 0).Select(wp => new WallpostDO(wp));
            int count = unitOfWork.WallpostsRepository.CountForUser(id);

            var result = new
            {
                Wallposts = wallposts,
                WallpostsCount = count
            };

            return Ok(result);
        }

        [Route("api/profile/wallposts/{userId:int}/{skip:int}")]
        public IHttpActionResult GetWallpostsForUser(int userId, int skip)
        {
            if (userContext == null || (userContext.UserId != userId && !AreUsersFriends(userContext.UserId, userId)) )
                return Unauthorized();

            var wallposts = unitOfWork.WallpostsRepository.GetLatestForUser(userId, LatestWallpostsCount, skip: skip).Select(wp => new WallpostDO(wp));

            return Ok(wallposts);
        }

        [Route("api/profile/wallpostanswers/{wallpostId:int}/{skip:int}")]
        public IHttpActionResult GetWallpostAnswers(int wallpostId, int skip)
        {
            if (userContext == null)
                return Unauthorized();

            var wallposts = unitOfWork.WallpostsRepository.GetWallpostAnswers(wallpostId, WallpostAnswersTakeCount, skip: skip).Select(wp => new WallpostAnswerDO(wp));

            return Ok(wallposts);
        }

        [Route("api/profile/wallpost")]
        [HttpPost]
        public IHttpActionResult AddWallpost([FromBody]WallpostDO wallpost)
        {
            if (userContext == null)
                return Unauthorized();

            if (!WallpostValidator.Validate(wallpost))
                return BadRequest("Invalid wallpost object.");

            try
            {
                Wallpost wp = new Wallpost();
                wp.AuthorID = userContext.UserId;
                wp.DatePosted = DateTime.Now;
                wp.Text = wallpost.Text;
                wp.UserID = wallpost.UserID;

                unitOfWork.WallpostsRepository.Add(wp);

                unitOfWork.Save();

                wallpost = new WallpostDO(wp);
                var user = unitOfWork.UsersRepository.GetById(userContext.UserId);
                wallpost.AuthorID = user.UserId;
                wallpost.AuthorName = user.Username;
                wallpost.AuthorThumbImage = user.ThumbPath;

                if (userContext.UserId != wallpost.UserID)
                {
                    var userToNotify = unitOfWork.UsersRepository.GetById(wp.UserID);
                    userToNotify.Wallposts2.Add(wp);
                    unitOfWork.Save();
                    Broadcaster.Instance.BroadcastToClients(new int[] { wallpost.UserID });
                }

                Broadcaster.Instance.BroadcastWallpost(wallpost);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(wallpost);
        }

        [Route("api/profile/wallpostanswer")]
        [HttpPost]
        public IHttpActionResult AddWallpostAnswer([FromBody]WallpostAnswerDO wallpostAnswer)
        {
            if (userContext == null)
                return Unauthorized();

            if (!WallpostAnswerValidator.Validate(wallpostAnswer))
                return BadRequest("Invalid wallpost answer object.");

            try
            {
                WallpostsAnswer wpa = new WallpostsAnswer();
                wpa.AuthorID = userContext.UserId;
                wpa.DatePosted = DateTime.Now;
                wpa.Text = wallpostAnswer.Text;
                wpa.WallpostID = wallpostAnswer.WallpostID;

                unitOfWork.WallpostsRepository.AddWallpostAnswer(wpa);
                unitOfWork.Save();

                wallpostAnswer = new WallpostAnswerDO(wpa);
                var user = unitOfWork.UsersRepository.GetById(userContext.UserId);
                wallpostAnswer.AuthorID = userContext.UserId;
                wallpostAnswer.AuthorName = user.Username;
                wallpostAnswer.AuthorThumbImage = user.ThumbPath;

                Broadcaster.Instance.BroadcastWallpostAnswer(wallpostAnswer);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(wallpostAnswer);
        }

        private bool AreUsersFriends(int user1Id, int user2Id)
        {
            var areUsersFriends = unitOfWork.UsersRepository.GetFriendshipInfo(user1Id, user2Id).Item2;
            return areUsersFriends;
        }
    }
}
