﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Globalization;
using ThesisTU.Repositories.Abstract;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.Entities;
using ThesisTU.Models.DataObjects;
using ThesisTU.Models.Validators;
using System.Transactions;

namespace ThesisTU.Web.Controllers
{
    public class TransfersController : BaseController
    {
        public TransfersController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        private const int MinSearchLength = 3;
        private string lengthErrMessage = string.Format("The search term must be at least {0} characters long.", MinSearchLength);

        public IQueryable<Transfer> GetAll()
        {
            return unitOfWork.TransfersRepository.GetAll();
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody]TransferDO newTransfer)
        {
            if (!HasAdminRights())
                return BadRequest("You are not allowed to add transfers to the database.");

            if (!TransferValidator.Validate(newTransfer))
                return BadRequest("Invalid new transfer object.");

            Club club = unitOfWork.ClubsRepository.GetClubByName(newTransfer.BuyerName);

            if (club == null)
                return BadRequest("The buying team was not found in the database.");

            Player player = unitOfWork.PlayersRepository.GetExactOneByName(newTransfer.PlayerName);

            if (player == null)
                return BadRequest("Player does not exist in the database.");

            if (player.ClubID == club.ClubID)
                return BadRequest("Buying club cannot be the player's current club.");

            DateTime? date;
            bool hasNewerTransfer = unitOfWork.TransfersRepository.HasPlayerNewerTransferDate(newTransfer.DateCompleted.Value, player.PlayerID, out date);

            if (hasNewerTransfer)
                return BadRequest(string.Format("There is a transfer with a newer date for this player. The first available date is {0}", date.Value.AddDays(1).ToString("dd/MM/yyyy")));

            using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
            {
                unitOfWork.TransfersRepository.AddNewTransfer(newTransfer.ToTransfer(), player, club);
                unitOfWork.Save();

                scope.Complete();
            }

            return Created(string.Empty, newTransfer);
        }

        [HttpPut]
        public IHttpActionResult Update(TransferDO updateModel)
        {
            if (!HasAdminRights())
                return BadRequest("You are not allowed to add transfers to the database.");

            if (!TransferValidator.Validate(updateModel))
                return BadRequest("Invalid update model.");

            if (updateModel.SellerName == updateModel.BuyerName)
                return BadRequest("The specified buying club is already selected as seller in this transfer.");

            var newBuyingClub = unitOfWork.ClubsRepository.GetClubByName(updateModel.BuyerName);

            if (newBuyingClub == null)
                return BadRequest("The new buying club was not found in the database.");

            var latestTransferForPlayer = unitOfWork.TransfersRepository.GetLatestTransferForPlayer(updateModel.PlayerID);

            if (latestTransferForPlayer == null)
                return BadRequest("Invalid player ID.");

            bool isValidTransferDate = unitOfWork.TransfersRepository.IsValidTransferDate(updateModel.TransferID, updateModel.PlayerID, updateModel.DateCompleted.Value);

            // if not the oldest, nor the newest - must be between the date of the previus and the next transfer
            // if the latest - must be after the date of the transfer before it
            // if the oldest - must be before the date of the transfer after it
            if (!isValidTransferDate)
                return BadRequest("Invalid transfer date.");

            // transfer will be partially updated (without player and date) if a more recent transfer for the player exists
            bool partiallyUpdated = (latestTransferForPlayer.TransferID == updateModel.TransferID) ? false : latestTransferForPlayer.DateCompleted > updateModel.DateCompleted;

            using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
            {
                Transfer transferEntity = unitOfWork.TransfersRepository.GetById(updateModel.TransferID);

                // set new buyer only for full update
                if (!partiallyUpdated)
                    transferEntity.BuyerID = newBuyingClub.ClubID;

                transferEntity.Amount = updateModel.Amount;
                transferEntity.DateCompleted = updateModel.DateCompleted;
                transferEntity.Type = updateModel.TransferTypeID;

                var playerEntity = unitOfWork.PlayersRepository.GetById(updateModel.PlayerID);

                // update player info only if buyer is new and this is the latest transfer for that player, otherwise 
                // don't update player and buying club
                if (playerEntity.ClubID != transferEntity.BuyerID && !partiallyUpdated)
                {
                    playerEntity.ClubID = newBuyingClub.ClubID;
                    unitOfWork.PlayersRepository.Update(playerEntity);
                }

                unitOfWork.TransfersRepository.Update(transferEntity);
                unitOfWork.Save();
                scope.Complete();
            }

            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            Transfer transferToDelete = unitOfWork.TransfersRepository.GetById(id);

            if (transferToDelete == null)
                return BadRequest("Transfer not found.");

            using (TransactionScope scope = this.unitOfWork.CreateTransactionScope())
            {
                Transfer latestTransferForPlayer = unitOfWork.TransfersRepository.GetLatestTransferForPlayer(transferToDelete.PlayerID);

                if (latestTransferForPlayer.DateCompleted == transferToDelete.DateCompleted)
                {
                    int transfersCount = unitOfWork.TransfersRepository.GetTransfersCountForPlayer(transferToDelete.PlayerID);
                    Player player = transferToDelete.Player;

                    if (transfersCount == 1)
                        player.ClubID = transferToDelete.SellerID;
                    else if (transfersCount > 0)
                    {
                        Transfer secondLatest = unitOfWork.TransfersRepository.GetSecondLatestTransferForPlayer(player.PlayerID);
                        player.ClubID = secondLatest.BuyerID;

                        unitOfWork.PlayersRepository.Update(player);
                    }
                }

                unitOfWork.TransfersRepository.Delete(id);
                unitOfWork.Save();
                scope.Complete();
            }

            return Ok();
        }

        //[HttpGet]
        //public ValidTransferModel IsValid(string transferPlayer, string buyer, string date)
        //{
        //    var validModel = unitOfWork.TransfersRepository.IsValidTransfer(transferPlayer, buyer, DateTime.ParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture));
        //    return validModel;
        //}

        [Route("api/transfers/byid/{search:int}")]
        public TransferDO GetById(int search)
        {
            var transfer = unitOfWork.TransfersRepository.GetById(search);
            return transfer != null ? new TransferDO(transfer) : null;
        }

        [Route("api/transfers/byteam/{search}")]
        public IHttpActionResult GetTransfersByTeam(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var transfers = unitOfWork.TransfersRepository.GetTransfersByTeam(search).Select(t => new TransferDO(t));
            return Ok(transfers);
        }

        [Route("api/transfers/byteambuyer/{search}")]
        public IHttpActionResult GetTransfersByTeamAsBuyer(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var transfers = unitOfWork.TransfersRepository.GetTransfersByTeamAsBuyer(search).Select(t => new TransferDO(t));
            return Ok(transfers);
        }

        [Route("api/transfers/byteamseller/{search}")]
        public IHttpActionResult GetTransfersByTeamAsSeller(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var transfers = unitOfWork.TransfersRepository.GetTransfersByTeamAsSeller(search).Select(t => new TransferDO(t));
            return Ok(transfers);
        }

        [Route("api/transfers/byplayer/{search}")]
        public IHttpActionResult GetTransfersByPlayer(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var transfers = unitOfWork.TransfersRepository.GetTransfersByPlayer(search).Select(t => new TransferDO(t));
            return Ok(transfers);
        }

        [Route("api/transfers/bycat/{search}")]
        public IHttpActionResult GetTransfersByCategory(string search)
        {
            if (!IsValidSearch(search))
                return BadRequest(lengthErrMessage);

            var transfers = unitOfWork.TransfersRepository.GetTransfersByCategory(search).Select(t => new TransferDO(t));
            return Ok(transfers);
        }

        [Route("api/transfers/latestbyplayer/{search:int}")]
        public TransferDO GetLatestTransferForPlayer(int search)
        {
            Transfer latestTransfer = unitOfWork.TransfersRepository.GetLatestTransferForPlayer(search);
            return latestTransfer != null ? new TransferDO(latestTransfer) : null;
        }

        [Route("api/transfers/secondlatestforplayer/{search:int}")]
        public TransferDO GetSecondLatestTransferForPlayer(int search)
        {
            Transfer secondLatestTransfer = unitOfWork.TransfersRepository.GetSecondLatestTransferForPlayer(search);
            return secondLatestTransfer != null ? new TransferDO(secondLatestTransfer) : null;
        }

        [Route("api/transfers/countforplayer/{search:int}")]
        public int GetTransfersCountForPlayer(int search)
        {
            int transfersCount = unitOfWork.TransfersRepository.GetTransfersCountForPlayer(search);
            return transfersCount;
        }

        [Route("api/transfers/latest/{latestTransfersCount:int:min(1)}")]
        public IEnumerable<TransferDO> GetLatestTransfers(int latestTransfersCount)
        {
            var transfers = unitOfWork.TransfersRepository.GetLatestTransfers(latestTransfersCount).Select(t => new TransferDO(t));
            return transfers;
        }

        [Route("api/transfers/mostexpensive/{mostExpensiveTransfersCount:int:min(1)}")]
        public IEnumerable<TransferDO> GetMostExpensiveTransfers(int mostExpensiveTransfersCount)
        {
            var transfers = unitOfWork.TransfersRepository.GetMostExpensiveTransfers(mostExpensiveTransfersCount).Select(t => new TransferDO(t));
            return transfers;
        }

        [HttpGet]
        public bool IsValidTransferDate(int transferId, int playerId, string date)
        {
            bool isValidDate = unitOfWork.TransfersRepository.IsValidTransferDate(transferId, playerId, DateTime.ParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture));
            return isValidDate;
        }

        private bool IsValidSearch(string search)
        {
            return !string.IsNullOrWhiteSpace(search) && search.Length >= MinSearchLength;
        }
    }
}
