﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThesisTU.Models.Authentication;
using ThesisTU.Models.Entities;
using ThesisTU.Models.DataObjects;
using ThesisTU.Repositories.Abstract;
using System.Transactions;
using ThesisTU.Web.SignalR;
using ThesisTU.Web.Common;
using System.IO;

namespace ThesisTU.Web.Controllers
{
    public class UsersController : BaseController
    {
        public UsersController(IUnitOfWork unitOfWork, IUserContextProvider userContextProvider)
            : base(unitOfWork, userContextProvider)
        {
        }

        [Route("api/users/current")]
        public IHttpActionResult GetCurrentUser()
        {
            if (userContext == null)
                return Unauthorized();

            var user = unitOfWork.UsersRepository.GetById(userContext.UserId);

            var result = new UserDO(user);

            return Ok(result);
        }

        [HttpPut]
        public IHttpActionResult PutUser(UserDO user)
        {
            if (userContext == null)
                return Unauthorized();

            var favTeam = unitOfWork.ClubsRepository.GetClubByName(user.FavouriteTeam);

            if (favTeam == null)
                return BadRequest("The club was not found in the database.");

            var userEntity = unitOfWork.UsersRepository.GetById(userContext.UserId);

            if (user.AvatarPath != null)
            {
                if (userEntity.AvatarPath != null)
                {
                    DeleteUserImages(userEntity);
                }

                userEntity.AvatarPath = Statics.UsersOriginalPath + user.AvatarPath;
                userEntity.ThumbPath = Statics.UsersThumbsPath + user.ThumbPath;
            }
            else if (user.DeletePreviousAvatar && userEntity.AvatarPath != null && userEntity.ThumbPath != null)
            {
                DeleteUserImages(userEntity); 
                userEntity.AvatarPath = null;
                userEntity.ThumbPath = null;
            }

            userEntity.Fullname = user.Fullname;
            userEntity.FavouriteTeamID = favTeam.ClubID;
            userEntity.CountryID = user.CountryID;

            unitOfWork.Save();

            return Ok();
        }

        [HttpPost]
        public IHttpActionResult ChangeUserPassword(PasswordsDO passwords)
        {
            if (userContext == null)
                return Unauthorized();

            string newPassword = passwords.NewPassword;
            string newPasswordRepeat = passwords.NewPasswordRepeat;
            string oldPassword = passwords.OldPassword;

            if (newPassword != newPasswordRepeat)
                return BadRequest("New password fields don't match!");

            if (newPassword.Length < 6)
                return BadRequest("New password should be at least 6 characters long.");

            User user = unitOfWork.UsersRepository.GetById(userContext.UserId);

            if (user.VerifyPassword(oldPassword))
            {
                user.SetPassword(newPassword);
                unitOfWork.Save();
                return Ok();
            }
            else
            {
                return BadRequest("The provided current password is wrong!");
            }
        }

        [Route("api/users/friendship/{user1ID:int}/{user2ID:int}")]
        public IHttpActionResult GetUsersFriendshipInfo(int user1ID, int user2ID)
        {
            var friendshipInfo = unitOfWork.UsersRepository.GetFriendshipInfo(user1ID, user2ID);

            return Ok(new
            {
                HasFriendshipEntry = friendshipInfo.Item1,
                IsConfirmedFriendship = friendshipInfo.Item2
            });
        }

        [HttpPost]
        [Route("api/users/newfriendship")]
        public IHttpActionResult CreateFriendship(FriendshipDO friendshipInfo)
        {
            int user1ID = friendshipInfo.User1ID;
            int user2ID = friendshipInfo.User2ID;

            if (user1ID == user2ID)
                return BadRequest("A user cannot add himself as a friend.");

            var existingFriendshipInfo = unitOfWork.UsersRepository.GetFriendshipInfo(user1ID, user2ID);

            if (existingFriendshipInfo.Item1 || existingFriendshipInfo.Item2)
                return BadRequest("The users are already friends or the friendship is not confirmed yet.");

            unitOfWork.UsersRepository.CreateFriendship(user1ID, user2ID);
            unitOfWork.Save();
            Broadcaster.Instance.BroadcastFriendRequest(user2ID);

            return Ok();
        }

        private void DeleteUserImages(User userEntity)
        {
            string avatarPhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath(Statics.RootPath + userEntity.AvatarPath);
            string thumbPhysicalPath = System.Web.Hosting.HostingEnvironment.MapPath(Statics.RootPath + userEntity.ThumbPath);
            File.Delete(avatarPhysicalPath);
            File.Delete(thumbPhysicalPath);
        }
    }
}
