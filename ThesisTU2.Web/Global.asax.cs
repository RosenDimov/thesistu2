﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ThesisTU.Common.Ninject;
using Ninject.Web.Common;
using ThesisTU.Web.Binders;

namespace ThesisTU.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ModelBinders.Binders.DefaultBinder = new TagsModelBinder();
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectDependencyResolver dependencyResolver = new NinjectDependencyResolver(new Bootstrapper().Kernel);

            GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;
            DependencyResolver.SetResolver(dependencyResolver);

            GlobalConfiguration.Configuration.IncludeErrorDetailPolicy =
    IncludeErrorDetailPolicy.Always;
            //AuthConfig.RegisterAuth();
        }
    }
}