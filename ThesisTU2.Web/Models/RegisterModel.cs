﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThesisTU.Web.Models
{
    public class RegisterModel
    {
        [Required]
        [Display(Name = "Username")]
        [StringLength(20, ErrorMessage = "The field {0} must be at least {2} symbols long.", MinimumLength = 6)]
        public string UserName { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "The field {0} can't be more than {1} symbols long.")]
        [Display(Name = "Name")]
        public string Fullname { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The field {0} must be at least {2} symbols long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password (repeat)")]
        [Compare("Password", ErrorMessage = "Passwords don't match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Favourite team")]
        public string FavouriteTeam { get; set; }

        [Required]
        [Display(Name = "Country")]
        public int CountryID { get; set; }
    }
}