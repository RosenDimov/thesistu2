﻿window.onload = function () {
    $('.autocomplete-team input').autocomplete(
        {
            source: '/JsonAutocomplete/GetTeamsNames',
            minLength: 4
        });
}