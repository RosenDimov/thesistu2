﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using ThesisTU.Common.DataStructures;
using ThesisTU.Models.DataObjects;

namespace ThesisTU.Web.SignalR
{
    public class Broadcaster
    {
        private readonly static Lazy<Broadcaster> _instance = new Lazy<Broadcaster>(() => new Broadcaster(GlobalHost.ConnectionManager.GetHubContext<FootballHub>().Clients));

        private readonly static ConnectionsDictionary<int> _connections =
            new ConnectionsDictionary<int>();

        private Broadcaster(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;
        }

        public static Broadcaster Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {
            get;
            set;
        }

        public void AddConnection(int userId, string connectionId)
        {
            _connections.Add(userId, connectionId);
        }

        public void RemoveConnection(int userId, string connectionId)
        {
            _connections.Remove(userId, connectionId);
        }

        public void UpdateConnection(int userId, string connectionId)
        {
            if (!_connections.GetConnections(userId).Contains(connectionId))
            {
                _connections.Add(userId, connectionId);
            }
        }

        public void BroadcastToClients(IEnumerable<int> userIds)
        {
            foreach (int id in userIds)
            {
                foreach (string connectionId in _connections.GetConnections(id))
                {
                    Clients.Client(connectionId).notifyUpdate();
                }
            }
        }

        public void BroadcastWallpost(WallpostDO wallpost)
        {
            Clients.All.notifyWallpost(wallpost);
        }

        public void BroadcastWallpostAnswer(WallpostAnswerDO wallpostAnswer)
        {
            Clients.All.notifyWallpostAnswer(wallpostAnswer);
        }

        public void BroadcastFriendRequest(int targetUserId) 
        {
            foreach (string connectionId in _connections.GetConnections(targetUserId))
            {
                Clients.Client(connectionId).notifyFriendRequest();
            }
        }

        public void BroadcastArticleComment(ArticleCommentDO articleComment)
        {
            Clients.All.notifyArticleComment(articleComment);
        }
    }
}