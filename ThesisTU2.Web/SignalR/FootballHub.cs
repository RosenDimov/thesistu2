﻿using Microsoft.AspNet.SignalR;
using System;
using System.Threading.Tasks;
using ThesisTU.Common.DataStructures;

namespace ThesisTU.Web.SignalR
{
    public class FootballHub : Hub
    {
        public override Task OnConnected()
        {
            int userId = int.Parse(Context.User.Identity.Name);

            Broadcaster.Instance.AddConnection(userId, Context.ConnectionId);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            int userId = int.Parse(Context.User.Identity.Name);

            Broadcaster.Instance.RemoveConnection(userId, Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            int userId = int.Parse(Context.User.Identity.Name);

            Broadcaster.Instance.UpdateConnection(userId, Context.ConnectionId);

            return base.OnReconnected();
        }
    }
}