﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(ThesisTU.Web.Startup))]
namespace ThesisTU.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}