(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "../$$_lazy_route_resource lazy recursive":
/*!*******************************************************!*\
  !*** ../$$_lazy_route_resource lazy namespace object ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./clubs/clubs.module": [
		"./clubs/clubs.module.ts",
		"clubs-clubs-module"
	],
	"./players/players.module": [
		"./players/players.module.ts",
		"players-players-module"
	],
	"./profiles/profiles.module": [
		"./profiles/profiles.module.ts",
		"profiles-profiles-module"
	],
	"./transfers/transfers.module": [
		"./transfers/transfers.module.ts",
		"transfers-transfers-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__.t(id, 7);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./app-routing.module.ts":
/*!*******************************!*\
  !*** ./app-routing.module.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var can_activate_guard_1 = __webpack_require__(/*! ./can-activate.guard */ "./can-activate.guard.ts");
var selective_preloading_strategy_1 = __webpack_require__(/*! ./selective-preloading-strategy */ "./selective-preloading-strategy.ts");
var success_component_1 = __webpack_require__(/*! ./success.component */ "./success.component.ts");
var not_found_component_1 = __webpack_require__(/*! ./not-found.component */ "./not-found.component.ts");
var unauthorized_component_1 = __webpack_require__(/*! ./unauthorized.component */ "./unauthorized.component.ts");
var removed_component_1 = __webpack_require__(/*! ./removed.component */ "./removed.component.ts");
var appRoutes = [
    { path: 'profile', loadChildren: './profiles/profiles.module#ProfilesModule', data: { preload: true } },
    { path: 'clubs', loadChildren: './clubs/clubs.module#ClubsModule' },
    { path: 'players', loadChildren: './players/players.module#PlayersModule' },
    { path: 'transfers', loadChildren: './transfers/transfers.module#TransfersModule' },
    { path: 'success', component: success_component_1.SuccessComponent },
    { path: 'unauthorized', component: unauthorized_component_1.UnauthorizedComponent },
    { path: 'removed', component: removed_component_1.RemovedComponent },
    { path: '', redirectTo: '/latest', pathMatch: 'full' },
    { path: '**', component: not_found_component_1.PageNotFoundComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot(appRoutes, { preloadingStrategy: selective_preloading_strategy_1.SelectivePreloadingStrategy })
            ],
            exports: [
                router_1.RouterModule
            ],
            providers: [
                can_activate_guard_1.CanActivateGuard,
                selective_preloading_strategy_1.SelectivePreloadingStrategy
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "./app.component.html":
/*!****************************!*\
  !*** ./app.component.html ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\r\n    <div class=\"container\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"#\" style=\"padding-top: 10px;\">\r\n                <img src=\"/Upload/logo.png\" width=\"32\" height=\"32\" />\r\n                FootballZone\r\n            </a>\r\n        </div>\r\n\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <li class=\"dropdown\" *ngIf=\"userContext.IsUserAdmin\">\r\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Add <span class=\"caret\"></span></a>\r\n                    <ul class=\"dropdown-menu\" role=\"menu\">\r\n                        <li><a routerLink=\"/players/new\">Player</a></li>\r\n                        <li><a routerLink=\"/clubs/new\">Club</a></li>\r\n                        <li><a routerLink=\"/transfers/new\">Transfer</a></li>\r\n                        <li><a routerLink=\"/articles/new\">Article</a></li>\r\n                    </ul>\r\n                </li>\r\n\r\n                <li class=\"dropdown\">\r\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Search <span class=\"caret\"></span></a>\r\n                    <ul class=\"dropdown-menu\" role=\"menu\">\r\n                        <li><a routerLink=\"/players/search\">Players</a></li>\r\n                        <li><a routerLink=\"/clubs/search\">Clubs</a></li>\r\n                        <li><a routerLink=\"/transfers/search\">Transfers</a></li>\r\n                        <li><a routerLink=\"/articles/search\">Articles</a></li>\r\n                    </ul>\r\n                </li>\r\n            </ul>\r\n\r\n            <ul class=\"nav navbar-nav navbar-right\">\r\n                <li><a [routerLink]=\"['/profile', userContext.UserId]\"><strong>{{userContext.Username}}</strong></a></li>\r\n                <li><a routerLink=\"/profile/notifications\">Messages <span class=\"badge\" *ngIf=\"notificationsCount > 0\">{{notificationsCount}}</span></a></li>\r\n                <li class=\"dropdown\">\r\n                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">Profile <span class=\"caret\"></span></a>\r\n                    <ul class=\"dropdown-menu\" role=\"menu\">\r\n                        <li><a routerLink=\"/profile/edit\">Edit profile</a></li>\r\n                        <li><a routerLink=\"/profile/changepass\">Change password</a></li>\r\n                    </ul>\r\n                </li>\r\n                <li><a href=\"/Account/Logout\">Logout</a></li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n<div class=\"container main-container\">\r\n    <router-outlet></router-outlet>\r\n</div>"

/***/ }),

/***/ "./app.component.ts":
/*!**************************!*\
  !*** ./app.component.ts ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var auth_service_1 = __webpack_require__(/*! ./core/auth.service */ "./core/auth.service.ts");
var signalr_service_1 = __webpack_require__(/*! ./core/signalr.service */ "./core/signalr.service.ts");
var notifications_service_1 = __webpack_require__(/*! ./business-services/notifications.service */ "./business-services/notifications.service.ts");
var notifications_manager_service_1 = __webpack_require__(/*! ./core/notifications-manager.service */ "./core/notifications-manager.service.ts");
var AppComponent = /** @class */ (function () {
    function AppComponent(authService, signalRService, notificationsService, notificationsManagerService) {
        this.authService = authService;
        this.signalRService = signalRService;
        this.notificationsService = notificationsService;
        this.notificationsManagerService = notificationsManagerService;
        this.notificationsCount = 0;
        this.userContext = authService.getUserContext();
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.signalRService.initialize();
        this.getNotificationsCount();
        this.notificationSubscription = this.signalRService.notificationAvailable.subscribe(function () {
            _this.notificationsCount++;
        });
        this.notificationsCountChangeSubscription =
            this.notificationsManagerService.notificationsCountChange.subscribe(function () {
                _this.notificationsCount--;
            });
        this.friendRequestSubscription =
            this.signalRService.friendRequestAvailable.subscribe(function () {
                _this.notificationsCount++;
            });
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.notificationSubscription.unsubscribe();
        this.notificationsCountChangeSubscription.unsubscribe();
        this.friendRequestSubscription.unsubscribe();
    };
    AppComponent.prototype.getNotificationsCount = function () {
        var _this = this;
        this.notificationsService.getNewCount().subscribe(function (count) {
            _this.notificationsCount = count;
        }, function (error) {
            alert(error);
            _this.notificationsCount = 0;
        });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'football-app',
            template: __webpack_require__(/*! ./app.component.html */ "./app.component.html"),
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            signalr_service_1.SignalRService,
            notifications_service_1.NotificationsService,
            notifications_manager_service_1.NotificationsManagerService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "./app.module.ts":
/*!***********************!*\
  !*** ./app.module.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var platform_browser_1 = __webpack_require__(/*! @angular/platform-browser */ "../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var common_1 = __webpack_require__(/*! @angular/common */ "../node_modules/@angular/common/fesm5/common.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
var app_routing_module_1 = __webpack_require__(/*! ./app-routing.module */ "./app-routing.module.ts");
var core_module_1 = __webpack_require__(/*! ./core/core.module */ "./core/core.module.ts");
var business_services_module_1 = __webpack_require__(/*! ./business-services/business-services.module */ "./business-services/business-services.module.ts");
var articles_module_1 = __webpack_require__(/*! ./articles/articles.module */ "./articles/articles.module.ts");
var app_component_1 = __webpack_require__(/*! ./app.component */ "./app.component.ts");
var success_component_1 = __webpack_require__(/*! ./success.component */ "./success.component.ts");
var not_found_component_1 = __webpack_require__(/*! ./not-found.component */ "./not-found.component.ts");
var unauthorized_component_1 = __webpack_require__(/*! ./unauthorized.component */ "./unauthorized.component.ts");
var removed_component_1 = __webpack_require__(/*! ./removed.component */ "./removed.component.ts");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                articles_module_1.ArticlesModule,
                core_module_1.CoreModule,
                business_services_module_1.BusinessServicesModule,
                app_routing_module_1.AppRoutingModule
            ],
            declarations: [
                app_component_1.AppComponent,
                success_component_1.SuccessComponent,
                not_found_component_1.PageNotFoundComponent,
                unauthorized_component_1.UnauthorizedComponent,
                removed_component_1.RemovedComponent
            ],
            bootstrap: [app_component_1.AppComponent],
            exports: [],
            providers: [common_1.DatePipe]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "./articles/article.component.html":
/*!*****************************************!*\
  !*** ./articles/article.component.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n        <h3>{{article?.Title}}</h3>\r\n        <div class=\"spacer10\">\r\n            <strong>posted on {{article?.DateAdded | date: 'MMM dd, yyyy'}}</strong>\r\n        </div>\r\n        <p>{{article?.Text}}</p>\r\n    </div>\r\n\r\n</div>\r\n\r\n<hr />\r\n\r\n<div class=\"row\">\r\n    <h4>Comments</h4>\r\n</div>\r\n\r\n<div class=\"row spacer10\">\r\n    <div class=\"text-center\" [hidden]=\"!hasMoreComments\">\r\n        <a class=\"showMoreLink\" (click)=\"getComments()\">Show more</a>\r\n    </div>\r\n</div>\r\n<div class=\"row\" *ngFor=\"let comment of article?.Comments\">\r\n    <div class=\"panel panel-info\">\r\n        <div class=\"panel-heading\">\r\n            <h3 class=\"panel-title\">\r\n                <span>{{comment.Author}} on {{comment.DatePosted | date: 'medium'}}</span>\r\n            </h3>\r\n        </div>\r\n        <div class=\"panel-body\">\r\n            {{comment.Text}}\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row\" *ngIf=\"article.Comments?.length == 0\">\r\n    <div class=\"col-xs-4\">\r\n        <div class=\"alert alert-info\" role=\"alert\">\r\n            <p>No comments have been added yet.</p>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<style type=\"text/css\">\r\n    input.ng-invalid.ng-dirty {\r\n        background-color: #FA787E;\r\n    }\r\n</style>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-xs-12\">\r\n        <p>You have {{commentMaxLength - newComment.length}} symbols remaining.</p>\r\n        <form name=\"form\" #commentForm=\"ngForm\">\r\n            <div class=\"form-group\">\r\n                <textarea name=\"newComment\" [(ngModel)]=\"newComment\" maxlength=\"300\" required class=\"form-control\" rows=\"3\"></textarea>\r\n            </div>\r\n            <button (click)=\"addComment()\" [disabled]=\"!commentForm.form.valid\" type=\"button\" class=\"btn btn-info\">Add</button>\r\n        </form>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./articles/article.component.ts":
/*!***************************************!*\
  !*** ./articles/article.component.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var auth_service_1 = __webpack_require__(/*! ../core/auth.service */ "./core/auth.service.ts");
var signalr_service_1 = __webpack_require__(/*! ../core/signalr.service */ "./core/signalr.service.ts");
var articles_service_1 = __webpack_require__(/*! ../business-services/articles.service */ "./business-services/articles.service.ts");
var article_1 = __webpack_require__(/*! ../business-services/models/article */ "./business-services/models/article.ts");
var ArticleComponent = /** @class */ (function () {
    function ArticleComponent(authService, signalRService, articesService, router, route) {
        this.authService = authService;
        this.signalRService = signalRService;
        this.articesService = articesService;
        this.router = router;
        this.newComment = "";
        this.hasMoreComments = true;
        this.skipCommentsCount = 0;
        this.article = new article_1.Article();
        this.id = +route.snapshot.params['id'];
        this.commentMaxLength = this.authService.getMaxCommentLength();
    }
    ArticleComponent.prototype.ngOnInit = function () {
        this.getArticle();
        this.subscribeForNewArticleComment();
    };
    ArticleComponent.prototype.ngOnDestroy = function () {
        this.articleCommentAvaiableSubscription.unsubscribe();
    };
    ArticleComponent.prototype.addComment = function () {
        var _this = this;
        this.articesService.addArticleComment(this.newComment, this.id).subscribe(function () {
            _this.newComment = "";
        });
    };
    ArticleComponent.prototype.getComments = function () {
        var _this = this;
        this.articesService.getArticleComments(this.id, this.skipCommentsCount).subscribe(function (comments) {
            _this.toggleMoreCommentsButton(comments.length);
            if (comments.length > 0) {
                for (var _i = 0, comments_1 = comments; _i < comments_1.length; _i++) {
                    var comment = comments_1[_i];
                    _this.article.Comments.splice(0, 0, comment);
                }
            }
        });
    };
    ArticleComponent.prototype.getArticle = function () {
        var _this = this;
        this.articesService.getArticle(this.id).subscribe(function (data) {
            _this.article = data.Article;
            _this.totalCommentsCount = data.TotalCount;
            _this.toggleMoreCommentsButton(data.Article.Comments.length);
            console.log('inside getArticle()');
        }, function (error) {
            _this.router.navigate(['/not-found']);
        });
    };
    ArticleComponent.prototype.toggleMoreCommentsButton = function (dataLength) {
        this.skipCommentsCount += dataLength;
        if (dataLength == 0 || this.skipCommentsCount >= this.totalCommentsCount) {
            this.hasMoreComments = false;
        }
    };
    ArticleComponent.prototype.subscribeForNewArticleComment = function () {
        var _this = this;
        this.articleCommentAvaiableSubscription =
            this.signalRService.articleCommentAvailable.subscribe(function (articleComment) {
                if (articleComment !== null && typeof articleComment !== 'undefined' && _this.id == articleComment.ArticleID) { // without last check it can post to every article
                    _this.article.Comments.push(articleComment);
                    _this.updateArticleAnswersCount();
                }
            });
    };
    ArticleComponent.prototype.updateArticleAnswersCount = function () {
        this.skipCommentsCount++;
        this.totalCommentsCount++;
    };
    ArticleComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./article.component.html */ "./articles/article.component.html"),
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            signalr_service_1.SignalRService,
            articles_service_1.ArticlesService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], ArticleComponent);
    return ArticleComponent;
}());
exports.ArticleComponent = ArticleComponent;


/***/ }),

/***/ "./articles/articles-list.component.html":
/*!***********************************************!*\
  !*** ./articles/articles-list.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\r\n    <div class=\"col-xs-4\" *ngFor=\"let article of articles\">\r\n        <div class=\"thumbnail\">\r\n            <div class=\"caption\">\r\n                <h3>{{article.Title}}</h3>\r\n                <p>{{article.Text.substring(0, 100)}}...</p>\r\n                <p><a [routerLink]=\"['/articles', article.ArticleID]\" class=\"btn btn-default\" role=\"button\">More</a></p>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./articles/articles-list.component.ts":
/*!*********************************************!*\
  !*** ./articles/articles-list.component.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var auth_service_1 = __webpack_require__(/*! ../core/auth.service */ "./core/auth.service.ts");
var articles_service_1 = __webpack_require__(/*! ../business-services/articles.service */ "./business-services/articles.service.ts");
var ArticlesListComponent = /** @class */ (function () {
    function ArticlesListComponent(authService, articesService) {
        this.articesService = articesService;
        this.indexItems = authService.getIndexPageItemsCount();
    }
    ArticlesListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.articesService.getLatestArticles(this.indexItems).subscribe((function (articles) {
            _this.articles = articles;
        }));
    };
    ArticlesListComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./articles-list.component.html */ "./articles/articles-list.component.html"),
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            articles_service_1.ArticlesService])
    ], ArticlesListComponent);
    return ArticlesListComponent;
}());
exports.ArticlesListComponent = ArticlesListComponent;


/***/ }),

/***/ "./articles/articles-routing.module.ts":
/*!*********************************************!*\
  !*** ./articles/articles-routing.module.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var articles_list_component_1 = __webpack_require__(/*! ./articles-list.component */ "./articles/articles-list.component.ts");
var new_article_component_1 = __webpack_require__(/*! ./new-article.component */ "./articles/new-article.component.ts");
var article_component_1 = __webpack_require__(/*! ./article.component */ "./articles/article.component.ts");
var search_articles_component_1 = __webpack_require__(/*! ./search-articles.component */ "./articles/search-articles.component.ts");
var articlesRoutes = [
    { path: 'latest', component: articles_list_component_1.ArticlesListComponent },
    { path: 'articles/new', component: new_article_component_1.NewArticleComponent },
    { path: 'articles/search', component: search_articles_component_1.SearchArticlesComponent },
    { path: 'articles/:id', component: article_component_1.ArticleComponent },
];
var ArticlesRoutingModule = /** @class */ (function () {
    function ArticlesRoutingModule() {
    }
    ArticlesRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(articlesRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], ArticlesRoutingModule);
    return ArticlesRoutingModule;
}());
exports.ArticlesRoutingModule = ArticlesRoutingModule;


/***/ }),

/***/ "./articles/articles.module.ts":
/*!*************************************!*\
  !*** ./articles/articles.module.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var shared_module_1 = __webpack_require__(/*! ../shared/shared.module */ "./shared/shared.module.ts");
var articles_routing_module_1 = __webpack_require__(/*! ./articles-routing.module */ "./articles/articles-routing.module.ts");
var articles_list_component_1 = __webpack_require__(/*! ./articles-list.component */ "./articles/articles-list.component.ts");
var new_article_component_1 = __webpack_require__(/*! ./new-article.component */ "./articles/new-article.component.ts");
var article_component_1 = __webpack_require__(/*! ./article.component */ "./articles/article.component.ts");
var search_articles_component_1 = __webpack_require__(/*! ./search-articles.component */ "./articles/search-articles.component.ts");
var ArticlesModule = /** @class */ (function () {
    function ArticlesModule() {
    }
    ArticlesModule = __decorate([
        core_1.NgModule({
            imports: [
                articles_routing_module_1.ArticlesRoutingModule,
                shared_module_1.SharedModule
            ],
            declarations: [
                articles_list_component_1.ArticlesListComponent,
                new_article_component_1.NewArticleComponent,
                article_component_1.ArticleComponent,
                search_articles_component_1.SearchArticlesComponent
            ],
            exports: []
        })
    ], ArticlesModule);
    return ArticlesModule;
}());
exports.ArticlesModule = ArticlesModule;


/***/ }),

/***/ "./articles/new-article.component.html":
/*!*********************************************!*\
  !*** ./articles/new-article.component.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-horizontal\" role=\"form\" #articleForm=\"ngForm\">\r\n    <h4>Create new article</h4>\r\n    <div class=\"row\" [hidden]=\"!hasServerError\">\r\n        <div class=\"col-sm-offset-2 col-xs-10\">\r\n            <div class=\"alert alert-danger\"><a class=\"close\" data-dismiss=\"alert\">×</a>{{errMessage}}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"title\" class=\"form-control\" val-summary=\"Title\" [(ngModel)]=\"newArticle.Title\" maxlength=\"100\" required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <textarea name=\"content\" class=\"form-control\" val-summary=\"Content\" [(ngModel)]=\"newArticle.Text\" required rows=\"5\"></textarea>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-offset-2 col-sm-5\" role=\"alert\">\r\n            <div class=\"alert alert-info\">You must specify at least one valid club name to connect to the article.</div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label for=\"content\" class=\"col-sm-2 control-label\">Connected teams (max. 3)</label>\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"teams\" class=\"form-control\" [(ngModel)]=\"teams\" suggestions-multiple=\"/JsonAutocomplete/GetTeamsNamesTokens\" maxlength=\"100\" />\r\n        </div>\r\n    </div>\r\n    <!--<div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" class=\"form-control\" aa-label=\"Team 2\" aa-field=\"connectedSecond\" suggestions=\"/JsonAutocomplete/GetTeamsNames\" maxlength=\"100\" />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" class=\"form-control\" aa-label=\"Team 3\" aa-field=\"connectedThird\" suggestions=\"/JsonAutocomplete/GetTeamsNames\" maxlength=\"100\" />\r\n        </div>\r\n    </div>-->\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" (click)=\"create()\" [disabled]=\"!articleForm.form.valid\" class=\"btn btn-default\">Create</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./articles/new-article.component.ts":
/*!*******************************************!*\
  !*** ./articles/new-article.component.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var articles_service_1 = __webpack_require__(/*! ../business-services/articles.service */ "./business-services/articles.service.ts");
var article_1 = __webpack_require__(/*! ../business-services/models/article */ "./business-services/models/article.ts");
var NewArticleComponent = /** @class */ (function () {
    function NewArticleComponent(articesService, router) {
        this.articesService = articesService;
        this.router = router;
        this.newArticle = new article_1.Article();
    }
    NewArticleComponent.prototype.create = function () {
        var _this = this;
        if (this.teams) {
            this.newArticle.ConnectedTeamsIds = this.teams.split(',').map(function (id) { return parseInt(id); });
        }
        else {
            alert('You must enter at least one team.');
            return;
        }
        this.articesService.addArticle(this.newArticle).subscribe(function () {
            _this.router.navigate(['/success']);
        }, function (response) {
            _this.errMessage = response.error.Message;
            _this.hasServerError = true;
            _this.newArticle.ConnectedTeamsIds = [];
        });
    };
    NewArticleComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./new-article.component.html */ "./articles/new-article.component.html"),
        }),
        __metadata("design:paramtypes", [articles_service_1.ArticlesService,
            router_1.Router])
    ], NewArticleComponent);
    return NewArticleComponent;
}());
exports.NewArticleComponent = NewArticleComponent;


/***/ }),

/***/ "./articles/search-articles.component.html":
/*!*************************************************!*\
  !*** ./articles/search-articles.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Search articles</h4>\r\n\r\n<form class=\"form-horizontal\" name=\"form\" (ngSubmit)=\"searchArticles()\" novalidate>\r\n    <div class=\"row\" [hidden]=\"!hasServerError\">\r\n        <div class=\"col-sm-offset-2 col-xs-10\">\r\n            <div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">×</a>{{errMessage}}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <label class=\"control-label col-sm-2\">Title</label>\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"title\" [(ngModel)]=\"search.Title\" class=\"form-control\" />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label class=\"control-label col-sm-2\">Content</label>\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"content\" [(ngModel)]=\"search.Content\" class=\"form-control\" />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label class=\"control-label col-sm-2\">Start date</label>\r\n        <div class=\"col-sm-3\">\r\n            <input type=\"text\" name=\"startDate\" class=\"form-control\" [(ngModel)]=\"search.StartDate\" readonly=\"readonly\" datepicker />\r\n        </div>\r\n        <label class=\"control-label col-sm-2\">End date</label>\r\n        <div class=\"col-sm-3\">\r\n            <input type=\"text\" name=\"endDate\" class=\"form-control\" [(ngModel)]=\"search.EndDate\" readonly=\"readonly\" datepicker />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" class=\"btn btn-default\">Search</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n\r\n<div class=\"panel panel-default\" *ngIf=\"results?.length > 0\">\r\n    <div class=\"panel-heading\" style=\"font-size: 14px;\">Found {{results.length}} result(s)</div>\r\n\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>Title</th>\r\n                <th>Content</th>\r\n                <th>Date</th>\r\n                <th>Details</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let article of results\">\r\n                <td>{{article.Title}}</td>\r\n                <td>{{article.Text.substr(0, 50)}}...</td>\r\n                <td>{{article.DateAdded | date: 'dd/MM/yyyy' | notAvailable }}</td>\r\n                <td><a [routerLink]=\"['/articles', article.ArticleID]\">Go</a></td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<div class=\"alert alert-info col-sm-offset-2 col-sm-6\" *ngIf=\"results?.length == 0\" role=\"alert\">\r\n    <p>No results were found.</p>\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./articles/search-articles.component.ts":
/*!***********************************************!*\
  !*** ./articles/search-articles.component.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var articles_service_1 = __webpack_require__(/*! ../business-services/articles.service */ "./business-services/articles.service.ts");
var search_articles_object_1 = __webpack_require__(/*! ../business-services/models/search-articles-object */ "./business-services/models/search-articles-object.ts");
var SearchArticlesComponent = /** @class */ (function () {
    function SearchArticlesComponent(articesService) {
        this.articesService = articesService;
        this.search = new search_articles_object_1.SearchArticlesObject();
    }
    SearchArticlesComponent.prototype.searchArticles = function () {
        var _this = this;
        this.articesService.searchArticles(this.search).subscribe(function (data) {
            _this.results = data;
            _this.hasServerError = false;
        }, function (response) {
            _this.setServerError(response.error.Message);
        });
    };
    SearchArticlesComponent.prototype.setServerError = function (msg) {
        this.hasServerError = true;
        this.errMessage = msg;
    };
    SearchArticlesComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./search-articles.component.html */ "./articles/search-articles.component.html"),
        }),
        __metadata("design:paramtypes", [articles_service_1.ArticlesService])
    ], SearchArticlesComponent);
    return SearchArticlesComponent;
}());
exports.SearchArticlesComponent = SearchArticlesComponent;


/***/ }),

/***/ "./business-services/articles.service.ts":
/*!***********************************************!*\
  !*** ./business-services/articles.service.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var ArticlesService = /** @class */ (function () {
    function ArticlesService(http) {
        this.http = http;
        this.baseUrl = 'api/articles';
    }
    ArticlesService.prototype.addArticle = function (article) {
        return this.http.post(this.baseUrl + "/", article);
    };
    ArticlesService.prototype.getLatestArticles = function (count) {
        return this.http.get(this.baseUrl + "/latest/" + count);
    };
    ArticlesService.prototype.getArticle = function (id) {
        return this.http.get(this.baseUrl + "/byid/" + id);
    };
    ArticlesService.prototype.addArticleComment = function (comment, articleId) {
        return this.http.post(this.baseUrl + "/comment", { Text: comment, ArticleID: articleId });
    };
    ArticlesService.prototype.getArticleComments = function (articleId, skip) {
        return this.http.get("api/article/comments/" + articleId + "/" + skip);
    };
    ArticlesService.prototype.searchArticles = function (searchObject) {
        return this.http.post(this.baseUrl + "/search", searchObject);
    };
    ArticlesService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ArticlesService);
    return ArticlesService;
}());
exports.ArticlesService = ArticlesService;


/***/ }),

/***/ "./business-services/business-services.module.ts":
/*!*******************************************************!*\
  !*** ./business-services/business-services.module.ts ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var articles_service_1 = __webpack_require__(/*! ./articles.service */ "./business-services/articles.service.ts");
var wallposts_service_1 = __webpack_require__(/*! ./wallposts.service */ "./business-services/wallposts.service.ts");
var users_service_1 = __webpack_require__(/*! ./users.service */ "./business-services/users.service.ts");
var clubs_service_1 = __webpack_require__(/*! ./clubs.service */ "./business-services/clubs.service.ts");
var players_service_1 = __webpack_require__(/*! ./players.service */ "./business-services/players.service.ts");
var transfers_service_1 = __webpack_require__(/*! ./transfers.service */ "./business-services/transfers.service.ts");
var noms_service_1 = __webpack_require__(/*! ./noms.service */ "./business-services/noms.service.ts");
var notifications_service_1 = __webpack_require__(/*! ./notifications.service */ "./business-services/notifications.service.ts");
var file_upload_service_1 = __webpack_require__(/*! ./file-upload.service */ "./business-services/file-upload.service.ts");
var BusinessServicesModule = /** @class */ (function () {
    function BusinessServicesModule() {
    }
    BusinessServicesModule = __decorate([
        core_1.NgModule({
            providers: [
                articles_service_1.ArticlesService,
                wallposts_service_1.WallpostsService,
                users_service_1.UsersService,
                clubs_service_1.ClubsService,
                players_service_1.PlayersService,
                transfers_service_1.TransfersService,
                noms_service_1.NomsService,
                notifications_service_1.NotificationsService,
                file_upload_service_1.FileUploadService
            ]
        })
    ], BusinessServicesModule);
    return BusinessServicesModule;
}());
exports.BusinessServicesModule = BusinessServicesModule;


/***/ }),

/***/ "./business-services/clubs.service.ts":
/*!********************************************!*\
  !*** ./business-services/clubs.service.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var ClubsService = /** @class */ (function () {
    function ClubsService(http) {
        this.http = http;
        this.baseUrl = 'api/clubs';
    }
    ClubsService.prototype.getClubsByTerm = function (term) {
        return this.http.get(this.baseUrl + "/byterm/" + term);
    };
    ClubsService.prototype.getClub = function (id) {
        return this.http.get(this.baseUrl + "/byid/" + id);
    };
    ClubsService.prototype.getClubWithNews = function (id) {
        return this.http.get(this.baseUrl + "/byidwithnews/" + id);
    };
    ClubsService.prototype.addClub = function (club) {
        return this.http.post("" + this.baseUrl, club);
    };
    ClubsService.prototype.putClub = function (club) {
        return this.http.put("" + this.baseUrl, club);
    };
    ClubsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ClubsService);
    return ClubsService;
}());
exports.ClubsService = ClubsService;


/***/ }),

/***/ "./business-services/file-upload.service.ts":
/*!**************************************************!*\
  !*** ./business-services/file-upload.service.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var FileUploadService = /** @class */ (function () {
    function FileUploadService(http) {
        this.http = http;
    }
    FileUploadService.prototype.uploadFile = function (file, url) {
        if (!file)
            return null;
        var formData = new FormData();
        formData.append('uploadFile', file, file.name);
        var headers = new http_1.HttpHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        return this.http.post(url, formData, { headers: headers });
    };
    FileUploadService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], FileUploadService);
    return FileUploadService;
}());
exports.FileUploadService = FileUploadService;


/***/ }),

/***/ "./business-services/models/article.ts":
/*!*********************************************!*\
  !*** ./business-services/models/article.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Article = /** @class */ (function () {
    function Article() {
    }
    return Article;
}());
exports.Article = Article;


/***/ }),

/***/ "./business-services/models/search-articles-object.ts":
/*!************************************************************!*\
  !*** ./business-services/models/search-articles-object.ts ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var SearchArticlesObject = /** @class */ (function () {
    function SearchArticlesObject() {
    }
    return SearchArticlesObject;
}());
exports.SearchArticlesObject = SearchArticlesObject;


/***/ }),

/***/ "./business-services/noms.service.ts":
/*!*******************************************!*\
  !*** ./business-services/noms.service.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var NomsService = /** @class */ (function () {
    function NomsService(http) {
        this.http = http;
        this.baseUrl = 'api/noms';
    }
    NomsService.prototype.getCountries = function () {
        return this.http.get(this.baseUrl + "/countries/");
    };
    NomsService.prototype.getPlayerPositions = function () {
        return this.http.get(this.baseUrl + "/positions/");
    };
    NomsService.prototype.getTransferTypes = function () {
        return this.http.get(this.baseUrl + "/transfertypes/");
    };
    NomsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], NomsService);
    return NomsService;
}());
exports.NomsService = NomsService;


/***/ }),

/***/ "./business-services/notifications.service.ts":
/*!****************************************************!*\
  !*** ./business-services/notifications.service.ts ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var NotificationsService = /** @class */ (function () {
    function NotificationsService(http) {
        this.http = http;
        this.baseUrl = 'api/notifications';
    }
    NotificationsService.prototype.getNewCount = function () {
        return this.http.get(this.baseUrl + "/countforuser/");
    };
    NotificationsService.prototype.getUnreadNotifications = function () {
        return this.http.get(this.baseUrl + "/unread/");
    };
    NotificationsService.prototype.clearUnreadNotifications = function () {
        return this.http.get(this.baseUrl + "/removeforuser/");
    };
    NotificationsService.prototype.deleteUnreadNotification = function (id) {
        return this.http.delete(this.baseUrl + "/deleteunreadarticle/" + id);
    };
    NotificationsService.prototype.deleteUnreadWallpost = function (id) {
        return this.http.delete(this.baseUrl + "/deleteunreadwallpost/" + id);
    };
    NotificationsService.prototype.acceptFriendRequest = function (id) {
        return this.http.get(this.baseUrl + "/acceptfriend/" + id);
    };
    NotificationsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], NotificationsService);
    return NotificationsService;
}());
exports.NotificationsService = NotificationsService;


/***/ }),

/***/ "./business-services/players.service.ts":
/*!**********************************************!*\
  !*** ./business-services/players.service.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var PlayersService = /** @class */ (function () {
    function PlayersService(http) {
        this.http = http;
        this.baseUrl = 'api/players';
    }
    PlayersService.prototype.getPlayer = function (id) {
        return this.http.get(this.baseUrl + "/byid/" + id);
    };
    PlayersService.prototype.getPlayerWithTransfers = function (id) {
        return this.http.get(this.baseUrl + "/byidwithtransfers/" + id);
    };
    PlayersService.prototype.getPlayersByName = function (term) {
        return this.http.get(this.baseUrl + "/byname/" + term);
    };
    PlayersService.prototype.getPlayersByNameAndPosition = function (term, positionID) {
        return this.http.get(this.baseUrl + "/bynamespos/" + term + "/" + positionID);
    };
    PlayersService.prototype.addPlayer = function (player) {
        return this.http.post("" + this.baseUrl, player);
    };
    PlayersService.prototype.putPlayer = function (player) {
        return this.http.put("" + this.baseUrl, player);
    };
    PlayersService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], PlayersService);
    return PlayersService;
}());
exports.PlayersService = PlayersService;


/***/ }),

/***/ "./business-services/transfers.service.ts":
/*!************************************************!*\
  !*** ./business-services/transfers.service.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var TransfersService = /** @class */ (function () {
    function TransfersService(http) {
        this.http = http;
        this.baseUrl = 'api/transfers';
    }
    TransfersService.prototype.getTransfer = function (transferID) {
        return this.http.get(this.baseUrl + "/byid/" + transferID);
    };
    TransfersService.prototype.getTransfersByPlayer = function (term) {
        return this.http.get(this.baseUrl + "/byplayer/" + term);
    };
    TransfersService.prototype.getTransfersByTeam = function (term) {
        return this.http.get(this.baseUrl + "/byteam/" + term);
    };
    TransfersService.prototype.getTransfersByTeamBuyer = function (term) {
        return this.http.get(this.baseUrl + "/byteambuyer/" + term);
    };
    TransfersService.prototype.getTransfersByTeamSeller = function (term) {
        return this.http.get(this.baseUrl + "/byteamseller/" + term);
    };
    TransfersService.prototype.addTransfer = function (transfer) {
        return this.http.post("" + this.baseUrl, transfer);
    };
    TransfersService.prototype.putTransfer = function (transfer) {
        return this.http.put("" + this.baseUrl, transfer);
    };
    TransfersService.prototype.deleteTransfer = function (id) {
        return this.http.delete(this.baseUrl + "/" + id);
    };
    TransfersService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], TransfersService);
    return TransfersService;
}());
exports.TransfersService = TransfersService;


/***/ }),

/***/ "./business-services/users.service.ts":
/*!********************************************!*\
  !*** ./business-services/users.service.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
        this.baseUrl = 'api/users';
    }
    UsersService.prototype.getCurrentUser = function () {
        return this.http.get(this.baseUrl + "/current");
    };
    UsersService.prototype.putUser = function (user) {
        return this.http.put(this.baseUrl + "/", user);
    };
    UsersService.prototype.changeUserPassword = function (passwords) {
        return this.http.post(this.baseUrl + "/", passwords);
    };
    ;
    UsersService.prototype.getLoggedUserId = function () {
        return this.http.get('api/loggedid/');
    };
    UsersService.prototype.getFriendshipInfo = function (user1ID, user2ID) {
        return this.http.get(this.baseUrl + "/friendship/" + user1ID + "/" + user2ID);
    };
    UsersService.prototype.sentFriendshipRequest = function (friendshipInfo) {
        return this.http.post(this.baseUrl + "/newfriendship", friendshipInfo);
    };
    UsersService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UsersService);
    return UsersService;
}());
exports.UsersService = UsersService;


/***/ }),

/***/ "./business-services/wallposts.service.ts":
/*!************************************************!*\
  !*** ./business-services/wallposts.service.ts ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var http_1 = __webpack_require__(/*! @angular/common/http */ "../node_modules/@angular/common/fesm5/http.js");
__webpack_require__(/*! ../rxjs-operators */ "./rxjs-operators.ts");
var WallpostsService = /** @class */ (function () {
    function WallpostsService(http) {
        this.http = http;
        this.baseUrl = 'api/profile';
    }
    WallpostsService.prototype.getWallpost = function (id) {
        return this.http.get(this.baseUrl + "/wallpost/" + id);
    };
    WallpostsService.prototype.getWallposts = function (userId, skip) {
        return this.http.get(this.baseUrl + "/wallposts/" + userId + "/" + skip);
    };
    WallpostsService.prototype.getWallpostAnswers = function (wallpostId, skip) {
        return this.http.get(this.baseUrl + "/wallpostanswers/" + wallpostId + "/" + skip);
    };
    WallpostsService.prototype.getProfileInfo = function (id) {
        return this.http.get(this.baseUrl + "/byid/" + id);
    };
    WallpostsService.prototype.getProfileWallposts = function (id) {
        return this.http.get(this.baseUrl + "/wallpostsinfo/" + id);
    };
    WallpostsService.prototype.addWallpost = function (wallpost) {
        return this.http.post(this.baseUrl + "/wallpost", wallpost);
    };
    WallpostsService.prototype.addWallpostAnswer = function (wallpostAnswer) {
        return this.http.post(this.baseUrl + "/wallpostanswer", wallpostAnswer);
    };
    WallpostsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], WallpostsService);
    return WallpostsService;
}());
exports.WallpostsService = WallpostsService;


/***/ }),

/***/ "./can-activate.guard.ts":
/*!*******************************!*\
  !*** ./can-activate.guard.ts ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var auth_service_1 = __webpack_require__(/*! ./core/auth.service */ "./core/auth.service.ts");
var CanActivateGuard = /** @class */ (function () {
    function CanActivateGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    CanActivateGuard.prototype.canActivate = function (route, state) {
        var permission = route.data['permission'];
        if (!permission)
            return true;
        var hasPermission = this.authService.hasPermission(permission);
        if (!hasPermission) {
            this.router.navigate(['/unauthorized']);
        }
        return hasPermission;
    };
    CanActivateGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router,
            auth_service_1.AuthService])
    ], CanActivateGuard);
    return CanActivateGuard;
}());
exports.CanActivateGuard = CanActivateGuard;


/***/ }),

/***/ "./core/auth.service.ts":
/*!******************************!*\
  !*** ./core/auth.service.ts ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");
var AuthService = /** @class */ (function () {
    function AuthService() {
        this.permissionsSubject = new rxjs_1.Subject();
        this.permissionsAvailable = this.permissionsSubject.asObservable();
        this.userContext = window["config"];
    }
    AuthService.prototype.getUserContext = function () {
        return this.userContext;
    };
    AuthService.prototype.getIndexPageItemsCount = function () {
        var config = this.userContext.Config;
        return config.IndexItems;
    };
    AuthService.prototype.getMaxCommentLength = function () {
        var config = this.userContext.Config;
        return config.MaxCommentLength;
    };
    AuthService.prototype.setPermissions = function (permissions) {
        this.userContext.Config.Permissions = permissions;
        this.permissionsSubject.next(permissions);
    };
    AuthService.prototype.hasPermission = function (permission) {
        permission = permission.trim();
        var permissions = this.userContext.Config.Permissions;
        var hasPermission = permissions.some(function (item) {
            return item.trim().indexOf(permission) !== -1;
        });
        return hasPermission;
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;


/***/ }),

/***/ "./core/core.module.ts":
/*!*****************************!*\
  !*** ./core/core.module.ts ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var auth_service_1 = __webpack_require__(/*! ./auth.service */ "./core/auth.service.ts");
var signalr_service_1 = __webpack_require__(/*! ./signalr.service */ "./core/signalr.service.ts");
var notifications_manager_service_1 = __webpack_require__(/*! ./notifications-manager.service */ "./core/notifications-manager.service.ts");
var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        core_1.NgModule({
            providers: [
                auth_service_1.AuthService,
                signalr_service_1.SignalRService,
                notifications_manager_service_1.NotificationsManagerService
            ]
        })
    ], CoreModule);
    return CoreModule;
}());
exports.CoreModule = CoreModule;


/***/ }),

/***/ "./core/notifications-manager.service.ts":
/*!***********************************************!*\
  !*** ./core/notifications-manager.service.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");
var NotificationsManagerService = /** @class */ (function () {
    function NotificationsManagerService() {
        this.notificationsCountChangeSubject = new rxjs_1.Subject();
        this.notificationsCountChange = this.notificationsCountChangeSubject.asObservable();
    }
    NotificationsManagerService.prototype.decreaseNotificationsCount = function () {
        this.notificationsCountChangeSubject.next();
    };
    NotificationsManagerService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], NotificationsManagerService);
    return NotificationsManagerService;
}());
exports.NotificationsManagerService = NotificationsManagerService;


/***/ }),

/***/ "./core/signalr.service.ts":
/*!*********************************!*\
  !*** ./core/signalr.service.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");
var SignalRService = /** @class */ (function () {
    function SignalRService() {
        this.isInitialized = false;
        this.notificationSource = new rxjs_1.Subject();
        this.wallpostSource = new rxjs_1.Subject();
        this.wallpostAnswerSource = new rxjs_1.Subject();
        this.friendRequestSource = new rxjs_1.Subject();
        this.articleCommentSource = new rxjs_1.Subject();
        this.notificationAvailable = this.notificationSource.asObservable();
        this.wallpostAvailable = this.wallpostSource.asObservable();
        this.wallpostAnswerAvailable = this.wallpostAnswerSource.asObservable();
        this.friendRequestAvailable = this.friendRequestSource.asObservable();
        this.articleCommentAvailable = this.articleCommentSource.asObservable();
    }
    SignalRService.prototype.initialize = function () {
        var _this = this;
        if (this.isInitialized) {
            return;
        }
        var fhub = $.connection.footballHub;
        fhub.client.notifyUpdate = function () {
            _this.notificationSource.next();
        };
        fhub.client.notifyWallpost = function (wallpost) {
            _this.wallpostSource.next(wallpost);
        };
        fhub.client.notifyWallpostAnswer = function (wallpostAnswer) {
            _this.wallpostAnswerSource.next(wallpostAnswer);
        };
        fhub.client.notifyFriendRequest = function () {
            _this.friendRequestSource.next();
        };
        fhub.client.notifyArticleComment = function (articleComment) {
            _this.articleCommentSource.next(articleComment);
        };
        $.connection.hub.start();
        this.isInitialized = true;
    };
    SignalRService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], SignalRService);
    return SignalRService;
}());
exports.SignalRService = SignalRService;


/***/ }),

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__(/*! ./app.module */ "./app.module.ts");
var browserPlatform = platform_browser_dynamic_1.platformBrowserDynamic();
browserPlatform.bootstrapModule(app_module_1.AppModule);


/***/ }),

/***/ "./not-found.component.ts":
/*!********************************!*\
  !*** ./not-found.component.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent = __decorate([
        core_1.Component({
            template: "<div class=\"alert alert-danger\">\n                 <p>Page not found.</p>\n             </div>",
        })
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());
exports.PageNotFoundComponent = PageNotFoundComponent;


/***/ }),

/***/ "./removed.component.ts":
/*!******************************!*\
  !*** ./removed.component.ts ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var RemovedComponent = /** @class */ (function () {
    function RemovedComponent() {
    }
    RemovedComponent = __decorate([
        core_1.Component({
            template: "<div class=\"alert alert-success\" role=\"alert\">\n                  <strong>Success!</strong> The item was successfully removed from the database.\n               </div>",
        })
    ], RemovedComponent);
    return RemovedComponent;
}());
exports.RemovedComponent = RemovedComponent;


/***/ }),

/***/ "./rxjs-operators.ts":
/*!***************************!*\
  !*** ./rxjs-operators.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");


/***/ }),

/***/ "./selective-preloading-strategy.ts":
/*!******************************************!*\
  !*** ./selective-preloading-strategy.ts ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var rxjs_1 = __webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");
__webpack_require__(/*! rxjs */ "../node_modules/rxjs/_esm5/index.js");
var SelectivePreloadingStrategy = /** @class */ (function () {
    function SelectivePreloadingStrategy() {
    }
    SelectivePreloadingStrategy.prototype.preload = function (route, load) {
        if (route.data && route.data['preload']) {
            return load();
        }
        else {
            return rxjs_1.Observable.create(null);
        }
    };
    SelectivePreloadingStrategy = __decorate([
        core_1.Injectable()
    ], SelectivePreloadingStrategy);
    return SelectivePreloadingStrategy;
}());
exports.SelectivePreloadingStrategy = SelectivePreloadingStrategy;


/***/ }),

/***/ "./shared/basic-validation-summary.directive.ts":
/*!******************************************************!*\
  !*** ./shared/basic-validation-summary.directive.ts ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
var BasicValidationSummaryDirective = /** @class */ (function () {
    function BasicValidationSummaryDirective(el, model) {
        this.el = el;
        this.isValidated = false;
        this.validationMessages = {
            'required': '{label} is required.',
            'minlength': '{label} must be at least {minLengthValue} characters long.',
            'min': '{label} count must be at least {min}.',
            'max': '{label} count must be at most {max}.',
            'integer': '{label} must be an integer.',
            'float': '{label} must be a floating-point number.'
            //'maxlength': '{label} cannot be more than {maxLengthValue} characters long.'
        };
        this.control = model.control;
        this.oldValue = this.control.value;
        var nativeElement = this.el.nativeElement;
        this.elementName = nativeElement.attributes["name"].value;
    }
    BasicValidationSummaryDirective.prototype.ngOnInit = function () {
        this.insertErrorMessageElement();
        this.insertLabel();
    };
    BasicValidationSummaryDirective.prototype.ngAfterViewChecked = function () {
        // if the field is not changed, but blurred from the first time - not validated and touched
        // otherwise it was validated once, just check if the value is new
        if ((!this.isValidated && this.control.touched) || this.oldValue != this.control.value) {
            this.oldValue = this.control.value;
            this.setValidationMessage();
            this.isValidated = true;
        }
    };
    BasicValidationSummaryDirective.prototype.setValidationMessage = function () {
        var errorMessage = '';
        var control = this.control;
        if (control && (control.touched || control.dirty) && !control.valid) {
            var messages = this.validationMessages;
            for (var key in control.errors) {
                // if delimiter is <br/> - this triggers infinite ngAfterViewChecked
                errorMessage = this.addErrorMessage(errorMessage, control, key);
            }
        }
        if (errorMessage) {
            this.errorMessageElement.innerHTML = errorMessage;
            this.errorMessageElement.style.display = 'block';
        }
        else {
            this.errorMessageElement.style.display = 'none';
        }
    };
    BasicValidationSummaryDirective.prototype.insertErrorMessageElement = function () {
        var nativeElement = this.el.nativeElement;
        this.errorMessageElement = document.createElement("div");
        nativeElement.insertAdjacentElement('afterend', this.errorMessageElement);
    };
    BasicValidationSummaryDirective.prototype.insertLabel = function () {
        // insert label
        var nativeElement = this.el.nativeElement;
        var requiredAttr = nativeElement.attributes["required"];
        var parent = nativeElement.parentElement;
        var labelElement = document.createElement("label");
        var finalLabelName = requiredAttr ? this.labelName + " *" : this.labelName;
        labelElement.innerHTML = finalLabelName;
        labelElement.className = "col-sm-2 control-label";
        labelElement.htmlFor = this.elementName;
        parent.insertAdjacentElement('beforebegin', labelElement);
    };
    BasicValidationSummaryDirective.prototype.addErrorMessage = function (currentResult, control, key) {
        var validationInfo = control.errors[key];
        var errorMessage;
        switch (key) {
            case "required":
                errorMessage = this.addRequiredValidationMessage(currentResult);
                break;
            case "minlength":
                errorMessage = this.addMinLengthValidationMessage(currentResult, validationInfo.requiredLength);
                break;
            case "min":
                errorMessage = this.addMinValidationMessage(currentResult, validationInfo.minValue);
                break;
            case "max":
                errorMessage = this.addMaxValidationMessage(currentResult, validationInfo.maxValue);
                break;
            case "integer":
                errorMessage = this.addIntegerValidationMessage(currentResult);
                break;
            case "float":
                errorMessage = this.addFloatValidationMessage(currentResult);
                break;
        }
        return errorMessage;
    };
    BasicValidationSummaryDirective.prototype.addRequiredValidationMessage = function (currentResult) {
        var validationMessageTemplate = this.validationMessages['required'];
        var requiredMessage = validationMessageTemplate.replace("{label}", this.labelName);
        return this.concatNewMessage(currentResult, requiredMessage);
    };
    BasicValidationSummaryDirective.prototype.addMinLengthValidationMessage = function (currentResult, minLength) {
        var validationMessageTemplate = this.validationMessages['minlength'];
        var minLengthMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{minLengthValue}", minLength);
        return this.concatNewMessage(currentResult, minLengthMessage);
    };
    BasicValidationSummaryDirective.prototype.addMinValidationMessage = function (currentResult, minValue) {
        var validationMessageTemplate = this.validationMessages['min'];
        var minMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{min}", minValue);
        return this.concatNewMessage(currentResult, minMessage);
    };
    BasicValidationSummaryDirective.prototype.addMaxValidationMessage = function (currentResult, maxValue) {
        var validationMessageTemplate = this.validationMessages['max'];
        var maxMessage = validationMessageTemplate.replace("{label}", this.labelName)
            .replace("{max}", maxValue);
        return this.concatNewMessage(currentResult, maxMessage);
    };
    BasicValidationSummaryDirective.prototype.addIntegerValidationMessage = function (currentResult) {
        var validationMessageTemplate = this.validationMessages['integer'];
        var integerMessage = validationMessageTemplate.replace("{label}", this.labelName);
        return this.concatNewMessage(currentResult, integerMessage);
    };
    BasicValidationSummaryDirective.prototype.addFloatValidationMessage = function (currentResult) {
        var validationMessageTemplate = this.validationMessages['float'];
        var floatMessage = validationMessageTemplate.replace("{label}", this.labelName);
        return this.concatNewMessage(currentResult, floatMessage);
    };
    BasicValidationSummaryDirective.prototype.concatNewMessage = function (currentResult, newMessage) {
        return "" + currentResult + newMessage + " ";
    };
    __decorate([
        core_1.Input('val-summary'),
        __metadata("design:type", String)
    ], BasicValidationSummaryDirective.prototype, "labelName", void 0);
    BasicValidationSummaryDirective = __decorate([
        core_1.Directive({
            selector: '[val-summary]',
            providers: [forms_1.NgModel]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef,
            forms_1.NgModel])
    ], BasicValidationSummaryDirective);
    return BasicValidationSummaryDirective;
}());
exports.BasicValidationSummaryDirective = BasicValidationSummaryDirective;


/***/ }),

/***/ "./shared/datepicker.directive.ts":
/*!****************************************!*\
  !*** ./shared/datepicker.directive.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var DatepickerDirective = /** @class */ (function () {
    function DatepickerDirective(el) {
        this.ngModelChange = new core_1.EventEmitter();
        this.jQueryEl = $(el.nativeElement);
    }
    DatepickerDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.jQueryEl.datepicker({
            dateFormat: 'mm/dd/yy',
            nextText: '',
            prevText: '',
            yearRange: "-50:+0",
            changeYear: true,
            onSelect: function (date) {
                _this.ngModelChange.emit(date);
            }
        });
    };
    DatepickerDirective.prototype.ngOnDestroy = function () {
        this.jQueryEl.datepicker('destroy');
    };
    ;
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], DatepickerDirective.prototype, "ngModelChange", void 0);
    DatepickerDirective = __decorate([
        core_1.Directive({
            selector: '[datepicker]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], DatepickerDirective);
    return DatepickerDirective;
}());
exports.DatepickerDirective = DatepickerDirective;


/***/ }),

/***/ "./shared/file-upload.component.html":
/*!*******************************************!*\
  !*** ./shared/file-upload.component.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"col-sm-5\">\r\n    <input type=\"file\" (change)=\"setUploadFile($event)\">\r\n</div>"

/***/ }),

/***/ "./shared/file-upload.component.ts":
/*!*****************************************!*\
  !*** ./shared/file-upload.component.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
var FileUploadComponent = /** @class */ (function () {
    function FileUploadComponent() {
        this.ngChange = function (_) { };
        this.ngTouched = function () { };
    }
    FileUploadComponent_1 = FileUploadComponent;
    FileUploadComponent.prototype.setUploadFile = function (event) {
        this.ngChange(event.target.files[0]);
    };
    FileUploadComponent.prototype.writeValue = function (value) {
        this.value = value;
    };
    FileUploadComponent.prototype.registerOnChange = function (fn) {
        this.ngChange = fn;
    };
    FileUploadComponent.prototype.registerOnTouched = function (fn) {
        this.ngTouched = fn;
    };
    var FileUploadComponent_1;
    FileUploadComponent = FileUploadComponent_1 = __decorate([
        core_1.Component({
            selector: 'file-upload',
            template: __webpack_require__(/*! ./file-upload.component.html */ "./shared/file-upload.component.html"),
            providers: [{ provide: forms_1.NG_VALUE_ACCESSOR, useExisting: core_1.forwardRef(function () { return FileUploadComponent_1; }), multi: true }]
        })
    ], FileUploadComponent);
    return FileUploadComponent;
}());
exports.FileUploadComponent = FileUploadComponent;


/***/ }),

/***/ "./shared/float-validator.directive.ts":
/*!*********************************************!*\
  !*** ./shared/float-validator.directive.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
function floatValidator() {
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    var validationInfoObject = { 'float': { valid: false } };
    return function (control) {
        if (!control.value) {
            return null;
        }
        var isNumber = FLOAT_REGEXP.test(control.value);
        return isNumber ? null : validationInfoObject;
    };
}
exports.floatValidator = floatValidator;
var FloatValidatorDirective = /** @class */ (function () {
    function FloatValidatorDirective() {
    }
    FloatValidatorDirective_1 = FloatValidatorDirective;
    FloatValidatorDirective.prototype.ngOnInit = function () {
        this.valFn = floatValidator();
    };
    FloatValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    var FloatValidatorDirective_1;
    FloatValidatorDirective = FloatValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[float]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: FloatValidatorDirective_1, multi: true }]
        })
    ], FloatValidatorDirective);
    return FloatValidatorDirective;
}());
exports.FloatValidatorDirective = FloatValidatorDirective;


/***/ }),

/***/ "./shared/integer-validator.directive.ts":
/*!***********************************************!*\
  !*** ./shared/integer-validator.directive.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
function integerValidator() {
    var INTEGER_REGEXP = /^\-?\d+$/;
    var validationInfoObject = { 'integer': { valid: false } };
    return function (control) {
        // if input is something like '15.' the received value will be null - behaviour of JS as a whole
        // this is valid only for inputs of type number
        if (!control.value) {
            return null;
        }
        var isNumber = INTEGER_REGEXP.test(control.value);
        return isNumber ? null : validationInfoObject;
    };
}
exports.integerValidator = integerValidator;
var IntegerValidatorDirective = /** @class */ (function () {
    function IntegerValidatorDirective() {
    }
    IntegerValidatorDirective_1 = IntegerValidatorDirective;
    IntegerValidatorDirective.prototype.ngOnInit = function () {
        this.valFn = integerValidator();
    };
    IntegerValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    var IntegerValidatorDirective_1;
    IntegerValidatorDirective = IntegerValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[integer]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: IntegerValidatorDirective_1, multi: true }]
        })
    ], IntegerValidatorDirective);
    return IntegerValidatorDirective;
}());
exports.IntegerValidatorDirective = IntegerValidatorDirective;


/***/ }),

/***/ "./shared/max-validator.directive.ts":
/*!*******************************************!*\
  !*** ./shared/max-validator.directive.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
function maxValidator(max) {
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    var validationInfoObject = { 'max': { maxValue: max } };
    return function (control) {
        if (!control.value) {
            return null;
        }
        var isNumber = FLOAT_REGEXP.test(control.value);
        if (!isNumber) {
            return validationInfoObject;
        }
        else {
            var numericVal = parseFloat(control.value);
            return (numericVal > max) ? validationInfoObject : null;
        }
    };
}
exports.maxValidator = maxValidator;
var MaxValidatorDirective = /** @class */ (function () {
    function MaxValidatorDirective() {
    }
    MaxValidatorDirective_1 = MaxValidatorDirective;
    MaxValidatorDirective.prototype.ngOnInit = function () {
        this.valFn = maxValidator(this.max);
    };
    MaxValidatorDirective.prototype.ngOnChanges = function (changes) {
        var change = changes['max'];
        if (change) {
            var maxValue = parseFloat(change.currentValue);
            this.valFn = maxValidator(maxValue);
        }
    };
    MaxValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    var MaxValidatorDirective_1;
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], MaxValidatorDirective.prototype, "max", void 0);
    MaxValidatorDirective = MaxValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[max]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: MaxValidatorDirective_1, multi: true }]
        })
    ], MaxValidatorDirective);
    return MaxValidatorDirective;
}());
exports.MaxValidatorDirective = MaxValidatorDirective;


/***/ }),

/***/ "./shared/min-validator.directive.ts":
/*!*******************************************!*\
  !*** ./shared/min-validator.directive.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
function minValidator(min) {
    var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
    var validationInfoObject = { 'min': { minValue: min } };
    return function (control) {
        if (!control.value) {
            return null;
        }
        var isNumber = FLOAT_REGEXP.test(control.value);
        if (!isNumber) {
            return validationInfoObject;
        }
        else {
            var numericVal = parseFloat(control.value);
            return (numericVal < min) ? validationInfoObject : null;
        }
    };
}
exports.minValidator = minValidator;
var MinValidatorDirective = /** @class */ (function () {
    function MinValidatorDirective() {
    }
    MinValidatorDirective_1 = MinValidatorDirective;
    MinValidatorDirective.prototype.ngOnInit = function () {
        this.valFn = minValidator(this.min);
    };
    MinValidatorDirective.prototype.ngOnChanges = function (changes) {
        var change = changes['min'];
        if (change) {
            var minValue = parseFloat(change.currentValue);
            this.valFn = minValidator(minValue);
        }
    };
    MinValidatorDirective.prototype.validate = function (control) {
        return this.valFn(control);
    };
    var MinValidatorDirective_1;
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], MinValidatorDirective.prototype, "min", void 0);
    MinValidatorDirective = MinValidatorDirective_1 = __decorate([
        core_1.Directive({
            selector: '[min]',
            providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: MinValidatorDirective_1, multi: true }]
        })
    ], MinValidatorDirective);
    return MinValidatorDirective;
}());
exports.MinValidatorDirective = MinValidatorDirective;


/***/ }),

/***/ "./shared/multiple-suggestions.directive.ts":
/*!**************************************************!*\
  !*** ./shared/multiple-suggestions.directive.ts ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var MultipleSuggestionsDirective = /** @class */ (function () {
    function MultipleSuggestionsDirective(el) {
        this.ngModelChange = new core_1.EventEmitter();
        this.jQueryEl = $(el.nativeElement);
    }
    MultipleSuggestionsDirective.prototype.ngOnInit = function () {
        var that = this;
        var handler = function handleModelChanges() {
            var currentValue = this.val();
            that.ngModelChange.emit(currentValue);
        };
        this.jQueryEl.tokenInput(this.sourceUrl, {
            queryParam: "term",
            minChars: 3,
            noResultsText: "No results found",
            resultsLimit: 5,
            tokenLimit: 3,
            preventDuplicates: true,
            onAdd: handler,
            onDelete: handler
        });
    };
    __decorate([
        core_1.Input('suggestions-multiple'),
        __metadata("design:type", String)
    ], MultipleSuggestionsDirective.prototype, "sourceUrl", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], MultipleSuggestionsDirective.prototype, "ngModelChange", void 0);
    MultipleSuggestionsDirective = __decorate([
        core_1.Directive({
            selector: '[suggestions-multiple]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], MultipleSuggestionsDirective);
    return MultipleSuggestionsDirective;
}());
exports.MultipleSuggestionsDirective = MultipleSuggestionsDirective;


/***/ }),

/***/ "./shared/not-available.pipe.ts":
/*!**************************************!*\
  !*** ./shared/not-available.pipe.ts ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var NotAvailablePipe = /** @class */ (function () {
    function NotAvailablePipe() {
    }
    NotAvailablePipe.prototype.transform = function (value) {
        return value == null ? "N/A" : value;
    };
    NotAvailablePipe = __decorate([
        core_1.Pipe({
            name: 'notAvailable'
        })
    ], NotAvailablePipe);
    return NotAvailablePipe;
}());
exports.NotAvailablePipe = NotAvailablePipe;


/***/ }),

/***/ "./shared/permissions.directive.ts":
/*!*****************************************!*\
  !*** ./shared/permissions.directive.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var auth_service_1 = __webpack_require__(/*! ../core/auth.service */ "./core/auth.service.ts");
var PermissionsDirective = /** @class */ (function () {
    function PermissionsDirective(authService, element) {
        this.authService = authService;
        this.jQueryEl = $(element.nativeElement);
    }
    PermissionsDirective.prototype.ngOnInit = function () {
        if (typeof this.permission !== "string") {
            throw "hasPermission value must be a string";
        }
        this.value = this.permission.trim();
        this.notPermissionFlag = this.value[0] === '!';
        this.value = this.notPermissionFlag ? this.value.slice(1).trim() : this.value;
        this.subscribeForPermissionChanges();
    };
    PermissionsDirective.prototype.ngOnDestroy = function () {
        this.permissionsSubscription.unsubscribe();
    };
    PermissionsDirective.prototype.subscribeForPermissionChanges = function () {
        var _this = this;
        this.permissionsSubscription =
            this.authService.permissionsAvailable.subscribe(function () {
                _this.toggleVisibilityBasedOnPermission();
            });
        this.toggleVisibilityBasedOnPermission();
    };
    PermissionsDirective.prototype.toggleVisibilityBasedOnPermission = function () {
        var hasPermission = this.authService.hasPermission(this.value);
        if (hasPermission && !this.notPermissionFlag || !hasPermission && this.notPermissionFlag)
            this.jQueryEl.show();
        else
            this.jQueryEl.hide();
    };
    __decorate([
        core_1.Input('has-permission'),
        __metadata("design:type", String)
    ], PermissionsDirective.prototype, "permission", void 0);
    PermissionsDirective = __decorate([
        core_1.Directive({
            selector: '[has-permission]'
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService,
            core_1.ElementRef])
    ], PermissionsDirective);
    return PermissionsDirective;
}());
exports.PermissionsDirective = PermissionsDirective;


/***/ }),

/***/ "./shared/shared.module.ts":
/*!*********************************!*\
  !*** ./shared/shared.module.ts ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "../node_modules/@angular/common/fesm5/common.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "../node_modules/@angular/forms/fesm5/forms.js");
var permissions_directive_1 = __webpack_require__(/*! ./permissions.directive */ "./shared/permissions.directive.ts");
var datepicker_directive_1 = __webpack_require__(/*! ./datepicker.directive */ "./shared/datepicker.directive.ts");
var suggestions_directive_1 = __webpack_require__(/*! ./suggestions.directive */ "./shared/suggestions.directive.ts");
var multiple_suggestions_directive_1 = __webpack_require__(/*! ./multiple-suggestions.directive */ "./shared/multiple-suggestions.directive.ts");
var basic_validation_summary_directive_1 = __webpack_require__(/*! ./basic-validation-summary.directive */ "./shared/basic-validation-summary.directive.ts");
var min_validator_directive_1 = __webpack_require__(/*! ./min-validator.directive */ "./shared/min-validator.directive.ts");
var max_validator_directive_1 = __webpack_require__(/*! ./max-validator.directive */ "./shared/max-validator.directive.ts");
var integer_validator_directive_1 = __webpack_require__(/*! ./integer-validator.directive */ "./shared/integer-validator.directive.ts");
var float_validator_directive_1 = __webpack_require__(/*! ./float-validator.directive */ "./shared/float-validator.directive.ts");
var file_upload_component_1 = __webpack_require__(/*! ./file-upload.component */ "./shared/file-upload.component.ts");
var transfer_amount_pipe_1 = __webpack_require__(/*! ./transfer-amount.pipe */ "./shared/transfer-amount.pipe.ts");
var not_available_pipe_1 = __webpack_require__(/*! ./not-available.pipe */ "./shared/not-available.pipe.ts");
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        core_1.NgModule({
            imports: [],
            declarations: [
                permissions_directive_1.PermissionsDirective,
                datepicker_directive_1.DatepickerDirective,
                suggestions_directive_1.SuggestionsDirective,
                multiple_suggestions_directive_1.MultipleSuggestionsDirective,
                basic_validation_summary_directive_1.BasicValidationSummaryDirective,
                min_validator_directive_1.MinValidatorDirective,
                max_validator_directive_1.MaxValidatorDirective,
                integer_validator_directive_1.IntegerValidatorDirective,
                float_validator_directive_1.FloatValidatorDirective,
                file_upload_component_1.FileUploadComponent,
                transfer_amount_pipe_1.TransferAmountPipe,
                not_available_pipe_1.NotAvailablePipe
            ],
            exports: [
                permissions_directive_1.PermissionsDirective,
                datepicker_directive_1.DatepickerDirective,
                suggestions_directive_1.SuggestionsDirective,
                multiple_suggestions_directive_1.MultipleSuggestionsDirective,
                basic_validation_summary_directive_1.BasicValidationSummaryDirective,
                min_validator_directive_1.MinValidatorDirective,
                max_validator_directive_1.MaxValidatorDirective,
                integer_validator_directive_1.IntegerValidatorDirective,
                float_validator_directive_1.FloatValidatorDirective,
                file_upload_component_1.FileUploadComponent,
                transfer_amount_pipe_1.TransferAmountPipe,
                not_available_pipe_1.NotAvailablePipe,
                common_1.CommonModule,
                forms_1.FormsModule
            ]
        })
    ], SharedModule);
    return SharedModule;
}());
exports.SharedModule = SharedModule;


/***/ }),

/***/ "./shared/suggestions.directive.ts":
/*!*****************************************!*\
  !*** ./shared/suggestions.directive.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var SuggestionsDirective = /** @class */ (function () {
    function SuggestionsDirective(el) {
        this.ngModelChange = new core_1.EventEmitter();
        this.jQueryEl = $(el.nativeElement);
    }
    SuggestionsDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.jQueryEl.autocomplete({
            minLength: 4,
            source: this.sourceUrl,
            select: function (event, selectedItem) {
                _this.ngModelChange.emit(selectedItem.item.value);
            }
        });
    };
    SuggestionsDirective.prototype.ngOnDestroy = function () {
        this.jQueryEl.autocomplete('destroy');
    };
    ;
    __decorate([
        core_1.Input('suggestions'),
        __metadata("design:type", String)
    ], SuggestionsDirective.prototype, "sourceUrl", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], SuggestionsDirective.prototype, "ngModelChange", void 0);
    SuggestionsDirective = __decorate([
        core_1.Directive({
            selector: '[suggestions]'
        }),
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], SuggestionsDirective);
    return SuggestionsDirective;
}());
exports.SuggestionsDirective = SuggestionsDirective;


/***/ }),

/***/ "./shared/transfer-amount.pipe.ts":
/*!****************************************!*\
  !*** ./shared/transfer-amount.pipe.ts ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var TransferAmountPipe = /** @class */ (function () {
    function TransferAmountPipe() {
    }
    TransferAmountPipe.prototype.transform = function (value) {
        if (value === 0 || value == null) {
            return "None";
        }
        else {
            return "\u20AC" + value + " mln."; // format with currency euros
        }
    };
    TransferAmountPipe = __decorate([
        core_1.Pipe({
            name: 'transferAmount'
        })
    ], TransferAmountPipe);
    return TransferAmountPipe;
}());
exports.TransferAmountPipe = TransferAmountPipe;


/***/ }),

/***/ "./success.component.ts":
/*!******************************!*\
  !*** ./success.component.ts ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var SuccessComponent = /** @class */ (function () {
    function SuccessComponent() {
    }
    SuccessComponent = __decorate([
        core_1.Component({
            template: "<div class=\"alert alert-success\" role=\"alert\">\n                   <strong>Success!</strong> Item was successfully added to the database.\n               </div>",
        })
    ], SuccessComponent);
    return SuccessComponent;
}());
exports.SuccessComponent = SuccessComponent;


/***/ }),

/***/ "./unauthorized.component.ts":
/*!***********************************!*\
  !*** ./unauthorized.component.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var UnauthorizedComponent = /** @class */ (function () {
    function UnauthorizedComponent() {
    }
    UnauthorizedComponent = __decorate([
        core_1.Component({
            template: "<div class=\"alert alert-danger\">\n                   <strong>You are not allowed to view this page!</strong>\n               </div>",
        })
    ], UnauthorizedComponent);
    return UnauthorizedComponent;
}());
exports.UnauthorizedComponent = UnauthorizedComponent;


/***/ }),

/***/ 0:
/*!***********************!*\
  !*** multi ./main.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\ThesisTU2\ThesisTU2.Web\App\main.ts */"./main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map