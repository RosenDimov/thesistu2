(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["players-players-module"],{

/***/ "./business-services/models/player.ts":
/*!********************************************!*\
  !*** ./business-services/models/player.ts ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Player = /** @class */ (function () {
    function Player() {
    }
    return Player;
}());
exports.Player = Player;


/***/ }),

/***/ "./players/new-player.component.html":
/*!*******************************************!*\
  !*** ./players/new-player.component.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-horizontal\" role=\"form\" #playerForm=\"ngForm\">\r\n    <h4>\r\n        <span *ngIf=\"isEdit\">Edit player</span>\r\n        <span *ngIf=\"!isEdit\">Create new player</span>\r\n    </h4>\r\n    <div class=\"row\" [hidden]=\"!hasServerError\">\r\n        <div class=\"col-sm-offset-2 col-xs-10\">\r\n            <div class=\"alert alert-danger\"><a class=\"close\" data-dismiss=\"alert\">×</a>{{errMessage}}</div>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"firstName\" class=\"form-control\" val-summary=\"First name\" [(ngModel)]=\"newPlayer.FirstName\" required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"lastName\" class=\"form-control\" val-summary=\"Last name\" [(ngModel)]=\"newPlayer.LastName\" required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-1\">\r\n            <input type=\"number\" name=\"age\" class=\"form-control\" val-summary=\"Age\" [(ngModel)]=\"newPlayer.Age\" min=\"15\" max=\"50\" required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-3\">\r\n            <input type=\"text\" name=\"birthDate\" class=\"form-control\" val-summary=\"Date of birth\" [(ngModel)]=\"newPlayer.BirthDate\" readonly=\"readonly\" datepicker required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-1\">\r\n            <input type=\"number\" name=\"number\" class=\"form-control\" val-summary=\"Shirt number\" [(ngModel)]=\"newPlayer.Number\" min=\"1\" max=\"99\" integer required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-2\">\r\n            <input type=\"number\" name=\"goals\" class=\"form-control\" val-summary=\"Goals\" [(ngModel)]=\"newPlayer.Goals\" min=\"0\" max=\"2000\" integer />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-2\">\r\n            <select name=\"countryId\" class=\"form-control\" val-summary=\"Country\" [(ngModel)]=\"newPlayer.CountryID\" required>\r\n                <option *ngFor=\"let country of countries\" value=\"{{country.CountryID}}\">{{country.CountryName}}</option>\r\n            </select>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-2\">\r\n            <select name=\"positionId\" class=\"form-control\" val-summary=\"Playing position\" [(ngModel)]=\"newPlayer.PositionID\" required>\r\n                <option *ngFor=\"let position of positions\" value=\"{{position.PositionID}}\">{{position.Name}}</option>\r\n            </select>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\" *ngIf=\"isEdit\">\r\n        <label class=\"control-label col-sm-2\">Club name</label>\r\n        <div class=\"col-sm-5\" style=\"padding-top: 7px;\">\r\n            <span>{{newPlayer.ClubName}}</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\" *ngIf=\"!isEdit\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"clubName\" class=\"form-control\" val-summary=\"Club name\" [(ngModel)]=\"newPlayer.ClubName\" suggestions=\"/JsonAutocomplete/GetTeamsNames\" required />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label class=\"control-label col-sm-2\">Picture</label>\r\n        <file-upload name=\"file\" [(ngModel)]=\"file\"></file-upload>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" (click)=\"save()\" [disabled]=\"!playerForm.form.valid\" class=\"btn btn-default\">Save</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./players/new-player.component.ts":
/*!*****************************************!*\
  !*** ./players/new-player.component.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "../node_modules/@angular/common/fesm5/common.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var player_1 = __webpack_require__(/*! ../business-services/models/player */ "./business-services/models/player.ts");
var players_service_1 = __webpack_require__(/*! ../business-services/players.service */ "./business-services/players.service.ts");
var noms_service_1 = __webpack_require__(/*! ../business-services/noms.service */ "./business-services/noms.service.ts");
var file_upload_service_1 = __webpack_require__(/*! ../business-services/file-upload.service */ "./business-services/file-upload.service.ts");
var NewPlayerComponent = /** @class */ (function () {
    function NewPlayerComponent(router, route, nomsService, fileUploadService, playersService, datePipe) {
        this.router = router;
        this.route = route;
        this.nomsService = nomsService;
        this.fileUploadService = fileUploadService;
        this.playersService = playersService;
        this.datePipe = datePipe;
        this.file = null;
        this.newPlayer = new player_1.Player();
        this.isEdit = this.route.snapshot.data['isEditMode'];
    }
    NewPlayerComponent.prototype.ngOnInit = function () {
        this.getCountries();
    };
    NewPlayerComponent.prototype.save = function () {
        var _this = this;
        if (this.file !== null) {
            this.uploadFile().subscribe(function (data) {
                _this.newPlayer.PicturePath = data.Filename;
                _this.savePlayer();
            }, function (response) {
                _this.setServerError(response.error.Message);
            });
        }
        else {
            this.savePlayer();
        }
    };
    NewPlayerComponent.prototype.uploadFile = function () {
        return this.fileUploadService.uploadFile(this.file, "api/files/players");
    };
    NewPlayerComponent.prototype.getCountries = function () {
        var _this = this;
        this.nomsService.getCountries().subscribe(function (data) {
            _this.countries = data;
            _this.newPlayer.CountryID = data[0].CountryID;
            _this.getPlayersPositions();
        });
    };
    NewPlayerComponent.prototype.getPlayersPositions = function () {
        var _this = this;
        this.nomsService.getPlayerPositions().subscribe(function (positions) {
            _this.positions = positions;
            _this.newPlayer.PositionID = _this.positions[0].PositionID;
            if (_this.isEdit) {
                _this.getExistingPlayer();
            }
        });
    };
    NewPlayerComponent.prototype.getExistingPlayer = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.params['id']);
        if (typeof id === 'number' && !isNaN(id)) {
            this.playersService.getPlayer(id).subscribe(function (data) {
                _this.newPlayer = data;
                _this.newPlayer.BirthDate = _this.datePipe.transform(_this.newPlayer.BirthDate, 'MM/dd/yyyy');
                _this.newPlayer.PicturePath = null;
            }, function (response) {
                _this.setServerError(response.error.Message);
            });
        }
        else {
            this.router.navigate(['/']);
        }
    };
    NewPlayerComponent.prototype.savePlayer = function () {
        if (this.isEdit) {
            this.updatePlayer(this.newPlayer);
        }
        else {
            this.createPlayer(this.newPlayer);
        }
    };
    NewPlayerComponent.prototype.updatePlayer = function (player) {
        var _this = this;
        this.playersService.putPlayer(player).subscribe(function () {
            _this.router.navigate(['/success']);
        }, function (response) {
            _this.setServerError(response.error.Message);
        });
    };
    NewPlayerComponent.prototype.createPlayer = function (player) {
        var _this = this;
        this.playersService.addPlayer(player).subscribe(function () {
            _this.router.navigate(['/success']);
        }, function (response) {
            _this.setServerError(response.error.Message);
        });
    };
    NewPlayerComponent.prototype.setServerError = function (message) {
        this.hasServerError = true;
        this.errMessage = message;
    };
    NewPlayerComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./new-player.component.html */ "./players/new-player.component.html")
        }),
        __metadata("design:paramtypes", [router_1.Router,
            router_1.ActivatedRoute,
            noms_service_1.NomsService,
            file_upload_service_1.FileUploadService,
            players_service_1.PlayersService,
            common_1.DatePipe])
    ], NewPlayerComponent);
    return NewPlayerComponent;
}());
exports.NewPlayerComponent = NewPlayerComponent;


/***/ }),

/***/ "./players/player.component.html":
/*!***************************************!*\
  !*** ./players/player.component.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Details for {{player?.FirstName}} {{player?.LastName}}</h4>\r\n<div class=\"row spacer10\" *ngIf=\"player?.PicturePath\">\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-6\">\r\n            <img [src]=\"player?.PicturePath\" alt=\"No image\" />\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">First name</label>\r\n        <div class=\"col-sm-5\">{{player?.FirstName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Last name</label>\r\n        <div class=\"col-sm-5\">{{player?.LastName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Club</label>\r\n        <div class=\"col-sm-5\">{{player?.ClubName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Age</label>\r\n        <div class=\"col-sm-1\">{{player?.Age}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Shirt number</label>\r\n        <div class=\"col-sm-1\">{{player?.Number}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Goals</label>\r\n        <div class=\"col-sm-1\">{{player?.Goals | notAvailable }}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Country</label>\r\n        <div class=\"col-sm-5\">{{player?.CountryName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Playing position</label>\r\n        <div class=\"col-sm-5\">{{player?.PositionName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\" has-permission=\"change\">\r\n    <div *ngIf=\"player\" class=\"form-group\">\r\n        <div class=\"col-sm-2\">\r\n            <a [routerLink]=\"['/players/edit/', player.PlayerID]\" class=\"btn btn-warning\">Edit</a>\r\n            <button type=\"submit\" class=\"btn btn-danger\">Delete</button>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<h4 class=\"topspacer20\">Latest transfers for this player</h4>\r\n<div class=\"panel panel-default\" *ngIf=\"transfers?.length > 0\">\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>Player</th>\r\n                <th>From</th>\r\n                <th>To</th>\r\n                <th>Amount</th>\r\n                <th>Details</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let transfer of transfers\">\r\n                <td>{{transfer.PlayerName}}</td>\r\n                <td>{{transfer.SellerName}}</td>\r\n                <td>{{transfer.BuyerName}}</td>\r\n                <td>{{transfer.Amount | transferAmount}}</td>\r\n                <td><a [routerLink]=\"['/transfer', transfer.TransferID]\">Go</a></td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<div class=\"alert alert-info col-sm-6\" *ngIf=\"transfers?.length == 0\" role=\"alert\">\r\n    <p>No results were found.</p>\r\n</div>"

/***/ }),

/***/ "./players/player.component.ts":
/*!*************************************!*\
  !*** ./players/player.component.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var players_service_1 = __webpack_require__(/*! ../business-services/players.service */ "./business-services/players.service.ts");
var PlayerComponent = /** @class */ (function () {
    function PlayerComponent(playersService, router, route) {
        this.playersService = playersService;
        this.router = router;
        this.route = route;
    }
    PlayerComponent.prototype.ngOnInit = function () {
        this.getPlayer();
    };
    PlayerComponent.prototype.getPlayer = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.params["id"]);
        this.playersService.getPlayerWithTransfers(id).subscribe(function (data) {
            _this.player = data.Player;
            _this.transfers = data.Transfers;
        }, function () {
            _this.router.navigate(["/notfound"]);
        });
    };
    PlayerComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./player.component.html */ "./players/player.component.html")
        }),
        __metadata("design:paramtypes", [players_service_1.PlayersService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], PlayerComponent);
    return PlayerComponent;
}());
exports.PlayerComponent = PlayerComponent;


/***/ }),

/***/ "./players/players-routing.module.ts":
/*!*******************************************!*\
  !*** ./players/players-routing.module.ts ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var new_player_component_1 = __webpack_require__(/*! ./new-player.component */ "./players/new-player.component.ts");
var search_players_component_1 = __webpack_require__(/*! ./search-players.component */ "./players/search-players.component.ts");
var player_component_1 = __webpack_require__(/*! ./player.component */ "./players/player.component.ts");
var playersRoutes = [
    { path: 'new', data: { permission: 'change', isEditMode: false }, component: new_player_component_1.NewPlayerComponent },
    { path: 'edit/:id', data: { permission: 'change', isEditMode: true }, component: new_player_component_1.NewPlayerComponent },
    { path: 'search', component: search_players_component_1.SearchPlayersComponent },
    { path: ':id', component: player_component_1.PlayerComponent }
];
var PlayersRoutingModule = /** @class */ (function () {
    function PlayersRoutingModule() {
    }
    PlayersRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(playersRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], PlayersRoutingModule);
    return PlayersRoutingModule;
}());
exports.PlayersRoutingModule = PlayersRoutingModule;


/***/ }),

/***/ "./players/players.module.ts":
/*!***********************************!*\
  !*** ./players/players.module.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var shared_module_1 = __webpack_require__(/*! ../shared/shared.module */ "./shared/shared.module.ts");
var players_routing_module_1 = __webpack_require__(/*! ./players-routing.module */ "./players/players-routing.module.ts");
var new_player_component_1 = __webpack_require__(/*! ./new-player.component */ "./players/new-player.component.ts");
var search_players_component_1 = __webpack_require__(/*! ./search-players.component */ "./players/search-players.component.ts");
var player_component_1 = __webpack_require__(/*! ./player.component */ "./players/player.component.ts");
var PlayersModule = /** @class */ (function () {
    function PlayersModule() {
    }
    PlayersModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_module_1.SharedModule,
                players_routing_module_1.PlayersRoutingModule
            ],
            declarations: [
                new_player_component_1.NewPlayerComponent,
                search_players_component_1.SearchPlayersComponent,
                player_component_1.PlayerComponent
            ],
            providers: []
        })
    ], PlayersModule);
    return PlayersModule;
}());
exports.PlayersModule = PlayersModule;


/***/ }),

/***/ "./players/search-players.component.html":
/*!***********************************************!*\
  !*** ./players/search-players.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Search players</h4>\r\n\r\n<form class=\"form-horizontal\" name=\"form\" (ngSubmit)=\"search()\" #searchForm=\"ngForm\" novalidate>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"searchTerm\" val-summary=\"Player name\" [(ngModel)]=\"searchTerm\" class=\"form-control\" required minlength=\"3\" />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-2 col-sm-offset-2\">\r\n            <div class=\"checkbox\">\r\n                <label>\r\n                    <input type=\"checkbox\" name=\"filterByPosition\" [(ngModel)]=\"filterByPosition\" />\r\n                    Filter by position\r\n                </label>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-sm-3\">\r\n            <select name=\"position\" class=\"form-control\" [(ngModel)]=\"position\" [disabled]=\"!filterByPosition\" required>\r\n                <option *ngFor=\"let position of positions\" value=\"{{position.PositionID}}\">{{position.Name}}</option>\r\n            </select>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" class=\"btn btn-default\" [disabled]=\"!searchForm.form.valid\">Search</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n\r\n<div class=\"panel panel-default\" *ngIf=\"results?.length > 0\">\r\n    <div class=\"panel-heading\" style=\"font-size: 14px;\">Found {{results.length}} result(s)</div>\r\n\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>First name</th>\r\n                <th>Last name</th>\r\n                <th>Club</th>\r\n                <th>Position</th>\r\n                <th>Details</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let player of results\">\r\n                <td>{{player.FirstName}}</td>\r\n                <td>{{player.LastName}}</td>\r\n                <td>{{player.ClubName}}</td>\r\n                <td>{{player.PositionName}}</td>\r\n                <td><a [routerLink]=\"['/players', player.PlayerID]\">Go</a></td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<div class=\"alert alert-info col-sm-offset-2 col-sm-6\" *ngIf=\"results?.length == 0\" role=\"alert\">\r\n    <p>No results were found.</p>\r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./players/search-players.component.ts":
/*!*********************************************!*\
  !*** ./players/search-players.component.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var players_service_1 = __webpack_require__(/*! ../business-services/players.service */ "./business-services/players.service.ts");
var noms_service_1 = __webpack_require__(/*! ../business-services/noms.service */ "./business-services/noms.service.ts");
var SearchPlayersComponent = /** @class */ (function () {
    function SearchPlayersComponent(playersService, nomsService) {
        this.playersService = playersService;
        this.nomsService = nomsService;
        this.position = 0;
    }
    SearchPlayersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.nomsService.getPlayerPositions().subscribe(function (positions) {
            _this.positions = positions;
            _this.position = _this.positions[0].PositionID;
        });
    };
    SearchPlayersComponent.prototype.search = function () {
        if (this.filterByPosition) {
            this.getPlayersByNameAndPosition();
        }
        else {
            this.getPlayersByName();
        }
    };
    SearchPlayersComponent.prototype.getPlayersByNameAndPosition = function () {
        var _this = this;
        this.playersService.getPlayersByNameAndPosition(this.searchTerm, this.position).subscribe(function (data) {
            _this.results = data;
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchPlayersComponent.prototype.getPlayersByName = function () {
        var _this = this;
        this.playersService.getPlayersByName(this.searchTerm).subscribe(function (data) {
            _this.results = data;
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchPlayersComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./search-players.component.html */ "./players/search-players.component.html"),
        }),
        __metadata("design:paramtypes", [players_service_1.PlayersService,
            noms_service_1.NomsService])
    ], SearchPlayersComponent);
    return SearchPlayersComponent;
}());
exports.SearchPlayersComponent = SearchPlayersComponent;


/***/ })

}]);
//# sourceMappingURL=players-players-module.js.map