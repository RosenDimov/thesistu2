(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["transfers-transfers-module"],{

/***/ "./business-services/models/transfer.ts":
/*!**********************************************!*\
  !*** ./business-services/models/transfer.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Transfer = /** @class */ (function () {
    function Transfer() {
    }
    return Transfer;
}());
exports.Transfer = Transfer;


/***/ }),

/***/ "./transfers/new-transfer.component.html":
/*!***********************************************!*\
  !*** ./transfers/new-transfer.component.html ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form class=\"form-horizontal\" role=\"form\" #transferForm=\"ngForm\">\r\n    <h4>\r\n        <span *ngIf=\"isEdit\">Edit transfer</span>\r\n        <span *ngIf=\"!isEdit\">Create new transfer</span>\r\n    </h4>\r\n    <div class=\"row\" [hidden]=\"!hasServerError\">\r\n        <div class=\"col-sm-offset-2 col-xs-10\">\r\n            <div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">×</a>{{errMessage}}</div>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\" *ngIf=\"isEdit\">\r\n        <label class=\"control-label col-sm-2\">Player</label>\r\n        <div class=\"col-sm-5\" style=\"padding-top: 7px;\">\r\n            <span>{{transfer.PlayerName}}</span>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\" *ngIf=\"!isEdit\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"player\" class=\"form-control\" val-summary=\"Player\" [(ngModel)]=\"transfer.PlayerName\" suggestions=\"/JsonAutocomplete/GetPlayersNames\" required />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"buyer\" class=\"form-control\" val-summary=\"Buyer\" [(ngModel)]=\"transfer.BuyerName\" suggestions=\"/JsonAutocomplete/GetTeamsNames\" required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\" *ngIf=\"showAmount\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"amount\" class=\"form-control\" val-summary=\"Transfer worth\" [(ngModel)]=\"transfer.Amount\" float />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"dateCompleted\" class=\"form-control\" val-summary=\"Date completed\" [(ngModel)]=\"transfer.DateCompleted\" readonly=\"readonly\" datepicker required />\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-2\">\r\n            <select name=\"transferType\" class=\"form-control\" val-summary=\"Transfer type\" [(ngModel)]=\"transfer.TransferTypeID\" required>\r\n                <option *ngFor=\"let type of transferTypes\" value=\"{{type.TransferTypeID}}\">{{type.Title}}</option>\r\n            </select>\r\n        </div>\r\n    </div>\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" (click)=\"save()\" [disabled]=\"!transferForm.form.valid\" class=\"btn btn-default\">Save</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n"

/***/ }),

/***/ "./transfers/new-transfer.component.ts":
/*!*********************************************!*\
  !*** ./transfers/new-transfer.component.ts ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "../node_modules/@angular/common/fesm5/common.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var transfer_1 = __webpack_require__(/*! ../business-services/models/transfer */ "./business-services/models/transfer.ts");
var transfers_service_1 = __webpack_require__(/*! ../business-services/transfers.service */ "./business-services/transfers.service.ts");
var noms_service_1 = __webpack_require__(/*! ../business-services/noms.service */ "./business-services/noms.service.ts");
var NewTransferComponent = /** @class */ (function () {
    function NewTransferComponent(router, route, nomsService, transfersService, datePipe) {
        this.router = router;
        this.route = route;
        this.nomsService = nomsService;
        this.transfersService = transfersService;
        this.datePipe = datePipe;
        this.transfer = new transfer_1.Transfer();
        this.isEdit = this.route.snapshot.data['isEditMode'];
    }
    NewTransferComponent.prototype.ngOnInit = function () {
        this.getTransferTypes();
    };
    NewTransferComponent.prototype.ngDoCheck = function () {
        if (!this.freeTransferValue || !this.transfer.TransferTypeID)
            return;
        this.toggleAmountField();
    };
    NewTransferComponent.prototype.save = function () {
        if (this.isEdit) {
            this.updateTransfer();
        }
        else {
            this.createTransfer();
        }
    };
    NewTransferComponent.prototype.getTransferTypes = function () {
        var _this = this;
        this.nomsService.getTransferTypes().subscribe(function (transferTypes) {
            _this.transferTypes = transferTypes;
            _this.transfer.TransferTypeID = _this.transferTypes[0].TransferTypeID;
            for (var index in transferTypes) {
                if (transferTypes[index].Title.indexOf('Free') != -1) {
                    _this.freeTransferValue = transferTypes[index].TransferTypeID;
                    break;
                }
            }
            if (_this.isEdit) {
                _this.getExistingTransfer();
            }
        });
    };
    NewTransferComponent.prototype.getExistingTransfer = function () {
        var _this = this;
        var id = parseInt(this.route.snapshot.params['id']);
        if (typeof id === 'number' && !isNaN(id)) {
            this.transfersService.getTransfer(id).subscribe(function (data) {
                _this.transfer = data;
                _this.transfer.DateCompleted = _this.datePipe.transform(_this.transfer.DateCompleted, 'MM/dd/yyyy');
            }, function (response) {
                _this.setServerError(response.error.Message);
            });
        }
        else {
            this.router.navigate(['/']);
        }
    };
    NewTransferComponent.prototype.createTransfer = function () {
        var _this = this;
        this.transfersService.addTransfer(this.transfer).subscribe(function () {
            _this.router.navigate(['/success']);
        }, function (response) {
            _this.setServerError(response.error.Message);
        });
    };
    NewTransferComponent.prototype.updateTransfer = function () {
        var _this = this;
        this.transfersService.putTransfer(this.transfer).subscribe(function () {
            _this.router.navigate(['/success']);
        }, function (response) {
            _this.setServerError(response.error.Message);
        });
    };
    NewTransferComponent.prototype.toggleAmountField = function () {
        if (this.transfer.TransferTypeID == this.freeTransferValue) {
            this.showAmount = false;
            this.transfer.Amount = null;
        }
        else {
            this.showAmount = true;
        }
    };
    NewTransferComponent.prototype.setServerError = function (message) {
        this.hasServerError = true;
        this.errMessage = message;
    };
    NewTransferComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./new-transfer.component.html */ "./transfers/new-transfer.component.html"),
        }),
        __metadata("design:paramtypes", [router_1.Router,
            router_1.ActivatedRoute,
            noms_service_1.NomsService,
            transfers_service_1.TransfersService,
            common_1.DatePipe])
    ], NewTransferComponent);
    return NewTransferComponent;
}());
exports.NewTransferComponent = NewTransferComponent;


/***/ }),

/***/ "./transfers/search-transfers.component.html":
/*!***************************************************!*\
  !*** ./transfers/search-transfers.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Search transfers</h4>\r\n\r\n<form class=\"form-horizontal\" name=\"form\" (ngSubmit)=\"search()\" #searchForm=\"ngForm\" novalidate>\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Search by</label>\r\n        <div class=\"col-sm-5 radio-group\">\r\n            <input name=\"playertransferCriteria\" type=\"radio\" value=\"player\" [(ngModel)]=\"transferCriteria\" /> Player\r\n            <input name=\"clubtransferCriteria\" type=\"radio\" value=\"club\" [(ngModel)]=\"transferCriteria\" /> Club\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-5\">\r\n            <input type=\"text\" name=\"search\" val-summary=\"Search term\" [(ngModel)]=\"searchTerm\" class=\"form-control\" required ng-minlength=\"3\" />\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Club is (available if club selected):</label>\r\n        <div class=\"col-sm-5 radio-group\">\r\n            <input name=\"buyer\" type=\"radio\" value=\"buyer\" [(ngModel)]=\"clubRole\" [disabled]=\"transferCriteria == 'player'\" /> Buyer\r\n            <input name=\"seller\" type=\"radio\" value=\"seller\" [(ngModel)]=\"clubRole\" [disabled]=\"transferCriteria == 'player'\" /> Seller\r\n            <input name=\"any\" type=\"radio\" value=\"any\" [(ngModel)]=\"clubRole\" [disabled]=\"transferCriteria == 'player'\" /> Any\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"form-group\">\r\n        <div class=\"col-sm-offset-2 col-sm-10\">\r\n            <button type=\"submit\" class=\"btn btn-default\" [disabled]=\"!searchForm.form.valid\">Search</button>\r\n        </div>\r\n    </div>\r\n</form>\r\n\r\n<div class=\"panel panel-default\" *ngIf=\"results?.length > 0\">\r\n    <div class=\"panel-heading\" style=\"font-size: 14px;\">Found {{results.length}} result(s)</div>\r\n\r\n    <table class=\"table\">\r\n        <thead>\r\n            <tr>\r\n                <th>Player</th>\r\n                <th>From</th>\r\n                <th>To</th>\r\n                <th>Amount</th>\r\n                <th>Details</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let transfer of results\">\r\n                <td>{{transfer.PlayerName}}</td>\r\n                <td>{{transfer.SellerName}}</td>\r\n                <td>{{transfer.BuyerName}}</td>\r\n                <td>{{transfer.Amount | transferAmount}}</td>\r\n                <td><a [routerLink]=\"['/transfers', transfer.TransferID]\">Go</a></td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n</div>\r\n\r\n<div class=\"alert alert-info col-sm-offset-2 col-sm-6\" *ngIf=\"results?.length == 0\" role=\"alert\">\r\n    <p>No transfers were found.</p>\r\n</div>"

/***/ }),

/***/ "./transfers/search-transfers.component.ts":
/*!*************************************************!*\
  !*** ./transfers/search-transfers.component.ts ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var transfers_service_1 = __webpack_require__(/*! ../business-services/transfers.service */ "./business-services/transfers.service.ts");
var SearchTransfersComponent = /** @class */ (function () {
    function SearchTransfersComponent(transfersService) {
        this.transfersService = transfersService;
        this.transferCriteria = "player";
        this.clubRole = "any";
    }
    SearchTransfersComponent.prototype.search = function () {
        if (this.transferCriteria === 'player') {
            this.getTransfersByPlayer();
        }
        else if (this.transferCriteria === 'club') {
            if (this.clubRole === 'buyer') {
                this.getTransfersByTeamBuyer();
            }
            else if (this.clubRole === 'seller') {
                this.getTransfersByTeamSeller();
            }
            else {
                this.getTransfersByTeam();
            }
        }
    };
    SearchTransfersComponent.prototype.getTransfersByPlayer = function () {
        var _this = this;
        this.transfersService.getTransfersByPlayer(this.searchTerm).subscribe(function (data) {
            _this.setResults(data);
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchTransfersComponent.prototype.getTransfersByTeamBuyer = function () {
        var _this = this;
        this.transfersService.getTransfersByTeamBuyer(this.searchTerm).subscribe(function (data) {
            _this.setResults(data);
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchTransfersComponent.prototype.getTransfersByTeamSeller = function () {
        var _this = this;
        this.transfersService.getTransfersByTeamSeller(this.searchTerm).subscribe(function (data) {
            _this.setResults(data);
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchTransfersComponent.prototype.getTransfersByTeam = function () {
        var _this = this;
        this.transfersService.getTransfersByTeam(this.searchTerm).subscribe(function (data) {
            _this.setResults(data);
        }, function (response) {
            alert(response.error.Message);
        });
    };
    SearchTransfersComponent.prototype.setResults = function (data) {
        this.results = data;
    };
    SearchTransfersComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./search-transfers.component.html */ "./transfers/search-transfers.component.html"),
        }),
        __metadata("design:paramtypes", [transfers_service_1.TransfersService])
    ], SearchTransfersComponent);
    return SearchTransfersComponent;
}());
exports.SearchTransfersComponent = SearchTransfersComponent;


/***/ }),

/***/ "./transfers/transfer.component.html":
/*!*******************************************!*\
  !*** ./transfers/transfer.component.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Details for transfer</h4>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Player name</label>\r\n        <div class=\"col-sm-5\">{{transfer?.PlayerName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Buyer club</label>\r\n        <div class=\"col-sm-5\">{{transfer?.BuyerName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Seller club</label>\r\n        <div class=\"col-sm-5\">{{transfer?.SellerName}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Date completed</label>\r\n        <div class=\"col-sm-3\">{{transfer?.DateCompleted | date: 'dd/MM/yyyy' | notAvailable }}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Amount</label>\r\n        <div class=\"col-sm-2\">{{transfer?.Amount | transferAmount }}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div class=\"form-group\">\r\n        <label class=\"col-sm-2 control-label\">Type</label>\r\n        <div class=\"col-sm-3\">{{transfer?.Title}}</div>\r\n    </div>\r\n</div>\r\n<div class=\"row\">\r\n    <div *ngIf=\"transfer\" class=\"form-group\" has-permission=\"change\">\r\n        <div class=\"col-sm-2\">\r\n            <a [routerLink]=\"['/transfers/edit', transfer.TransferID]\" class=\"btn btn-warning\">Edit</a>\r\n            <a data-toggle=\"modal\" data-target=\"#confirm-delete\" class=\"btn btn-danger\">Delete</a>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"modal fade\" id=\"confirm-delete\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h4>Confirm deletion</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                Do you really want to delete this transfer?\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <a (click)=\"remove()\" class=\"btn btn-danger danger\" data-dismiss=\"modal\">Yes</a>\r\n                <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">No</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./transfers/transfer.component.ts":
/*!*****************************************!*\
  !*** ./transfers/transfer.component.ts ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var transfers_service_1 = __webpack_require__(/*! ../business-services/transfers.service */ "./business-services/transfers.service.ts");
var TransferComponent = /** @class */ (function () {
    function TransferComponent(router, route, transfersService) {
        this.router = router;
        this.route = route;
        this.transfersService = transfersService;
        this.id = parseInt(this.route.snapshot.params['id']);
    }
    TransferComponent.prototype.ngOnInit = function () {
        this.getTransfer();
    };
    TransferComponent.prototype.getTransfer = function () {
        var _this = this;
        this.transfersService.getTransfer(this.id).subscribe(function (transfer) {
            if (transfer == null)
                _this.router.navigate(['/notfound']);
            _this.transfer = transfer;
        }, function (error) {
            _this.router.navigate(['/notfound']);
        });
    };
    TransferComponent.prototype.remove = function () {
        var _this = this;
        this.transfersService.deleteTransfer(this.id).subscribe(function () {
            var element = $(".modal.in");
            element.remove();
            _this.router.navigate(['/removed']);
        }, function (response) {
            alert(response.error.Message);
        });
    };
    TransferComponent = __decorate([
        core_1.Component({
            template: __webpack_require__(/*! ./transfer.component.html */ "./transfers/transfer.component.html"),
        }),
        __metadata("design:paramtypes", [router_1.Router,
            router_1.ActivatedRoute,
            transfers_service_1.TransfersService])
    ], TransferComponent);
    return TransferComponent;
}());
exports.TransferComponent = TransferComponent;


/***/ }),

/***/ "./transfers/transfers-routing.module.ts":
/*!***********************************************!*\
  !*** ./transfers/transfers-routing.module.ts ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var router_1 = __webpack_require__(/*! @angular/router */ "../node_modules/@angular/router/fesm5/router.js");
var new_transfer_component_1 = __webpack_require__(/*! ./new-transfer.component */ "./transfers/new-transfer.component.ts");
var search_transfers_component_1 = __webpack_require__(/*! ./search-transfers.component */ "./transfers/search-transfers.component.ts");
var transfer_component_1 = __webpack_require__(/*! ./transfer.component */ "./transfers/transfer.component.ts");
var transfersRoutes = [
    { path: 'new', data: { permission: 'change', isEditMode: false }, component: new_transfer_component_1.NewTransferComponent },
    { path: 'edit/:id', data: { permission: 'change', isEditMode: true }, component: new_transfer_component_1.NewTransferComponent },
    { path: 'search', component: search_transfers_component_1.SearchTransfersComponent },
    { path: ':id', component: transfer_component_1.TransferComponent }
];
var TransfersRoutingModule = /** @class */ (function () {
    function TransfersRoutingModule() {
    }
    TransfersRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forChild(transfersRoutes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], TransfersRoutingModule);
    return TransfersRoutingModule;
}());
exports.TransfersRoutingModule = TransfersRoutingModule;


/***/ }),

/***/ "./transfers/transfers.module.ts":
/*!***************************************!*\
  !*** ./transfers/transfers.module.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "../node_modules/@angular/core/fesm5/core.js");
var shared_module_1 = __webpack_require__(/*! ../shared/shared.module */ "./shared/shared.module.ts");
var transfers_routing_module_1 = __webpack_require__(/*! ./transfers-routing.module */ "./transfers/transfers-routing.module.ts");
var new_transfer_component_1 = __webpack_require__(/*! ./new-transfer.component */ "./transfers/new-transfer.component.ts");
var search_transfers_component_1 = __webpack_require__(/*! ./search-transfers.component */ "./transfers/search-transfers.component.ts");
var transfer_component_1 = __webpack_require__(/*! ./transfer.component */ "./transfers/transfer.component.ts");
var TransfersModule = /** @class */ (function () {
    function TransfersModule() {
    }
    TransfersModule = __decorate([
        core_1.NgModule({
            imports: [
                shared_module_1.SharedModule,
                transfers_routing_module_1.TransfersRoutingModule
            ],
            declarations: [
                new_transfer_component_1.NewTransferComponent,
                search_transfers_component_1.SearchTransfersComponent,
                transfer_component_1.TransferComponent
            ],
            providers: []
        })
    ], TransfersModule);
    return TransfersModule;
}());
exports.TransfersModule = TransfersModule;


/***/ })

}]);
//# sourceMappingURL=transfers-transfers-module.js.map