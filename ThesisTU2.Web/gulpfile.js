var gulp = require('gulp');
var tslint = require('gulp-tslint');
var spawn = require('child_process').spawn;
var browserSync = require('browser-sync');

gulp.task('ngBuild', function () {
    var options = {
        detached: true,
        stdio: 'ignore'
    };

    var cmd = spawn('cmd', ['/c', 'ng build --watch=true'], options);
    cmd.unref();
    return cmd;
});

gulp.task('tslint', function () {
    var initialPath = "./App/**/*.ts";
    check(initialPath);

    gulp.watch(initialPath, function (event) {
        check(event.path);
    })

    function check(path) {
        gulp.src(path)
            .pipe(tslint({
                formatter: "verbose"
            }))
            .pipe(tslint.report({
                emitError: false
            }));
    }
});

gulp.task('browsersync', function () {
    var path = "./dist/*.js";

    var options = {
        proxy: {
            target: 'localhost:5291',
            ws: true
        },
        port: 3021,
        ghostMode: false,
        logPrefix: 'FootballSystem',
        reloadDelay: 0,
        reloadDebounce: 3000
    };

    browserSync(options);

    gulp.watch(path, function () {
        browserSync.reload();
    })
});